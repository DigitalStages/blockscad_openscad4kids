# BlocksCAD_OpenSCAD4Kids

![BlocksCAD](images/BlocksCAD_OpenSCAD4Kids_FeatureImg.png)

---

## Description

BlocksCAD4Kids is a course that teaches children and/or teachers how to create 3D models with BlocksCAD. BlocksCAD uses visual building blocks or puzzle pieces, which are modified and put together as elements. These blocks generate the code in the background that creates the 3D models.

The course can be hold in English or German. Taking into account the current level of knowledge of the children. The course leader will provide questions and translations into German or respectively into English, if necessary so that the children can follow the course content.

![](images/BlockSCADKurs_Korbinhalt.jpg)

---

## Folder Content

### >> files

* Course paper in English and German as .md files
* code >> Workshop guide with images, code and examples
* flyer >> Flyer for course promotion
* images >> Images used in Course guide
* printer >>
  - README.md
  - diy_spool >> 3D print designs and guide for a D.I.Y. filament spool
  - fixes >> Guidelines of how to fix issues with the printer
  - flyer >> 3D Printer Specs as flyer in A5 Format
  - images >> Images of printer
  - tips >> Contains information about the printer and how to use it
* slic3r >>
  - README.md contains tips of how to use the slicing software Slic3r
  - files >> Useful data e.g. Slic3r configuration file for the EasyThreedX2 printer
  - images >> Images used in Slic3r tutorial

### >> images

* Icons and feature images

---

#### [English Version](files/BlocksCAD_OpenSCAD4Kids_en.md)

#### [Deutsche Version](files/BlocksCAD_OpenSCAD4Kids_de.md)

---

[Flyer (Deutsch)](files/flyer/flyer_de.png)

[Link to the Workshop on my website](https://jensmeisner.net/blockscad/)
