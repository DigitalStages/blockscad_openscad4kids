### Tips and Tricks to use your EasyThreed X2 printer better

<br>

#### Blue tape your magnetic printing sheet

Use for this a blue tape, that is used for paint jobs on automobiles.
The tape will protect the actual platform from scratches, and the print
can be removed easy.

![img1](images/BlueTapeOnPrintBed.jpg)
