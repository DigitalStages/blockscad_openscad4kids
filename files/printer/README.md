# Easy Threed X2 Mini Printer For Kids
<br>

![img1](images/easythreedx2.jpg)

Easythreed X2  Mini 3D Printer is a small beginners 3D printer for kids

The Easythreed X2 Mini comes in a striking orange color (optionally also available in other colors, depending on the dealer). The dimensions of the printer are smaller (100 x 100 x 100mm WxDxH) than conventional printing areas of the usual 3D printer. However, if you want to start small and inexpensive, you will certainly be pleased with an interesting entry price. The usable printing area is only 100x100x100 mm, but should still be interesting for smaller 3D models. The print speed is probably 10-40 mm/s which is not necessarily fast but should ensure good results. The print height / layer height is between selectable 0.05mm and 0.4mm. The pressure nozzle measures the usual 0.4mm and the filament used also corresponds to the usual 1.75mm. However, only PLA can be used.

### Easythreed X2 Mini - Specs
<br>
Product Details



    Features
    With LCD screen and cooling fan, support WiFi
    Lightweight, portable and quiet. Certificate: CE, FCC, RoHS
    One-click printing, press the print button after inserting the TF card, and it will print automatically after warming up
    Super easy to install and easy to operate. Experience new fun with a removable magnetic platform
    Suitable for home education or DIY amateurs, but also suitable for students, children
    Support print PLA safety filament


    Specification
    Brand: Easythreed
    Model: X2
    Type: DIY
    Material (Frames): ABS,Metal
    Platform board: Aluminum Base,PCB
    Nozzle quantity: Single
    Nozzle diameter: 0.4mm
    Product forming size: 100*100*100mm
    Layer thickness: 0.05-0.3mm
    Print speed: 10-40mm/s
    Supporting material: PLA
    Material diameter: 1.75mm
    Language: English
    File format: OBJ,STL
    Voltage: 110-240V
    Working Power: 12V, 30W
    System support: Windows XP/7/8/10 (32 bit/64bit)
    Connector Type: TF card,USB


    Package content
    1 x 3D Printer, 1 x Power Cable, 1 x USB Cable, 1 x TF Card, 1 x Card Reader, 1 x English User Manual ( inside TF card ), 1 x Cross Screwdriver, 1 x 10M PLA 1.75 Filament ( Color Random ), 1 x Filament Holder Set included Software Accessories inside TF Card


<br>

[Link to EasyThreed](https://www.easythreed.com/h-col-1224.html)

## Directories and its content

* DIY_Spool:
  * SpoolDesign4BigPrinters_4mmNozzle >> Design for the printer matching spool in .stl format for a print, that can be used with bigger printer
  * SpoolDesign4EasyThreedX2_4mmNozzle >> Designs for the printer matching spool and a Do-It-Yourself instruction guide to create spools even with the printer itself (Side plates will be made of a 4 mm wooden sheet)
* Fixes:
  * FixLooseRubberBand_Y_Axis >> A guide to show how to tighten a rubber transport band of the Y Axis
* Flyer: Contains a flyer for the parents to take home and think about buying the printer for their kids as birthday present (German)
* Tips: Information about using the printer
