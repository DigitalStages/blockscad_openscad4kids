## Fixes and troubleshooting around the EasyThreed X2 printer

<br>

### Fix a loose rubber transport band: Y Axis

|![img1](images/FixLooseY_1.jpg)|
|----|
|1. Carefully remove the front cover of the header using hands or a small screw driver. *Do not use force!*|
|![img2](images/FixLooseY_2.jpg)|
|2. Remove the 2 white plugs on the left side of the images|
|![img3](images/FixLooseY_3.jpg)|
|3. Remove 4 screws to take off the transport motor|
|![img4](images/FixLooseY_4.jpg)|
|4. Remove 2 screws on the right side to remove plate in front of rubber band|
|![img5](images/FixLooseY_5.jpg)|
|5. Remove the rubber band, test how far it can be tighten, cut the rubber of with a scissor. You might need some force to get it tight. Be careful!|
|6. Walk back 4. to 1. to assemble|
