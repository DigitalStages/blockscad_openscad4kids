/*File Info--------------------------------------------------------------------
File Name: Spool4EasyThreedX2.scad
License: Attribution-ShareAlike 4.0 International (CC-BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc: This design is a mini filament spool for the Easy Threed X2 printer
Usage: 1. Uncomment the axis block and export it in stl format, then comment it again.
       2. Uncomment the clip code block and export it in stl format. 
       3. Uncomment the side plate block and export it in stl format for printing, or svg to use it
          as stencil for wooden sheets (4 mm thickness).
       You need 1 axis, 2 clips, and 2 side plates for 1 filament spool.
       !Please read instruction guideline of how to build it!
*/

////Code for axis -- 
////Select the entire block and press SHIFT+CTRL+D to uncomment, and CTRL+D to comment
//$fn=80;
//union()
//{
//    difference()
//    {
//        union()
//        {
//            //Lower cylinder
//            translate([0,0,12])
//            cylinder(h=40,d=44);
//            //Middle cylinder
//            cylinder(h=12,d=36);
//            //Upper cylinder
//            translate([0,0,52])
//            cylinder(h=12,d=36);
//        }
//        union()
//        {
//            //Center cutout
//            cylinder(h=64,d=30);
//            //Lower clip holes
//            translate([0,0,6])
//            cube([10,42,3],center=true);
//            //Upper clip holes
//            translate([0,0,58])
//            cube([10,42,3],center=true);
//            //Hole for Filament
//            rotate([0,0,0])
//            translate([0,0,0])
//            cylinder(h=50,d=2);
//            //Hole for Filament
//            translate([-25,18,45])
//            rotate([0,90,0])
//            cylinder(h=50,d=2.5);
//        }
//    }
//    //Bottom support cylinder
//    difference()
//    {
//        cylinder(h=12,d=45);
//        //Support cutout
//        cylinder(h=12,d=43);
//    }
//}

////Code for clip holder
////Select the entire block and press SHIFT+CTRL+D to uncomment, and CTRL+D to comment
//$fn=80;
//union()
//{
//    difference()
//    {
//        union()
//        {
//            translate([0,-20,0])
//            cube([14,7,2.8],center=true);
//            translate([3.4,19,0])
//            rotate([0,0,-4])
//            scale([0.8,1.5,1])
//            cylinder(h=2.8,d=5,center=true,$fn=3);
//            mirror([-1,0,0])
//            translate([3.4,19,0])
//            rotate([0,0,-4])
//            scale([0.8,1.5,1])
//            cylinder(h=2.8,d=5,center=true,$fn=3);
//        }
//        cylinder(h=4,d=38,center=true);
//    }
//    hull()
//    {
//        translate([2,-19.5,0])
//        cube([2.5,2.5,2.8],center=true);
//        translate([2,21.3,0])
//        cube([1.5,2,2.8],center=true);
//    }
//    mirror([-1,0,0])
//    hull()
//    {
//        translate([2,-19.5,0])
//        cube([2.5,2.5,2.8],center=true);
//        translate([2,21.3,0])
//        cube([1.5,2,2.8],center=true);
//    }
//}

////Code for side plate
////Select the entire block and press SHIFT+CTRL+D to uncomment, and CTRL+D to comment
//$fn=80;
////Comment next line only, if you want to export in dxf or svg format
//projection()
//union()
//{
//    difference()
//    {
//        cylinder(h=4,d=170,$fn=6);
//        union()
//        {
//            cylinder(h=4,d=37);
//            for(i=[0:360/6:360])
//            {
//                rotate([0,0,i])
//                translate([48,0,0])
//                cylinder(h=4,d=37);
//            }
//        }
//    }
//    //This are the markers for the drill of the boreholing
//    cylinder(h=4,d=2);
//    for(i=[0:360/6:360])
//    {
//        rotate([0,0,i])
//        translate([48,0,0])
//        cylinder(h=4,d=2);
//    }
//}
