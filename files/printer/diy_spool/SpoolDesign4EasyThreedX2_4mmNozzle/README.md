## DIY Spool for Easy Threed X2 Mini Printer

![img0](images/DIY_Spool_0.jpg)

The source file of the design is Spool4EasyThreedX2.scad with all designs including the plate. It is parametric and therefore can be modified.

### files Content

* Axis: Contains axis design in .stl format, and .gcode file to directly print with the Easy Threed X2 Printer
* Clip: Contains clip design in .stl format, and .gcode file to directly print with the Easy Threed X2 Printer
* Plate: Contains a round and a hexagonal graphic in .svg format for printing with a paper printer

<br>

### Do-it-youself instructions

|![img1](images/DIY_Spool_1.jpg)|
|----|
|1. Print out the plate design with thick paper, cut it out with a scissor, and poke holes to create drill marks|
|![img2](images/DIY_Spool_2.jpg)|
|2. Draw the shape and holes onto a 4mm thick wooden sheet (2 plates on A4)|
|![img3](images/DIY_Spool_3.jpg)|
|3. Saw and cut-out the holes (diameter 37 mm)|
|![img4](images/DIY_Spool_4.jpg)|
|4. Paint the plates (optional)|
|![img5](images/DIY_Spool_5.jpg)|
|5. Remove the support cylinder from the printed axis|
|![img6](images/DIY_Spool_6.jpg)|
|6. Assemble the spool: 2 plates, 2 clips, 1 axis|
|![img7](images/DIY_Spool_7.jpg)|
|7. Final result (Does not win a beauty contest, but it works!)|
