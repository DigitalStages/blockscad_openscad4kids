## Spool for Easy Threed X2 Mini Printer

![img1](images/Spool4Big3DPrinter.jpg)

* Content of files: Design in 2 parts with a screw thread to connect
* Available formats: .blend with both parts, .stl for each part
