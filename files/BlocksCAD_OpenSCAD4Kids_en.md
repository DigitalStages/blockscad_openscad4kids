# BlocksCAD >> OpenSCAD for Kids

<img src="../images/BlocksCAD_OpenSCAD4Kids_FeatureImg.png" width="960"/>

## Description

BlocksCAD4Kids is a course that teaches children how to create 3D models with BlocksCAD. BlocksCAD uses visual building blocks or puzzle pieces, which are modified and put together as elements. These blocks generate the code in the background that creates the 3D models.

We will do small exercises together to learn the different elements of the colorful modeling program. Each day ends with an exercise in which the children will consolidate their new knowledge.

The first part helps to build confidence in working with BlockSCAD by using easy modeling exercises. The second part will then go into a little more advanced examples to show, how to build 3D printable models. The third part shows, how to transform designs made with BlockSCAD into real things printed with a little 3D printer for kids.

---

## Index <a name="index"></a>

[Preparation](#preparation)

1. [Introduction](#introduction)

   1.1. [What is OpenSCAD?](#whatisopenscad)

   1.2. [What is BlocksCAD?](#whatisblockscad)

   1.3. [Why using Code?](#whycodeto3d)

   1.4. [How does BlocksCAD work?](#howblockscadwork)

2. [First steps with easy examples](#firststeps)

   2.1. [Lollipop](#lollipop)

   2.2. [Umbrella](#umbrella)

   2.3. [House](#house)

   2.4. [Cup](#cup)

   2.5. [Heart](#heart)

   2.6. [Key chain](#keychain)

   2.7. [Calculator](#calculator)

   2.8. [Pavilion](#pavilion)

   2.9. [Pine tree](#pinetree)

   2.10. [Forest](#forest)

3. [How to build models for 3D printing?](#howfor3d)

   3.1. [Finger Ring](#fingerring)

   3.2. [Necklace](#necklace)

   3.3. [Head for finger puppet](#fingerpuppet)

   3.4. [Shoe tag](#shoetag)

   3.5. [Robot 1 & 2](#robots)

4. [Print it out!](#printing)

   4.1. [My 3D Printer](#3dprinter)

   4.2. [Prepare for printing](#prepareprint)

   4.3. [How to print it?](#howtoprint)


---

## Preparation <a name="preparation"></a>

### BlocksCAD

There is not much preparation necessary to prepare for this course. The simplest way is using the online version of BlocksCAD. The browser you are going to use needs WebGL support. Simply test the online version by dragging a sphere onto the workspace and press "RENDER". The sphere then should appear in the 3D preview.

[BlocksCAD Online](https://www.blockscad3d.com/editor/)

Another way is downloading BlocksCAD from their Github project site, and run it offline.

[BlocksCAD Offline](https://github.com/einsteinsworkshop/blockscad)

There is one thing you need to do in order to get the text working using the offline version:

BlocksCAD must be run on a webserver for the text block to work, rather than being run on the local filesystem.  If you have Python or Python3 installed, you can use their simpleHTTPserver. Open a terminal and execute following commands:

    cd /path/to/blockscad-dir-with-index.html-in-it


    python -m SimpleHTTPServer 9000

or in case of Python3:

    python3 -m SimpleHTTPServer 9000


Then point your browser to:

    http://localhost:9000/


### 3D Printer

In case you already have a 3D printer and/or you want to learn how to prepare your 3D models to be printed.

You definitely need to slice your 3D models in slices according to your printers settings.

A controlling software is not necessary, if you use a sd card to transfer the sliced model file .gcode directly to the printer. Most printer can handle it through their in-built software.

You will find more about this topic in Section 4.


**All platforms** ====================================================

The simplest way of slicing your models does not need any installation. Go to the site, upload your .stl file you exported from BlocksCAD, enter the details of your printer and slice it into .gcode format. This file you can then upload to your printer directly or with a printer control software.

Online GCODE slicer using **kiri:moto** with a web browser:

[kiri:moto - Free to use](https://grid.space/kiri/)

<img src="images/kirimoto.png" width="800"/><br>

**PC - Linux, Windows, Mac** ==========================================

Otherwise, you can download and install an advanced software to slice models and / or control your printer from a PC (connected via USB or via Network):

[Slic3r - Free open-source software](https://slic3r.org/download/)

<img src="images/slic3r.png" width="800"/><br>

There is a printer [Profile](files/slic3r/files/Slic3r_config_EasyThreedX2.ini) file I stored on gitlab, that will setup Slic3r for a EasyThreedX2 printer. This is the printer I will introduce to you as a starter printer for little Makers.

An alternative to Slic3r is [CUDA](https://ultimaker.com/software/ultimaker-cura). However, this is too much for Beginners in my opinion.


**Android** ===========================================================

There is no proper slicing software for Android at the moment, so [kiri:moto - Kostenlos](https://grid.space/kiri/) is still the best guess of getting your models sliced for your printer.

However there is a cheap and well done 3D printer controlling software for Android. If you use an usb adaptor, you will be able to connect your 3D printer directly to your Android Tablet and control the 3D printer.

Adaptor:

<img src="images/OTG_USB_Adapter.jpg" width="256"/><br>


Software:

[GCodePrintr](https://play.google.com/store/apps/details?id=de.dietzm.gcodesimulatorprinter&hl=en_US&gl=US)

<img src="images/gcodeprintr.png" width="800"/><br>



---

#### [Back to Index](#index)

## 1. Introduction <a name="introduction"></a>

Before we start, lets have a look into the background of BlocksCAD and OpenSCAD.

---

#### [Back to Index](#index)

## 1.1. What is OpenSCAD? <a name="whatisopenscad"></a>

>*OpenSCAD is a free software application for creating solid 3D CAD (computer-aided design) objects. It is a script-only based modeller that uses its own description language; parts can be previewed, but cannot be interactively selected or modified by mouse in the 3D view. An OpenSCAD script specifies geometric primitives (such as spheres, boxes, cylinders, etc.) and defines how they are modified and combined (for instance by intersection, difference, envelope combination and Minkowski sums) to render a 3D model. As such, the program does constructive solid geometry (CSG). OpenSCAD is available for Windows, Linux and macOS.*

[Wiki Reference](https://en.wikipedia.org/wiki/OpenSCAD)

---

#### [Back to Index](#index)

## 1.2. What is BlocksCAD? <a name="index"></a>

>*BlocksCAD is an education technology company dedicated to creating tools that help students of all ages pursue their love of science, technology, engineering, art, and math (STEAM). Our signature product, BlocksCAD, is a cloud-based 3D modeling tool that encourages users to learn math, computational thinking and coding concepts through visualization and designing models to be 3D printed.*

* BlocksCAD is a scratch-like programming environment to create 3D objects.
* It was developed with the sponsorship of the Defense Advanced Research Projects Agency (DARPA) and the code is licensed GPLv3 or later.
* BlocksCad code will generate OpenSCAD code that one can export (code button on top right or the design window). Otherwise the STL model will be generated on the server. Playing with BlocksCAD is therefore also a way to learn OpenSCAD.

[EduWiki Reference](https://edutechwiki.unige.ch/en/BlocksCAD)

---

#### [Back to Index](#index)

## 1.3. Why using Code for 3D Modeling? <a name="whycodeto3d"></a>

There basic advantages in learning coding:

1. Using Python or other close-to-human languages are easy to learn, especially if you are younger
2. Enhances problem solving skills through logical thinking in both linear and non-linear ways
3. Programming skills are transferable to many different areas in life
4. Opens opportunities to invention and innovation by giving a tool to express thoughts and ideas
5. Using coding with 3D modeling helps kids to have fun with math

There are many CAD (Computer Aided Design) software available. OpenSCAD is unique with its own scripting language, and is a brilliant starter into CAD Modeling and coding. The code you are using is so close to human understanding, that you theoretically would not need a computer to create blueprints / sketches from it. Just read the script code without one of the axis, thus creating different views, like front view, side view, top view s.o.

Here some basic pros and cons of using OpenSCAD as a CAD modeling tool:

| Pros                           | Cons                                 |
|--------------------------------|--------------------------------------|
| Easy to learn                  | Difficult creating organic shapes    |
| Easy precision modeling        | Non-Traditional UI                   |
| Small file size                | Moving objects only via code         |
| Vivid helping community        | First need to learn coding           |
| Complex modeling with modules  | Limited Preview Functionality        |
| Can be imported into FreeCAD   | Animation very tricky to not possible|

#### **Most of all: It is fun to work with it once you figured it out!**

---

#### [Back to Index](#index)

## 1.4. How does BlocksCAD work? <a name="howblockscadwork"></a>

BlocksCAD is a web-browser application, that does not need to be installed on your computer. There is an offline version available that needs a simple webserver script to run in order to create 3D/2D text in BlocksCAD.

The graphical interface of BlocksCAD is fairly simple, so lets get through it.

<img src="images/1_Introduction_1.png" width="960"/><br>

#### Header

The header is the main menu with several subsections. I will only mention the section we are going to use.

* Wired sphere >> Change language
* Projects >> Create new project, save and open files with different formats
* Learn >> Examples, tutorials and videos
* Project Name >> Give your current project a name (Default: untitled)
* Left Arrow Icon >> Undo
* Right Arrow Icon >> Redo
* Trashcan Icon >> Delete workspace
* *Blocks | Code* >> Switch between blocks editor and OpenSCAD code view

#### Left Panel

This panel is the blocks menu, which has several subsections. A Block contains a certain functions, that generates the represented OpenSCAD code.

* 3D Shapes >> Basic 3d primitives
* 2D Shapes >> Basic 2d primitives
* Transforms >> Transform embedded shapes
* Set Ops >> Boolean operators
* Math >> Mathematical operators
* Logic >> Conditional operators
* Loops >> Loop block
* Text >> 3D and 2D Text related blocks
* Variables >> Variable blocks for parametric designs
* Modules >> Blocks to create modules

#### Center Panel

The center panel is basically the workspace. You drag in and combine all block on in this place. If you want to erase blocks, just drag it far to the Left out of the window, or press Del.

* Icon Pointer >> Centers the view of the workspace
* Icon + >> Zooms in
* Icon - >> Zooms out

***You can use the Left Mouse Button to pan, 2 finger gesture to zoom, and the Right Mouse Button to open a sub menu.***

***Use the finger to pan, the scrolling wheel to zoom in and out, and hold the finger for 2-3 second to open a sub menu.***

#### Right Panel

This is the 3D view port to see the generated 3D objects. The changes appears, after the button *Render* is pressed.

***You can use the Left Mouse Button to rotate, the scrolling wheel (or middle mouse button) to zoom in and out, and the Right Mouse Button to pan.***

***Use the finger to pan, the scrolling wheel to zoom in and out, and hold the finger for 2-3 second to open a sub menu.***

* Colored Square >> Change the color, with which your object is previewed
* 3 lines Icon >> Switches on/off Grid view
* Icon + >> Zooms in
* Icon - >> Zooms out
* Icon Pointer >> Centers the view of the workspace
* Render >> Creates a preview version of everything on the workspace
* Generate STL >> Exports preview as a 3D Mesh in .stl format

Lets try things out, so you learn the basic workflow in BlocksCAD:

* Change a language using the selection under wired sphere icon (Default: English)

  <img src="images/1_Introduction_2.png" width="960"/><br>

* Try the first example under **Learn >> Examples >> cube with cutouts**

  <img src="images/1_Introduction_3.png" width="960"/><br>

* If BlocksCAD ask, if you want to save the previous code, press **Don't Save**.

  <img src="images/1_Introduction_4.png" width="960"/><br>

* Press **Render** in the right panel to see, what the code is doing

  <img src="images/1_Introduction_5.png" width="960"/><br>


* Save the blocks on your workplace **Project >> Save Blocks to your Computer**

  <img src="images/1_Introduction_6.png" width="960"/><br>

* Now reopen your saved project **Projects >> Load Blocks from Computer**

  <img src="images/1_Introduction_7.png" width="960"/><br>

* As you can see, it saved it with the Project Name. Select it and press **Open**

  <img src="images/1_Introduction_8.png" width="960"/><br>

* Now press ***Code*** on right side of the header panel **Blocks | Code**. You will see the actual code, that is created by the blocks.

  <img src="images/1_Introduction_9.png" width="960"/><br>

* Now press ***Blocks***, then press **Projects >> Save OpenSCAD code to your Computer** to export the code you have seen to your computer.

  <img src="images/1_Introduction_10.png" width="960"/><br>

* As you probably do not have OpenSCAD installed yet, check out the image to see, how it will look like in OpenSCAD.

  <img src="images/1_Introduction_11.png" width="960"/><br>


* If you want to export the rendered preview as an object, press in the right panel **Generate STL**. Do not forget to name your project first.

* The exported STL file is relevant, if you want to continue and print it with a 3D printer, CNC Router or Laser cutter.

---

#### [Back to Index](#index)

## 2. First steps with easy examples <a name="firststeps"></a>

Before we go into printable Designs, let us learn about all the different blocks. You will learn with the following examples how to combine different blocks to build one object.

**Beside using numbers for counting or true or false (1 or 0), most of the time you will use numbers as units, such as Millimeter and Degree.**

Lets start with our first model...a lollipop.

## 2.1. Lollipop <a name="lollipop"></a>

<img src="images/Lollipop.jpg" width="512"/><br>

1. Start wit a new project by pressing **Projects >> New***
2. Drag and drop a **sphere** from **3D Shapes** into our workspace.
3. Press **Render** and change your point of view by clicking LMB and move mouse around.
4. If your object is out of sight, use the pointer icon to get it back.

  **The sphere shows 10, which means 10 mm or 1 cm radius. The diameter of the sphere is therefore?...2 cm or 20 mm.**

5. Change the number of the **sphere** to ***30*** and render again.

  **Now the sphere is 6 cm in diameter.**

  **But hold on, how can I see the difference?**

  **To see the difference, lets put a second sphere aside.**

  **But first, in order to move the sphere as aside, we need another piece called.**

6. Drag a **translate** block into the workspace. You will find it in **Transforms**.
7. Now enter ***50*** into the first cell with the name **X**.
8. Now drag another **sphere** from the **3D Shapes** menu, and plug it into the translate slot. Press **Render** again!

  **See! The new sphere is now created 50 mm aside our first sphere, alongside the red line.**

  **The red line is the x axis, the green line is the y axis and the blue line is the z axis.**

  ***Change the numbers in translate and see how the sphere is moving around with the changes you made. Do not forget to press "Render".***

9. Delete the first sphere by clicking on it and drag it left out of the scene, or press Delete key, while keeping the sphere within the translate block.
10. Now enter new values to **translate** as follows: ***X = 0, Y = 0, Z = 35***.
18. Change the **sphere** radius to ***8*** millimeter.
19. Now drag and drop another **cylinder** block from **3D Shapes** onto your workspace.
20. Enter the number ***1*** for **radius1** and ***28*** for **height**.
21. Press **Render** again.

  #### A voila, a lollipop!

22. Name your project as ***Lollipop*** under **Project Name:** .
23. Save your project under **Project >> Save Blocks to your computer**.

<img src="images/2_1_FirstSteps_1.png" width="960"/><br>


---

#### [Back to Index](#index)

## 2.2. Umbrella <a name="umbrella"></a>

<img src="images/Umbrella.jpg" width="512"/><br>

1. Open your Lollipop project under **Project >> Open Blocks from your computer**.
2. Change the name of this project to ***Umbrella*** under **Project Name:** .
3. Now click on the **sphere** and move it out of the workspace.
4. Keep the **translate** block.
5. Now drag another **cylinder** from **3D Shapes** into the scene and plug it into **translate**, and press **Render**.

  **Can you see the space between the 2 cylinders?**

6. Lets change it by entering the height of the first cylinder to the **translate** module ***Z = 25***.
7. Press **Render**.

  **Ok, now lets make an umbrella out of it!**

  **The cylinder block has radius1 and radius2 for the bottom circle and the top circle. The value of radius2 is bound to radius1 by default. The lock icon between the 2 values represent this connection.**  

8. Press the little lock icon to unlock the connection between **radius1** and **radius2** of the **cylinder** block, that is plugged into the **translate** block.
9. Change the value of **radius1** to ***20***, and **radius2** to ***1***.
10. Press **Render** again!

  **Now we’ve got it! There is our umbrella.**

9. Save the project under **Project >> Save Blocks to your computer**.

<img src="images/2_2_FirstSteps_1.png" width="960"/><br>

---

#### Exercise:

***Try all different 3D shapes, change the values and use translate to move them around in space! Let us see, what you can make of it!***

**Tip: You can plug translate blocks into each other, and press *+* to add more slots for objects. This way you can group objects together and move all at once!**

<img src="images/2_2_FirstSteps_2.png" width="512"/><br>

---

#### [Back to Index](#index)

## 2.3. House <a name="house"></a>

<img src="images/House.jpg" width="512"/><br>

1. Lets start with **Project >> New** again.
2. Name this project ***House***.
3. Drag and drop a **cube** from **3D Shapes** onto the workspace.
4. Give it following values ***X = 30, Y = 40, Z = 20***.
5. Press **Render**.

  **Now, we want to get a roof on top of our house.**

6. Drag and drop one **cube** from **3D Shapes**, one **translate** and one **rotate** from **Transforms**.
7. Change the values of the new cube to ***X = 25, Y = 40, Z = 25***.
8. Next, plug the **cube** block you just changed into the **rotate** block.
9. Change the rotation of **Y** to ***45*** degree.

  **You will see the wheel, that you can turn to 45 degrees along the axis you selected.**

  **You also can enter the number directly.**

10. Now plug this **rotate** block into the **translate** block.
11. Change the values of **translate** to ***X = -3, Y = 0, Z = 20***.
12. Press **Render**.

  **The roof is on the right place, but there is something wrong!**

  **We need to cutout the lower part of the roof cube, so it really looks like a roof.**

13. Drag and drop one **cube** block from **3DShapes**, one **translate** block from **Transforms**, and one **difference** block from **Set Ops** onto your workspace.
14. First change the values of the new **cube** to ***X = 36, Y = 40, and Z = 20***.
15. Then plug it into the new **translate** block and change its values only in **X** to ***-3***.
16. Press **Render**.

  **This new cube represents the part we want to cut away from the roof cube.**

  **For this we have "difference". It cuts away the 2nd part named "minus" from the 1st part.**

17. Plug the roof **cube** block inclusive **translate** and **rotate** into the first section of **difference**.
18. Then plug the cut out **cube** inclusive its **translate** block into **difference** section named **minus**.
19. Press **Render** again.

  **Did it cut away the lower part? Does it look like a roof now?**

  **Now lets give the house and roof a different color!**

20. Drag n drop two **color** blocks from **Transforms** onto your workspace.
21. Plug in the house **cube** and the roof **difference** block each into one **color** block.
22. Click on the little color squares and choose your own color.
24. Press **Render**.

  **To keep things in check, you can name each of the blocks.**

25. Select each **color** block, click the right mouse button or use finger touch to open a little pop-up menu.
26. Select **Add comment**. You will see a little question mark afterwards.
27. Click on that, and type in ***House*** and ***Roof***.

  **Keep on commenting the parts your create. It is helpful for you and others to understand your designs better, especially when it is going to be more complicated.**

  **Commenting is a very important part in programming. It is like a friend, that whispers you the right message when you need it.**

28. Save your project under **Project >> Save Blocks to your computer**.

<img src="images/2_3_FirstSteps_1.png" width="960"/><br>

---

#### Exercise:

***Use cube, translate, rotate, and difference to add doors, windows, or a chimney to the house.***

**Tips:**

**1. You can use the “+” symbol to add more slots to difference, so it will cut out more boxes at once.**

<img src="images/2_3_FirstSteps_2.png" width="960"/><br>

**2. You can use RMB (Right Mouse Button) or finger touch to open the Pop-up-menu and choose “Duplicate” to get copies of your blocks.**

<img src="images/2_3_FirstSteps_3.png" width="960"/><br>

#### Here an example to help you getting started:

<img src="images/2_3_FirstSteps_4.png" width="960"/><br>

---

#### [Back to Index](#index)

## 2.4. Tea cup <a name="cup"></a>

<img src="images/Cup.jpg" width="512"/><br>

**With this design, I will introduce you to a new method you can use called intersection**

1. Drag and drop one **intersection** block from **Set Ops**, one **translate** and one **scale** block from **Transforms**, one **sphere** block and one **cube** from **3D Shapes**.
2. Lets start with the **sphere** by change the **radius** to ***40***.
3. Plug the **sphere** into **scale** and change the values there to ***X = 0.6, Y = 0.6, Z = 1***.
4. Press **Render**.

  **To create a cup, we want to cut off the top and the bottom.**

  **While "difference" cuts away the blocks that is represented, "intersection" cuts away anything else but the represented part.**

  **Lets build a cube of the area, that will remain after the cut.**

5. Change the values of the **cube** to ***X = 80, Y = 80, Z = 50***.
6. Change the **cube** option ***not centered*** to ***centered***.

  **The block intersection will keep everything, that is covered by the second object under "with".**

7. Plug the **scale** block with the **sphere** into the first position of **intersection**.
8. Then move the **cube** into the second position named **with**.
9. Press **Render**.

  **See!? It should look like a barrel now.**

  **Ok, lets move the barrel up with a translate block.**

10. Connect the **intersection** block with **translate**.
11. Change the **translate** block value **Z** to ***25***.
12. Press **Render**.

  **Next we will use a cylinder to cut out the inside of the cup. Now this is a block you already used. What is it? … difference!**

13. Drag and drop one **cylinder** block from **3D Shapes**, one **translate** from **Transforms**, and one **difference** block from **Set Ops** onto your workspace.
14. Change the values of the **cylinder** to ***radius1 = 18***, and ***height = 50***.
15. Plug the **cylinder** into the new **translate** block.
16. Change the **Z** value of **translate** to ***5***.
17. Now take the **translate** block with the **intersection** and plug it into the first position of the **difference** block.
18. Plug the **translate** block with the **cylinder** in the second one called **minus**.
19. Press **Render**.

  **Last but not least, we need a handle for our cup.**

20. Drag n drop one **torus** block from **3D Shapes**, one **rotate** and one **translate** block from **Transforms**.
21. Change the values of the **torus** to ***radius1 = 12, radius2 = 3, sides = 40, and faces = 20***.
22. Plug the **torus** into the **rotate** and change its **X** value to ***90*** degrees.
23. Connect this to **translate**, and change its values to ***X = 25, Y = 0, Z = 2***.
24. Press **Render**.

  **Ok, you will see the handle on the bottom. The torus goes through the cup, but we want to cut this out as well. We need to implement the torus into the difference block.**

  **You can do this by adding a slot in translate of the intersection block.**

25. Press the **+** sign of the **translate** block in the first position of **difference**, and plug in the entire **torus** block inclusive **rotate** and **translate**.
26. Press **Render**.
27. Add a **color** block from **Transforms**, and plug the entire block section into it.
28. Save your project under **Project >> Save Blocks to your computer**.

<img src="images/2_4_FirstSteps_1.png" width="960"/><br>

---

**Before you create your own objects in the next exercise, I want to show you something, that can become very useful.**

**Lets create a new project, do not forget to save your cup project.**

1. Start wit a new project by pressing **Projects >> New***.
2. Drag n drop one **sphere** and one **cylinder** from **3D Shapes**, one **translate** block and two **sides** blocks from **Transforms** onto your workspace.
3. Plug the **sphere** into **translate** and change the **X** value to ***40***.
4. Then plug **translate** with the **sphere** into one of the **sides** block and change its value to ***4***.
5. Press **Render**.

  **In geometry you call this shape an Octahedron.**

  <img src="images/2_4_FirstSteps_6.png" width="960"/><br>

6. Now plug in the **cylinder** into the other **sides**, and change it to ***3***.
7. Press **Render**.

  **In geometry you call this shape an Triangular Prism.**

  <img src="images/2_4_FirstSteps_7.png" width="960"/><br>

8. Change the value of **sides** to ***5***.
9. Press **Render**.

  **In geometry you call this shape an Pentagonal Prism.**

  <img src="images/2_4_FirstSteps_8.png" width="960"/><br>

10. Change the value of **sides** to ***7***.
11. Press **Render**.

  **In geometry you call this shape an Hexagonal Prism.**

  <img src="images/2_4_FirstSteps_9.png" width="960"/><br>

12. Now click on the **Lock icon** of the **cylinder** block and change **radius2** to ***0***.
13. Change the **sides** value to 4.
14. Press **Render**.

  **In geometry you call this shape a Pyramid.**

  <img src="images/2_4_FirstSteps_10.png" width="960"/><br>

15. Name your project and save under **Project >> Save Blocks to your computer**.
16. Reopen your cup project under **Project >> Load Blocks from your computer**.

---

#### Exercise:

***Create a similar object, e.g. a plate, a different cups, a bowl or other items. You can use the sides block to get different looks.***

**Tips:**

**1. Use a translate block to move your cup aside.**

<img src="images/2_4_FirstSteps_2.png" width="960"/><br>

**2. The rendering time will increase the more sides or polygons you have in the scene. To speed up the rendering reduce "sides" in this tutorial.**

<img src="images/2_4_FirstSteps_3.png" width="960"/><br>

**3. What if you do not need the cup, but you want to keep it on your workspace as reference? Just click the RMB or use your 2 finger touch to open the Pop-up menu, then select "Disable Block".**

<img src="images/2_4_FirstSteps_4.png" width="960"/><br>

**4. You can reverse it the same. It will show "Enable Block".**

<img src="images/2_4_FirstSteps_5.png" width="960"/><br>


---

#### [Back to Index](#index)

## 2.5. Heart <a name="heart"></a>

<img src="images/Heart.jpg" width="512"/><br>

In this tutorial you will learn 3 new blocks: **union**, **hull**, and **mirror**. Lets get started!

1. Start wit a new project by pressing **Projects >> New***.
2. Drag and drop one **cube** block and one **cylinder** block from **3D Shapes**,  one **translate** block and one **rotate** block from **Transforms**, and one **hull** block from **Set Ops** onto your workspace.
3. Change the values of the **cube** to ***X = 10, y = 10, z = 10, centered***, and plug it into the **rotate** block.
4. Change the **Z** value of **rotate** to ***45*** degree, so the cube becomes the edge on the bottom of the heart.
5. Now change the values of the **cylinder** to ***radius1 = 10, height = 10, centered*** and plug it into the **translate** block.
6. Changing the **translate** values to ***X = 7, Y = 12, Z = 0***.
7. Now plug both blocks **cube** with **rotate**, and **cylinder** with **translate** into the **hull** block.
8. Press **Render**.

  **But this is only the half of the heart. How are we going to add the second part?**

9. Drag and drop one **mirror across** from **Transforms**, and one **union** block from **Set Ops**.
10. Change in **mirror across** the value from ***XY*** to ***YZ***.
11. Click onto the **hull** block, open the Pop-up menu, and press ***Duplicate***.
12. Then plug the duplicate into the **mirror** block.
13. Now plug both of the main blocks into **union**.
14. Press **Render**.

  **There you have a full heart!**

15. Name your project and save it.

<img src="images/2_5_FirstSteps_1.png" width="960"/><br>

---

#### [Back to Index](#index)

## 2.6. Key chain <a name="keychain"></a>

<img src="images/KeyChain.jpg" width="512"/><br>

**First, we will create the basic plate**

1. Drag and drop three **cylinder** blocks from **3D Shapes**, three **translate** blocks from **Transforms**, and one **hull** from **Set Ops**.
2. Change the values of the first **cylinder** to ***radius = 10, and height = 2, centered***.
3. Plug it into one **translate** and change the **X** value to ***-30***.
4. Change the values of the second and third **cylinder** to ***radius1 = 2, height = 2, centered***.
5. Plug the second **cylinder** into **translate**, and change the **X** value to ***30***, and the **Y** value to ***8***.
6. Plug the third **cylinder** into the last **translate**, and change the **X** value to ***30***, and **Y** value to ***-8***.
7. Press **Render**.

  **You should see one big disk on one side, and 2 small knobs at the other hand.**

  **We want to create one shape from those three objects.**

8. Click on the **+** symbol on **hull**, and plug all three **translate** blocks with each **cylinder** into **hull**.
9. Press **Render**.

  **See, this is the basic plate!**

  **Now let us add a hole, so we can hook it onto our key ring.**

10. Drag and drop one **cylinder** from **3D Shapes**, one **translate** from **Transforms**, and one **difference** from **Set Ops**.
11. The values for the **cylinder** are ***radius1 = 3, height = 3, centered***.
12. Plug the **cylinder** into the **translate** block, and change the values there to ***X = -33, Y = 0, Z = 3***.
13. Now plug the **translate** block with the **hull** block that contains the other three **cylinder** into the first position of **difference**.
14. Then plug in the **translate** module with the cylinder in the second section of **difference** named **minus**.
15. Press **Render**.

  **Now the keychain plate is finished!**

  **Now you can add your graphics or names to it. Lets continue with a name.**

16. Drag and drop one **translate** from **Transforms**, and one **3D text** from **Text** onto your workspace.

  **In order to cut the name into the plate, we need to add it to difference.**

17. Press the **+** symbol on **difference**.
18. Change the values of **3D text** as follow ***3D text: BlocksCAD, size: 11, thickness: 3, font: Nimbus Sans***.
19. Plug the **3D text** block into **translate** and change those values to ***X = -30, Y = -4, Z = 3***.
20. Then plug **translate** with the **3D text** block into the third section of **difference** named **minus**.
21. Press **Render**.
22. Name your project and save it.

<img src="images/2_6_FirstSteps_1.png" width="960"/><br>

---

<img src="images/IHeartU.jpg" width="512"/><br>

**Lets use the heart we modeled previously to create a 2nd keychain.**

1. Reopen the keychain project (if you closed it).
2. Rename it to ***IHeartU***.
3. Unplug the last **translate** with the **3D text** block by dragging it aside onto your workspace.
4. Drag and drop one **union** block from **Set Ops**, one **translate** and one **scale** from **Transforms**.
5. Plug **union** into the third section of **difference** named **minus**.
6. Now drag your **translate** with the **3D text** block into the first section of **union**.
7. Change the values of this **translate** to ***X = -15, Y = -5, Z = 0***.
8. Change the values of **3D text** to ***3D text: I      U, size: 14, thickness: 10, font: Roboto***.
9. Press **Render**.

  **There are 6 empty spaces between I and U in the text. We want to match the heart between I and U.**

10. Plug **scale** into the remaining **translate** on your workspace.
11. Change the values of **scale** to ***X = 0.4, Y = 0.4, Z = 0.3***.
12. Now change the values of **translate** to ***X = 0, Y = -3, Z = 3***.
13. Drag **translate** with the **scale** block into the second section of **union** named **plus**.

  **Now lets import your heart project!**

14. Go to **Projects >> Import Blocks into Current Project**, select your heart project file and press **Open**.

  **You might need to zoom out to see the imported block!**

15. Drag the block into **scale**.
16. Press **Render**.

  **Your keychain should look like the one in the picture below.**

17. Save your project.


<img src="images/2_6_FirstSteps_2.png" width="960"/><br>

---

#### [Back to Index](#index)

## 2.7. Calculator <a name="calculator"></a>

<img src="images/Calculator.jpg" width="512"/><br>

Lets create a little calculator in BlocksCAD. You will be able to use all blocks in the math section put together math homework. Well a real calculator might be easier, but you can not make a model out of your result :-D.

  **First we build a display background**.

1. Drag n drop one **cube** from **3D Shapes**, one **translate** and **color** from **Transforms**, and one **union** from **Set Ops**.
2. Change the value of **cube** to ***X = 80, Y = 16, Z = 1***.
3. Then plug **cube** into **translate**.
4. Change the values of **translate** to ***X = -45, Y = -4, Z = 0***.
5. Now plug **translate** with **cube** into **color**,and change the color.
6. Press **Render**.

  **You should see a small box that looks like a display.**

  **Now lets put the text "Result: " on top**.

7. Drag and drop one **3D text** from **Text**, one **translate** and one block **color** from **Transforms**, and one **union** from **Set Ops**.
8. Change the values of **3D text** as follow: ***3D text: Result: , size: 10, thickness: 2, font = Open Sans***, and plug it into **translate**.
9. Change the values of **translate** to ***X = -40, Y = 0, Z = 0***.
10. Drag **translate** with **3D text** into the first section of **union**.
11. Now drag this block **union** into **color**. Change the color.
12. Press **Render**.

  **The blank calculator unit is ready! Now we need the actual math part!**

13. Drag and drop one **3D text** block from **Text**, two **1 + 1** blocks from **Math**, and one **set item to** block from **Variables**.
14. Plug one **1 + 1** into the first number place of the second **1 + 1** block, which results in ***1 + 1 + 1***.

  **You can change number, operators or replace blocks with other blocks from Math. This depends on what you want to calculate!**

15. Now drag this math block part into **set item to** block.
16. Click on **item** and choose from the Drop-down menu **Rename**.
17. Rename it to ***Math***.
18. Now take the **3D text** block and plug it into the second part of **union** named **plus**.
19. Drag n drop the new **Math** block under **Variables** onto the first value of **3D text**, where the text suppose to go.

  **Each time you create a new variable, BlocksCAD generates its own blocks "set YOUR_NAME to" and "YOUR_NAME". This is very important for parametric design. We actually will now do our first parametric design.**

20. Change the rest of values of **3D text** as follows ***size: 10, thickness: 2, font: Open Sans***.
21. Press **Render**.

  **You should see 3 now as the result of 1 + 1 + 1. Play a bit around with the numbers and operators to see the results, after you rendered the preview.**

<img src="images/2_7_FirstSteps_1.png" width="960"/><br>


  **And this is what parametric design all about. You can change the numbers in the variable, and the program will adjust the new numbers.**

  **Lets do another little example, so you understand parametric modeling a bit better.**

#### Lets create a pipe that we can change easy

1. Start a new project.
2. Name it ***Parametric pipe***.
3. Drag n drop two **cylinder** from **3D shapes**, one **difference** from **Set Ops** to your workspace.
4. Plug in both **cylinder** into **difference**.
5. Use you right mouse button or finger touch to open the Pop-up window and choose ***External inputs***.

  **This will change the way the block is displayed. As we use variables soon, it would become a very long block. This way, it will easier to work with.**

  <img src="images/2_7_FirstSteps_3.png" width="960"/><br>

6. Drag and drop three **set item to** blocks from **Variables**, two **1 + 1** blocks and three **0** blocks from **Math**.
7. Plug in one **0** block in each **set item to** block.
8. Use the Drop-Down menu of each **set item to**, select **New Variable ...**, and use following names ***length, inner_radius, thickness***.
9. Change the value in **0** of **set length to** to ***20***.
10. Change the value in **0** of **set inner_radius to** to ***30***.
11. Then change the value in **0** of **set thickness to** to ***2***.
12. Now plug in one **1 + 1** block into the **cylinder** for **radius1** of the **cylinder** in the first section of **difference**.
13. In the same **cylinder** plug in the second **1 + 1** block for **radius2**.
14. Drag and drop four **inner_radius** blocks, two **thickness** blocks, and two **length** blocks from **Variables** onto your workspace.
15. Plug in one **inner_radius** block into each first position of **1 + 1** of **radius1** and **radius2** of the **cylinder** in the first section of **difference**.
16. Plug in the remaining two **inner_radius** blocks as **radius1** and **radius2** of the **cylinder** in the second section of **difference** named **minus**.
17. Plug in one **thickness** block into each second position of **1 + 1** of **radius1** and **radius2** of the **cylinder** in the first section of **difference**.
18. Finally, plug in one **length** block for the **height** value of each cylinder.
19. Press **Render**.

 **Now you create a fully parametric design for a pipe!**

Change the values of ***length***, ***inner_radius***, and ***thickness***, while pressing **Render** after the change to see the different outcomes.

<img src="images/2_7_FirstSteps_2.png" width="960"/><br>

---

#### [Back to Index](#index)

## 2.8. Pavilion <a name="pavilion"></a>

Before we go and model the Pavilion, I will introduce you to the use of loops in modeling. Loops are essential in programming. The loop repeats a certain task till it the programm reaches a specified condition.

**In the following example, we will create 10 cylinders positioned side by side throug a loop.**

<img src="images/CountWith.jpg" width="512"/><br>

1. Drag n drop one **count with** from **Loops**, one **translate** from **Transforms**, one **1 + 1** from **Math**, and **cylinder** from **3D Shapes** onto your workspace.
2. Change the values of **cylinder** to ***radius1: 4*** and ***height: 10***, and plug it into **translate**.
3. Drag n drop **i** from **Variables** onto the workspace, then plug it into the first position of **1 + 1** instead of **1**.
4. Change the **+** sign to **x**, and change the second **1** to **10**.
5. Now plug the **i x 10** block into **X** of **translate**.
6. Plug **translate** into **count with**.
7. Press **Render**.

  **The loop counts from 1 to 10 by 1. i is the variable, that contains the current number of the count. Each step the loop creates a cylinder and moves it along the x axis by 10. There are 10 cylinders with a distance of 10 mm along the x axis.**

  **Lets add some colors to the cylinder randomly!**

8. Drag out **cylinder** out of **translate**.
9. Drag and drop one **color HSV** block from **Transforms**, and one **random integer** from **Math** onto the workspace.
10. Plug **random integer** into **hue** of the **color HSV** block, and plug this one into **translate**.
11. Now plug in the **cylinder** block into **color HSV**.
12. Press **Render**.

  **Every time you press Render, the cylinders will be recreated with different colors**.

<img src="images/2_8_FirstSteps_1.png" width="960"/><br>

---

<img src="images/Pavilion.jpg" width="512"/><br>

**Now lets model a pavilion.**

1. Drag n drop three **cylinder** from **3D Shapes**, and two **translate** blocks from **Transforms**.
2. Change the value of the first **cylinder** to ***radius1 = 40, height = 5***.
3. Press **Render**.

  **This is the foundation! Now lets build the roof with the other cylinders.**

4. Change the values of the second **cylinder** to ***radius1 = 38, height = 5***, and plug it into the first **translate**. Then change the values of this **translate** to ***X = 0, Y = 0, Z = 40***.
5. Like in with the umbrella example, click on the **lock symbol** of the third **cylinder** to unlock the 2nd radius.
6. Change the values of this **cylinder** to ***radius1 = 38, radius2 = 1, and height = 20***.
7. Then plug it into the second **translate** block and change its **Z** value to ***45***.
8. Press **Render**.

  **See! The empty space between the foundation and the roof are going to be filled with columns that circles around near the outer edge.**

  **To avoid modeling of each column, we will use a loop, like in the starting example to create all of them at once.**

  **Each cycle of the loop will create one column, and apply a translation and rotation to it.**

9. Drag and drop one **cylinder** from **3D Shape**, one **translate** block and one **rotate** block from **Transforms**, one **count with** from **Loops**, and the just created block **i** in **Variables**.
10. Change the values of the **cylinder** to ***radius1 = 3, height = 40***, and plug it into the **translate** module.
11. Then enter ***30*** as **X** value in the **translate** block.
12. Now we plug this into the **rotate** block, and use the variable block **i** as **Z** of **rotation**.
13. Press **Render**.

  **As you see, there is the first column!**

  **i is a variable that represent a number. We want to build 10 columns, that is going to be positioned near the outer edge. To do this, we need to rotate it with each loop, so the 10 columns get distributed all around the outer edge.**

  **We will do this with the “count with” block. It is already using “i”, so we do not need to change this. The counter will start with 1 and end with 360, which is one full turn. As we want to create 10 columns, we will increase the value “i” by 36 each loop.**

14. Change the values of **count with** to ***from: 1 to: 360 by: 36***.
15. Now plug in the **rotate** block with **translate** and the **cylinder** into the **count with** block.
16. Press **Render**.

<img src="images/2_8_FirstSteps_2.png" width="960"/><br>

#### Exercise:

**Create a little model and multiply it with “count with”. You can use translate in all directions, or different rotations.**

---

#### [Back to Index](#index)

## 2.9. Pine tree <a name="pinetree"></a>

<img src="images/PineTree.jpg" width="512"/><br>

**We will now create a tree, that can be later used to build a forest.**

**With this I will introduce you to the functionality of modules. Modules are like a block box, that contains many many blocks at once. This block box is then represented by a single block.**

**Lets build first the trunk, then the rest with the module we just learned about.**

1. Drag n drop one **cylinder** from **3D Shapes**, and one **color** from **Transforms**.
2. Change the value of the cylinder to ***radius1 = 2, height = 10***.
3. Plug it into the **color** block, and change the **color** to brown.
4. Press **Render**.

  **Now lets do the tree top or pines in an easy form.**

5. Drag and drop one **cylinder** from **3D Shape**, one **translate** and one **color** from **Transforms**, one **count with** from **Loops**,  one **1 + 1** block from **Math**, and the generated **i** block from **Variables** onto the workspace.
6. Change the value of **cylinder** to ***radius1 = 10, radius2 = 1, height = 10***, then plug it into **translate**.
7. Add the **i** variable into the first position of the **1 + 1** block.
8. Change the operator symbol **+** to **x**, and the second number to ***10***.
9. Now plug it into the **Z** value of **translate**, so with each loop the next object created will move up by 10.
10. Now we plug all into the **count with** block.

  **We want to create 3 parts, so we will do 3 loops.**

11. Change the values **count with** to ***from: 1 to: 3 by: 1***.
12. Next plug the whole block you just created into the **color** block, and change **color** to green.
13. Press **Render**.

  **You should see a nice pine tree now!**

  **But it is just one tree. I would like to use the same tree many times, and I do not want to duplicate the tree each time I need a new one.**

  ***All blocks you can choose in BlocksCAD are actually modules, that you used to create your objects. However, you can create your own modules to create customized blocks.***

  **That is why I show you this with the pine tree. We will build a forest, that will use the pine tree module as block.**

14. Drag and drop one **To do something** block from **Modules**, and one **union** block from **Set Ops** onto your workspace.
15. Plug the trunk **color** block inclusive all embedded blocks, and the tree top **color** block with all blocks into the **union** block.
16. Change the **To do something** blocks name ***do something*** into ***Pine Tree***, and plug the **union** block into it.
17. Press **Render**.

  **What!? Nothing to see? That is right, as the Pine Tree is a module now.**

  **As all other blocks, you will need to drag and drop it first onto your workspace.**

18. Drag and drop **Pine Tree** from **Modules** onto your workspace.
19. Press **Render**.

  **The tree should reemerged agian.**

  **We will now save this project and name it, so we can use it in our next project**.

20. Now delete the **Pine Tree** block you just dragged in.
21. Name the project after the name of your module ***Pine Tree*** and save it.

<img src="images/2_9_FirstSteps_1.png" width="960"/><br>


#### Exercise:

***Create similar modules, e.g. flowers, stones, or similar things you find in a forest. Save each separately. Do not forget to erase the module representing block with the module name, before saving.***

**Tip: Create first the object without plugging it into "To do something" block. Only plug it in, before you finished as last step before saving.**

---

#### [Back to Index](#index)

## 2.10. Forest <a name="forest"></a>

<img src="images/Forest.jpg" width="512"/><br>

**Now we will use the module "Pine Tree", we created in the last tutorial to create a forest.**

1. Open a new project and name it ***Forest***.

  **Next we will import our tree module.**

2. Go to **Project >> Import blocks into Current Project**, select ***Pine Tree.xml*** and click **Open**.

  **As we did not do not have the module presenting block on our workspace, pressing Render should show nothing. Lets build the forest ground.**

3. Drag and drop one **cube** from **3D Shapes**, and one **color** from **Transforms**.
4. Change the values of **cube** to ***X = 1000, Y = 1000, Z = 1,  centered***, and plug it into **color**.
5. Now choose the color for your ground by changing it in **color**. I choose a light green.
6. Press **Render**.

  **This is the place we are going to spread a hundred trees randomly. The floor is 1000 from side to side, the random generator will distribute the tree within 800 of both sides.**

  **Lets create 100 trees randomly distributes across the ground!**

7. Drag and drop one **count with** from **Loops**, one **translate** from **Transforms**, two **random integer** from **Math**, and **Pine Tree** from **Modules**.
8. Plug in the **Pine Tree** block into **translate**.
9. Plug in one **random integer** to **X** of **translate**, and one **random integer** to **Y** of **translate**.
10. Change the values of both **random integer** to ***from: -400, to: 400***.
11. Plug in the **translate** block with **Pine Tree** into **count with**.

  **Each time you plug in a new "count with" the index will change. It always starts with "i", then "j", "k", so on. You also can use your own name or variable. We stick with "j" for now.**

12. Change the value **to** in **count with** to ***100***, as we want to build 100 trees.
13. Press **Render**.

  **Be patient! This is quite some task for BlocksCAD.**

  **There is your forest! But all the trees have the same height. Lets create some more variety!**

  **For that, we need to modify our module.**

  14. Press on the gear icon of the **to Pine Tree** block.
  15. Drag n drop **input name** on the left into **inputs** on the right.
  16. Rename the input to ***height***.

  **Now check your module block in "count with j". Can you see the new slot called "height"?**

  **But this is not the only new thing, that got created.**

  17. Drag and drop three **height** from **Variables**, and one **random integer** from **Math** onto your workspace.
  18. Now plug in all three **height** blocks in your **to Pine Tree** module. Two for each **height** value of the two **cylinder**, and one under **translate** instead of ***10*** in **i x 10**.
  19. Plug the **random integer** block into the new slot of the module representing block **Pine Tree** as **height** input.
  20. Change the values of **random integer** to  ***from: 10, to: 18***.
  21. Press **Render**.

  **Again, be patient, it will take a while.**

<img src="images/2_10_FirstSteps_1.png" width="960"/><br>

---

#### Exercise:

***Import the house, pavilion, umbrella, and your own modules into the scene and create an image from your modeled "House in the woods" scene. Move the camera into a nice position and press the camera icon in the 3D viewer to save the image.***

**Tip:**

**1. Reduce the random integer area and tree counts, so you have a free area on the floor.**

**2. Drag and drop translate, rotate, and scale accordingly to move your modules and objects around.**

**3. Disable the representing "Pine Tree" block to avoid the long render times. Can you remember how?... Select the block and press the right mouse button or hold your finger till the Pop-up menu appears. Press on "Disable Block". Do the same thing to enable it again. The choice will be "Enable Block".**

---

#### [Back to Index](#index)

## 3. How to build models for 3D printing? <a name="howfor3d"></a>

Modeling objects you can print with a 3D printer need to follow certain guide lines to assure a successful print.

* Check how small are your objects, or its details. Do not make it too small, as it might get messy or unrecognizable when printed.
* Think of your printed model like a layer cake. The printer is printing your object layer by layer.
* Model your object with the biggest flat surface downwards. Simplest example to think of is a pyramid. Do not model the pyramid with the tip on the bottom.
* Make the wall thick enough for the printer to print, when modeling hollow objects or supporting material (we will do that in the robot examples). Otherwise it will not print those walls.
* Do not model parts of your object too thin, as it can break easily.

---

#### [Back to Index](#index)

## 3.1. Finger Ring <a name="fingerring"></a>

<img src="images/Fingerring1.jpg" width="512"/><br>

Our first object, that you can print is going to be a finger ring.

You can use ruler to measure your finger diameter. It is even easier, if you have got a ring already.

**Lets get started!**

1. As always, lets create a new project and name it ***Fingerring***.

  **First, lets create be ring base.**

2. Drag and drop two **cylinders** from **3D Shapes**, one **translate** from **Transforms**, and one **union** from **Set Ops** onto your workspace.
3. Plug one **cylinder** into **translate**.
4. Change the values of **cylinder** to ***radius1: 16, height: 3***.
5. Change the values of **translate** to ***X: 0, Y: 0, Z: 2***.
6. Now plug the **translate** block with the embedded **cylinder** into the first section of **union**.
7. Plug the second **cylinder** into the second section of **union** named **plus**.
8. Click on the lock icon of this **cylinder** to disconnect **radius2** from **radius1**.
9. Change the values of this **cylinder** to ***radius1: 14, radius2: 16, height: 2***.
10. Press **Render**.

  **You should now see the base of the ring.**

  **Lets carve out the word "fun". But it also can be a graphic like the heart we modeled.**

11. Drag and drop one **3D text** from **Text**, one **translate** block and one **rotate** block from **Transforms**, and one **difference** block from **Set Ops**.
12. Plug in the **3D text** block into **rotate**.
13. Change the values of **3D text** to ***text: fun, size: 19, font: Chewy, thickness: 3***.

  **As you might already recognized, the base of the ring is upside down. This is because of the printing guide. Start with the side, that has the biggest flat surface.**

  **Therefore, we need to turn the text 180 degrees.**

14. Change the **Y** value of **rotate** to ***180*** degrees.
15. To get the text into the center of the base change the values of the **translate** block to ***X: 12, Y: -6, Z: 2***.
16. Press **Render**.

  **It is fun on the base!**

  **Now lets create the actual ring. This is very similar to the previous parametric pipe example.**

17. Drag and drop two **cylinder** from **3D Shapes**, one **translate** and one **rotate** from **Transforms**, and one **difference** from **Set Ops** onto your workspace.
18. Plug in a **cylinder** into the first section of **difference**, and change its value to ***radius1: 13, height: 5***.
19. Plug in the second **cylinder** into the second section of **difference** named **minus**, and change its values to ***radius: 11, height: 3***.
20. Plug **differnence** into **rotate** and change the value of **X** to ***90***.
21. Now move **rotate** into **translate** and change the **Z** value of **translate** to ***16***. This is the height of the rotated ring.
22. Press **Render**.

  **You should see the ring now with base and carved text. However, the edges are still rough. Lets add smoothness to it!**

23. Drag and drop one **sides** block from **Transforms**, and one **union** block from **Set Ops** onto your workspace.
24. Plug in the base part into the first section of **union**, then the ring part into the second section named **plus**.
25. Now drag the entire union block into **sides**. Change the value to ***80***.
26. Press **Render**.
27. Name your project and save it to your computer.

<img src="images/3_1_3DPrints_1.png" width="512"/><br>

---

#### Exercise:

***1. Change the ring inner radius to the variable size and change the outer ring accordingly. This must be connected to the height of the ring as well, which is "Z" of "translate".***

***2. Change the text, font size and font in 3D Text, adjust translate() of the text, so it it is in the center of your button. You might want to try other shapes, or mix of shapes instead of letters.***

<img src="images/3_1_3DPrints_2.png" width="512"/><br>

---

#### [Back to Index](#index)

## 3.2. Necklace <a name="necklace"></a>

In this examples we will use loop again to create nice geometric patterns, that we will use to create necklaces.

<img src="images/Necklace1.jpg" width="512"/><br>

1. First, lets create a new project.
2. Drag and drop two **cylinder** from **3D Shapes**, and one **difference** block from **Set Ops** onto your workspace.
3. Plug in both **cylinder** into **difference**.
4. Change the values of the first **cylinder** to ***radius: 20, height: 4***, and the values of the **cylinder** below to ***radius: 19, height: 4***.
5. Press **Render**.

  **You now should see a nice ring. Well, not really, however we will smooth the edges later.**

  **Now lets add shapes within this outer ring! We start with rings.**

6. Repeat **2.** and **3**.
7. Now change the values of the first **cylinder** to ***radius: 12, height: 4***, and the values of the second **cylinder** to ***radius: 11, height: 4***.
8. Press **Render**.

  **You should see a smaller ring within the bigger ring.**

9. Drag and drop one **translate** and one **rotate** from **Transforms** onto your workspace.
10. Plug in the **difference** block into **translate**, and change the **X** and **Y** value of **translate** both to **5**.

**You should see the ring lining up with the outer ring, while set a bit off.**

11. Now plug this entire block into **rotate**.
12. Change the **Z** value with different numbers, press **Render** each time.

  **The inner ring should now "roll" around the outer ring.**

  **This is what we want to do, but moving around each ring to a different position.**

13. Drag and drop a **count with** block from **Loops**, a **1 + 1** block from **Math**, and the new variable **i** generated in **Variables** onto your workspace.

  **We will do something similar, that we did by building the pavilion.**

  **So what do we want to do? We want to create one ring and rotate each around the center. For 6 rings, each ring needs to rotate: 360 / 6 = 60 degrees.**

14. Plug in the **i** variable into the second position of the **1 + 1** block, and change the values to make it look like **60 x i**.
15. Now plug the entire block **rotate** into **count with**, and change the values as follows >> ***from: 1 to: 6 by: 1***.
16. Press **Render**.

  **See!? A nice flowery pattern.**

  **But how do we attach it to the chain? We need another ring on top of the outer ring.**

17. Go to any of the **difference**, select it, then use your right mouse button or hold your finger on it. Choose **Duplicate** from the Pop-up menu options.
18. Move it down to the bottom of the entire blocks.
19. Drag and drop in a **translate** block from **Transforms**. Change the values to ***X: 22, Y: 6, Z: 0***.
20. Then plug the duplicated **difference** block into **translate**. Change the values of the first **cylinder** to ***radius1: 4, height: 4***, and the values of the second **cylinder** to ***radius: 2, height: 4***.
21. Press **Render**.

  **You should see now a ring on the outer edge of the big ring.**

  **Now lets smoothen the edges again!**

22. Drag and drop one **union** block from **Set Ops**, and one **sides** from **Transforms** onto your workspace.
23. Press the **+** icon on the **union** block to add one more slot.
24. Plug all three block entities **difference**, **count with**, and **translate** one in each slot of **union**.
25. Then plug this **union** block into **sides**, and change the number to ***80***.
26. Press **Render**.

  **Finished! Ready to print. It should looks similar to the image below.**

27. Like always, name and save your project.

<img src="images/3_2_3DPrints_1.png" width="960"/><br>

---

#### Exercise:

**Duplicate your “Count with” block, and disable the original block via RMB/Finger touch “Disable block”. Move the duplicated version below and start experimenting. Try different shapes.**

***Tip:***

***1. Use sides() between translate() and difference() with 3, 4, 6 in order to create different shapes easy. Change the values in translate() to get different results.***

***2. Increase or reduce "to" in "count with", and degrees in "rotate" beneath, to change to outcome of the patterns.***

***3. Again, reduce the number of entire sides (from 80 down to 20 or even lower), when you experiment with shapes. Otherwise, it takes more time to render, and you have to wait longer. Once you are satisfied with your design, increase the sides number.***

#### Squares example:

<img src="images/3_2_3DPrints_2.png" width="960"/><br>

#### Hexagons example:

<img src="images/3_2_3DPrints_3.png" width="960"/><br>

---

#### [Back to Index](#index)

## 3.3. Head for finger puppet <a name="fingerpuppet"></a>

<img src="images/Fingerpuppet1.jpg" width="512"/><br>

**First, we need to measure our index finger to know, what each of us need to put into the values later.**

**For that use your ruler and write down the diameter of your index finger just above the second joint. Measure from front and side and take the bigger number.**

**If there is no ruler around right now, just use the basic values in this tutorial. We are going to model the neck parametric, so you can change it easier later.**

1. First, create a new project.
2. Drag and drop three **cylinder** from **3D Shapes**, one **difference** and one **union** block from **Set Ops**, and one **translate** from **Transforms**.
3. Plug in **union** into the first section of **difference**.
4. Then plug in the **translate** block into the second section of **union** named **plus**, and change the values to ***X: 0, Y: 0, Z: 10***.
5. Now plug in one **cylinder** into the first section of **union**, the second **cylinder** in the **translate** block in the second section of **union**, and one **cylinder** in the second section of **difference** named **minus**.

  **Now lets prepare the neck of your finger puppet to be parametric!**

6. Drag and drop one **set item to** from **Variables**, one **0** block and one **1 + 1** block from **Math** onto your workplace.
7. Click on **item** of the **set item to** and rename it to ***size***.
8. Now drag in the new block named **size** from **Variables** onto your workspace.
9. Plug in **0** into **set size to**, and change it to ***10***.
10. Now duplicate the **size** block once by right clicking or holding your finger on the block till the Pop-up menu opens, so you can select **Duplicate**.
11. Take this duplicate **size** block and plug it into **radius1** of **cylinder** in the second section of **difference** called **minus**.
12. Click on the Lock icon in the same **cylinder**.
13. Then plug in the remaining **size** block into **1 + 1** as first number. Change the rest so it looks like **size - 1**.
14. Now plug **size - 1** into **radius2** of **cylinder** and change **height** to ***50***.
15. Select the **size - 1** in **radius2** of the **cylinder** block.
16. Duplicate it four times, and plug those 4 for each **radius1** and **radius2** of the remaining two **cylinder**.
17. Click on the Lock icon of the **cylinder** in the first section of **union**. Change to values till it looks like ***radius1: size + 2, radius2: size + 1, height: 50***.
18. Now change the values of the **cylinder** of the second section of **union** named **plus** to look like ***radius1: size + 5, radius2: size + 5, height: 4***.
19. Press **Render**.

  **You should have a cylinder looking similar like the pipe example, except there is an extra collar cylinder.**

  **Try a different number in "size", and press "Render" to see the changes. This is where you finger will be***

  **The collar is the part, that holds your puppet clothing in place.**

  **Now we will continue with the head. Follow the instructions, but you can start from here again, if you would like to create your own head.**

20. Press the **+** icon of **union** four time to create four extra slots, which will result in six slots in total.

  **First and second is already used by the neck and the collar. The third is for the head shape, the fourth for the nose, the fifth for the eyes, and the sixth for the ears.**

  **Lets start with the head.**

21. Drag and drop one **sphere** from **3D Shapes**, one **translate** block and one **scale** block from **Transforms** onto your workspace.
22. Change number in **sphere** to ***21***, and plug it into **scale**.
23. Keep **scale** at is ***1, 1, 1***, and plug it into **translate**.
24. Change the values of **translate** to ***X: 0, Y: 0, Z: 46*** and plug it into the third slot of **union**.
25. Press **Render**.

  **Super....Charlie Browns soccer ball head!**

  **Next the nose!**

26. Select **translate** of the head sphere, and click the right mouse button or hold your finger till the Pop-up menu appears.
27. Choose **Duplicate**, and plug the whole block into the fourth slot of **union**.
28. Now change the values of **sphere** to ***21***, of **translate** to ***X: 0, Y: -18, Z: 46***, and of **scale** to ***X: 1, Y: 1, Z: 0.5***.
29. This **translate** block with both duplicates goes into the fourth slot of **union**.
30. Press **Render**.

  **Do not be so nosy...the nose is not the only thing!**

  **Lets carry on with the eyes!**

31. Drag and drop two **sphere** from **3D Shapes**, and three **translate** from **Transforms** onto your workspace.
32. Now plug each **sphere** in one **translate**, then both **translate** blocks with their **sphere** into the remaining **translate**.
33. Press **+** to add one slot to the main **translate**, so both **translate** fit in.
34. Change the value of both **sphere** to ***4***.
35. Then change **X** value of both **translate** above to ***5*** and ***-5*** accordingly.
36. Change the values of the main **translate** to ***X: 0, Y: -17, Z: 49***.
37. This **translate** block goes now into the fifth slot of **union**.
38. Press **Render**.

  **Both eyes should appear. The spheres are identical, except translate is positive 5 and negative 5 for each eye.**

  **Encapsulating blocks is very useful so you can keep it in relation with each other, while being able to use these block as a group.**

  **In this example we can now move around the both eyes with the main "translate" block.**

  **Lets continue with the ears!**

39. Drag and drop two **spheres** blocks from **3D Shapes**, two **translate** block and one **scale** block from **Transforms**, and one **difference** from **Set Ops** onto your workspace.
40. Plug one **sphere** into one **translate** block, and change the value to ***4***.
41. Take this **translate** block with the **sphere** and drag it into the second section of **difference** named **minus**. Change the value **Y** of **translate** to ***-4***.
42. Now plug in the second **sphere** into the first section of **difference**, and change the value to ***5***.
43. Plug this **difference** block now into the remaining **translate** block, and change the values of **translate** to ***X:20, Y:0, Z:46***.
44. Select this **translate** block and duplicate it.
45. In the duplicate, only change the values of the **translate** block on top to ***X:-20, Y:0, Z:46***.
46. Now plug both of these **translate** blocks with their content into **scale** and change the **Y** value of **scale** to ***0.8***.
47. The **scale** block finally can go into the sixth slot of **union**.
48. Press **Render**.

  **Ok, so far so good!**

  **But all is a little minecrafty. Lets smooth it up a little again!**

  **What block did we used last time?**

49. Drag and drop one **sides** from **Transforms** into the scene.
50. Take now the entire **difference** block and plug it into **sides**.
51. Change its value to ***80***.

  **You can use a different number, if you like. Remember, the higher the number, the more squares and triangles in the scene, the longer it will take to render.**

52. Press **Render**.
53. Name the project and save it.

<img src="images/3_3_3DPrints_1.png" width="960"/><br>

---

#### Exercise:

**Create your own head. Start by naming the project differently, so you keep everything we did in this project so far.**

***Tip:***

***1. Draw a doodle of a head first, so you can see in front of you, what you want to create.***

***2. Create only one side of eyes and ears first, then duplicate those parts later on.***

---

#### [Back to Index](#index)

## 3.4. Shoe tag <a name="shoetag"></a>

<img src="images/ShoeTag1.jpg" width="512"/><br>

**The shoe tag has holes to thread your shoe laces. So you can wear it with your shoes.**

1. As usually, lets start with a new project.
2. Name it ***Shoe Tag***.

  **Like the finger ring, we will create the base first.**

  **Also this time, we will build it upside down, as it is better to print it that way.**

  **Lets start with the base. We are going to use "hull", that creates an objects from all objects within this block.**

3. Drag and drop one **cylinder** from **3D Shapes**, one **translate** from **Transforms**, and one **hull** block from **Set Ops** onto your workspace.
4. Click the lock icon on **cylinder** to unlock the connection between **radius1** and **radius2**, and change the values to ***radius1: 11, radius2: 12, height: 7***.
5. Now plug the **cylinder** into the **translate** block, and change its **X** value to ***-18***.
6. Select the **translate** block and duplicate it.
7. Plug in both **translate** blocks into **hull**.
8. Press **Render**.

  **"hull" combined both cylinders to one element. The different radius gives it a little slop.**

  **Now we will cut out a part to make space for a word.**

9. Drag and drop one **difference** block from **Set Ops** onto your workspace.
10. Select the **hull** block group and duplicate it.
11. Plug both **hull** groups into **difference**.
12. Change the values of **cylinder** in the second section of **difference** named **minus** to ***radius1: 9, radius2: 9, height: 3***.
13. Press **Render**.

  **Now you should see an area that is cut out of the base.**

  **Lets add the word "Flower" to it.**

14. Drag and drop one **3D text** block from **Text**, one **translate** and one **rotate** from **Transforms**, and one **union** from **Set Ops** onto your workspace.
15. First lets get **union** ready for all block groups by using the **+** sign to add two more slots, so we have four slots.
16. Plug in the base block group into the first slot.
17. Next, plug in the **3D Text** block into **translate** and change the values to ***3D text: FLOWER, size: 17, thickness: 4, font: Chewy***.
18. Drag this **translate** block with the text into **rotate**, and change the values of **translate** to ***X: -26, Y: -6, Z: -4***.
19. Change the **Y** value of **rotate** to ***180***.
20. Plug the **rotate** block group into the second slot of **union**.
21. Press **Render**.

  **Now the word is in place.**

  **But there is nothing, we can attach it to our shoe laces yet.**

  **Lets do that now!**

22. Drag and drop one **torus** from **3D Shapes**, one **scale** block + one **translate** block + one **rotate** block from **Transforms** onto your workspace.
23. Plug the **torus** block into **scale**, and change its values to ***radius1: 8, radius2: 2, sides: 40, faces: 20***.
24. Drag **scale** with **torus** into **rotate**, and change the values of **scale** to ***X: 0.5, Y: 1, Z: 2***.
25. As next take this **rotate** block and plug it into **translate**.
26. Change the **Y** value of **rotate** to ***90***.
27. Finally change the values of **translate** to ***X: 18, Y: 0, Z: 8***.
28. Press **Render**.

  **We created one side of the lace hole.**

  **Before we continue with the second hole, change the positions of "translate", "rotate", and "scale" within the block group. Render it, after you changed it.**

  **Can you see!? Order matters in this case. The program will start with the lowest instance, which is the "torus", then it does the next action above, up and up as many of those are in on encapsulated group.**

  **Get back the original order, but keep this in mind when you model your own designs.**

  **Back to the second hole for our show laces.**

29. Select the **translate** block group we worked on, and duplicate it.
30. As all stays the same, but the position, change the **Y** value of one **translate** block to ***-18***.
31. Plug both **translate** block groups into the third and fourth slot of **union**.

  **Before we do a final render, lets increase the smoothness through more squares used to create the model.**

32. Drag and drop one **sides** from **Transforms** into your workspace.
33. Plug the entire block group **union** into **sides**, then change its value to ***80***.
34. Press **Render**.

<img src="images/3_4_3DPrints_1.png" width="960"/><br>

---
#### Exercise:

**Add your own words to the shoe tag, or use other shapes to build a graphic.**

---

#### [Back to Index](#index)

## 3.5. Robot 1 & 2 <a name="robots"></a>

#### Robot 1

<img src="images/Robot1.jpg" width="512"/><br>

This robot has the most parts of all the other models we did together.

I will show you, how to make things simpler, if you think first, after you made you sketch of a new thing you want to create.

**So, lets get started!**

1. First open a new project and name it ***Robot***.

  **We will do the body and head, including the antenna.**

  **Lets start with Head of Robby.**

2. Drag and drop one **cylinder** and one **sphere** from **3D Shapes**, one **scale** and two **translate** blocks from **Transforms** and one **hull** form **Set Opts** onto your workspace.

  **We will use hull() to create one shape from a sphere and a cylinder.**

3. Add following values to the **cylinder**: ***radius1 = 17, height = 10***.
4. The **sphere** block has a radius of ***17*** as well.
5. Plug the **sphere** into **scale**, and change the value of **Z** to ***0.7***.
6. Plug the **scale** block into one **translate** block.
7. Change the values of **Z** to ***15***.
8. Render it!

  **So, this are the 2 object we want to combine to one.**

9. Plug **translate** with the sphere in it into the first position of **hull**.
10. Then place the **cylinder** block in the second position called **with**.
11. Render again! See!?
12. Now plug the **hull** block into the second **translate**, so we can put the head in place.
13. Change the **Z** value of **translate** to ***45***.

  **As next, we create the body by duplicating the entire block**

14. Duplicate the entire block you just created.

  **Start to change the values of the copy from up to down.**

15. Change the **Z** value of the top **translate** to ***18***.
16. Change the **Z** value of the **translate** block in first position of the **hull** block to ***19***.
17. Change the radius of the **sphere** to ***14***.
18. Change **radius1** of the **cylinder** in the second slot of **hull** to ***15***, and the **height** to ***15*** as well.
19. Press **Render**!

  **Now you should see the body and head!**

  **Next we will create the antenna on top of the head, and group all together.**

20. Drag and drop on **union** from **Set Opts**, one **cylinder** and one **sphere** from **3D Shapes**, and two **translate** from **Transforms** onto your workspace.
21. Change the values of **cylinder** to ***radius1 = 2, height = 10***.
22. Now plug it into one **translate** and change the **Z** axis to ***71***.
23. Continue with the **sphere** by changing the radius to ***3.5***.
24. Plug it in the second **translate** block, and change its **Z** value to ***83***.
25. Lets group all 4 blocks to one by creating 4 free slots in **union** using the **+** symbol two times.
26. Plug all parts in.
27. Press **Render**.

  **Now you should see an antenna on the head on its torso.**

  **Lets create a module from this entire *union* block.**

28. Drag and drop one **to do something** from **Modules** onto your workspcae.
29. Now, plug the entire **union** block into the **to do something** block.
30. Rename **to do something** into ***to head***.
31. Now drag and drop the new block called **head** from **Modules** onto your workspace.
32. Render it!

  **Now we will create the leg, the arm and the eye on one side, starting with the leg.**

  **As it will look similar to the body and head, you can again create a duplicate of either by clicking on the *to head* module including the *translate* and *hull* blocks.**

33. Change **to head** into ***to left side***.
34. Change the values of **translate** to ***X = 8, Y = 0, Z = 0***.
35. In the second ***translate***, change the values to ***X = 0, Y = 0, Z = 5***.
36. The radius of the **sphere** block becomes ***7***, as well as the **cylinder** **radius1** with a **height** of ***5***.
37. Render it!

  **This is the foot, so we need to add the rest of the leg.**

38. Drag and drop one **cylinder** from **3D Shapes** onto your workspace.
39. Change the values of **cylinder** to ***radius1 = 3***, and ***height = 30***.
40. Now, use the **+** symbol of the foots first **translate** block to get another slot for the leg.
41. Plug in the **cylinder** block.
42. Press **Render**!

  **You should see now the leg with the foot connected to the body.**

  **Lets continue with the arm!**

37. Drag and drop two **sphere** and one **cylinder** from **3D Shapes**, two **translate** from **Transforms**, one **rotate** block from **Transforms**, and one **union** from **Set Opts** onto your work spaces.

  **Let start with the shoulder.**

38. Change to radius of one **sphere** to ***6***, then plug it into **rotate**.
39. Change the **Y** Axis to ***80*** degrees.

  ***You need to write it in, as the wheel does not have 80 degree to set.***

40. Next, plug the **rotate** block into a **translate** block, and change to values there to ***X = 14, Y = 0, Z = 37***.
41. For the rest of the arm, take the other **translate** and use **+** symbol to add a second slot.
42. Change the values of that **translate** module to ***X = 18, Y = 0, and Z = 20***.
43. Take the second **sphere** and change its radius to ***5***, then plug it into one slot of that **translate** block with 2 slots.
44. Now change the values of **cylinder** to ***radius1 = 3, and height = 16***.
45. Then plug it into the second **rotate** block. The **Y** Axis here needs to be ***350*** degrees.
46. Now plug in the **rotate** block with the **cylinder** into the second free **translate** slot.
47. Then group the shoulder and arm with hand together by plugging it all into the **union** block. Add one slot for **union** using **+**.

  **Now we create the eye on that side!**

48. Drag and drop one **sphere** and one **cylinder** block from **3D Shapes**, two **translate** + two **rotate** + one **scale** from **Transforms** onto the workspace.
49. Lets start with changing the **cylinder** values to ***radius1 = 5, height = 8***.
50. Plug the **cylinder** block into one **rotate** block.
51. Change the **X** rotation to ***90*** degrees.
52. Then plug it into one **translate** block, and change that values to ***X = 7, Y = -10, Z = 54***.
53. Add another slot to this **translate** block by using the **+** symbol.

  **We will build now the other block, that is going to plugged in there**.

54. Change the radius of **sphere** to ***4***, then plug it into the second **rotate** block.
55. Change the **X** rotation to ***90*** degrees, and plug this into the **scale** block.
56. The **scale** value of **Y** needs to be change into ***0.6***.
57. Plug this module containing the **sphere** into the second **translate** block.
58. Change there the **Y** value to ***-8***.
59. This block goes now into the free slot of the first **translate** block.
60. Now lets group the leg, the arm, and the eye using the leftover **union** block. You need to add one free slot.
61. Drag and drop the new block **Left side** from **Modules** onto the workspace.
62. Now press **Render**.

  **Now you should see the eye as well with all other parts on one side.**

  **We do not need to model everything again for the other side.**

63. Just drag and drop one **Mirror across XY** from **Transforms** into the workspace.
64. Click on **XY** and change it into ***YZ***.
65. Then make a duplicate of the **Left Side** block.
66. Plug it into the **mirror** block.
67. Render it!

  **The robot is finished!**

  **But hold on, we want to have the robot a bit smoother. So we group all together and use sides again!**


68. Drag and drop one **union** from **Set Opts**, and one **side** from **Transforms** into the space.
69. Use the **+** button to add one slot and add all 3 module blocks **head**, **Left side** and **mirror** with **Left side** into **union**.
70. Now plug the **union** block into **sides**, and change the value to ***40***.
71. Render it!

  **A voila!**

<img src="images/3_5_3DPrints_1.png" width="960"/><br>

---

#### Robot 2

<img src="images/CuteRobot1.jpg" width="512"/><br>

Here is a second robot, that looks a bit like the robot Marvin from "Hitchhiker's Guide To The Galaxy".

  **Lets start with the head!**

1. Drag n drop one **sphere** from **3D Shapes**, one **translate** and one **rotate** from **Transforms** onto your workspace.
2. Set the value of **sphere** to ***17***, and plug it into **rotate**.
3. Change the values of **rotate** to ***X: 5, Y: 355, Z: 0***, and plug it into **translate**.
4. Change the values of **translate** to ***X: -2, Y: 0, Z: 48***.
5. Press **Render**.

  **Now lets create the eyes.**

6. Duplicate the entire block you just created for the head.
7. Change the values as follow: **sphere** to ***3***, **rotate** to ***X: 0, Y: 90, Z: 90***, and **translate** to ***X: 0, Y: -18, Z: 45***.
8. Duplicate this entire block again, keep the **sphere** value, while changing the values of **rotate** to ***X: 0, Y: 90, Z: 60***, and **translate** to ***X: -14, Y: -13, Z: 44.5***.
9. Now drag and drop one **union** block and one **difference** block from **Set Ups** into your workspace.
10. Plug both eye **sphere** blocks into the **union** block.
11. Then plug in this **union** block into the second slot of **difference** called **minus**.
12. Plug the head **translate** block into the first slot of **difference**.
13. Press **Render**.

  **This is the head that has now two eyes carved into it!**

  **Let us continue with the torso sphere!**

14. Drag and drop one **sphere** from **3D Shapes**, and one **translate** from **Transforms** onto your workspace.
15. Set the value of **sphere** to ***15***, and plug it into the **translate** block.
16. Change the values of **translate** to ***X: 0, Y: 2, Z:20***.
17. Press **Render**.

  **Now lets create the foot of our little robot, starting with the leg!**

  **Again, we will later use *mirror* for the second leg.**

18. Drag and drop one **cylinder** from **3D Shapes**, one **rotate** and one **translate** block from **Transforms** onto your workspace.
19. Change the value of **cylinder** to ***radius1: 2.5, height:8***, and plug it into **rotate**.
20. Change those values to ***X: 0, Y: 330, Z: 0***.
21. Then plug **rotate** into **translate** and change the values of **translate** to ***X: 11, Y: 2, Z: 2***.
22. Press **Render**.

  **The leg is done. Now we will create a foot by using a sphere and cutting it into half.**

23. Drag and drop one **sphere** as well as one **cube** from **3D Shapes**, two **translate** blocks + one **rotate** block + one **scale** block from **Transforms**, and one **difference** block from **Set Ops** onto your workplace.
24. Change the value of **sphere** to ***12***, and plug it into **scale**.
25. Change the values of **scale** to ***X: 0.6, Y: 1, Z: 0.6***, and plug it into the first slot of **difference**.
26. Take the **cube** block, change the values to ***X: 30, Y: 30, Z: 10, centered***, and plug it into the second slot of **difference**.
27. Plug in the entire **difference** block into **rotate**.
28. Change the **Z** value of **rotate** to ***30***.
29. Then plug **rotate** with **difference** into **translate**, and change the **X** value of **translate** to ***11***.
30. Press **Render**.

  **Now you should see one leg and foot on one side.**

  **Lets build a module of the leg and mirror it!**

31. Drag and drop one **union** block from **Set Ops**, and one **to do something** block from **Modules** onto the workspace.
32. Rename the **to do something** block to **to Leg**, and drag in the new generated **Leg** block from **Modules** onto your workspace.
33. Now plug the block collection **translate** of the foot, and **translate** of the leg into **union**, then **union** into **to Leg**.
34. Press **Render**.

  **Now the leg is ready to get mirrored**

  ***If you can not see the leg, then your forgot to drag in the "Leg" block from "Modules"!***

35. Now duplicate **Leg** one time, and drag and drop **mirror across** from **Transforms** onto the your workplace.
36. Change the value of **mirror across** to ***YZ***, and plug one **Leg** into it.
37. Press **Render**.

  **You should see now 2 legs with feet on the right positions.**

  **Ready to create another module for the arm? Lets go!**

38. Drag and drop one **cylinder**, one **cube**, and one **sphere** from **3D Shapes** onto your workplace.
39. Furthermore, drag and drop one **scale**, two **translate**, and two **rotate** blocks from **Transforms** into your space.
40. Finally drag and drop one **union** and one **difference** block from **Set Ops** + one **to do something** block from **Modules** onto your workspace.
41. Rename **to do something** to ***to Arm***, and plug in **union**.
42. Change the values of **cylinder** to ***radius1: 2.5, height: 8***, and plug it into the second slot of **union**.

  **Now lets create something similar to the foot. This time we will use it as arm.**

43. Change the value of **sphere** to ***12***, and plug it into **scale**.
44. Set the values of **scale** to ***X: 0.6, Y: 1, Z: 0.6***, and plug it into the first slot of **difference**.
45. Now change the values of **cube** to ***X: 30, Y: 30, Z: 10, centered***, and plug it into **translate**.
46. Change the value **Z** of **translate** to ***-5***, and plug this one in the second slot of **difference** called **minus**.

  **We need to move the arm up a bit, as the center of the arm will be the bottom of the cylinder.**

47. Plug **difference** into the last **translate**, and change those values to ***X: 0, Y: -6, Z: 8***.
48. Now plug in **translate** block into the first slot of **union**.
49. Drag and drop the **Arm** block that got generated from **Modules** onto your workspace.
50. Press **Render**.

  **Well, that looks odd! Does it not?**

  **Lets move the arm to a nice position**

51. Drag and drop one **translate** block and one **rotate** block from **Transforms** onto your workplace.
52. Take the **Arm** block, and plug it into **rotate**.
53. Change the values of **rotate** to ***X: 255, Y: 210, Z: 24***, and plug it into **translate**.
54. Now change those values to ***X: -7, Y: 2, Z: 24***.
55. Press **Render**.

  **See!? Now lets use the module "Arm" for the other side, but lets change the position a bit to make it look a bit more interesting.**

56. Duplicate the **translate** block including **rotate** and **Arm**.
57. Drag and drop **mirror across** from **Transforms** into the workspace, and change the value to ***YZ***.
58. Plug in the arms duplicate, and change the **rotate** values to ***X: 0, Y: 300, Z: 330***.
59. Press **Render**.

  **Well, there is Marvin. But hey, lets smooth the surface for it. The print will look much better!***

60. Drag and drop one **union** block from **Set Ops**, and one **sides** block from **Transforms** onto your workspace.
61. Use the **+** symbol to add four more slots to **union**.
62. Plug in the head, torso, both arms and both legs into the **union** block.
63. Then plug in the entire **union** block into **sides**, and change its value to ***80***.
64. Press **Render**.

  **Be patient, this are lot of faces BlocksCAD needs to render.**

  ***If you want to work on the pose of your robot, change the "sides" value down to 12 or so.***

  ***This will shorten the render time tremendously!***

<img src="images/3_5_3DPrints_2.png" width="960"/><br>

---

#### [Back to Index](#index)

## 4. Print it out! <a name="printing"></a>

This section I will introduce you to the actual process of 3D printing.

Before we print a 3D object, that has been exported as STL file, we will need to slice it into layer and exported into a script.

The script is also called GCode, which gives the printer the exact information where to move, how fast and many other infos.

The slicing is depend on factors we will adjust with a software, which can be accessed via a browser or installed on your computer.

After you successful generated the GCode of your object, I will show you how to control the printer to print it out.

---

#### [Back to Index](#index)

## 4.1. My 3D Printer <a name="3dprinter"></a>

The printer I choose is as a beginners printer for kids is the EasyThreedX2.

<img src="images/4_1_PrintIt_1.png" width="960"/><br>

* The printer is easy to use and has pretty good reviews.
* It is a little printer, that has a 10 by 10 by 10 cm printing space and uses only 30 Watt of energy consumption. It has amazing printing results for its low price.
* If something is not working, it is easy to fix (out of my own experience).
* It is for PLA Plastic, a starch based Bio-plastic.
* With its light-weight and small size, it fits in bags to take it to school or friends.

<img src="images/4_1_PrintIt_2.png" width="960"/><br>

---

#### [Back to Index](#index)

## 4.2. Prepare for printing <a name="prepareprint"></a>

Checklist for the printer:

* Do you have enough filament on your spool?
* Did you replace the blue tape, that can not be reused?
* Is the print head releasing plastic when preheating?
* Is your print plate clean?


### 4.2.1 Use kiri:moto website to slice objects

This section shows you, how to use the online STL to GCODE slicing using a web browser application.

[kiri:moto - Free to use](https://grid.space/kiri/)

  <img src="images/kirimoto.png" width="960"/><br>

* Use following settings, if you have purchased an EasyThreedX1 or EasyThreedX2 printer:
* The web browser will save your settings on your device, and use it automatically next time you open this url.
* Therefore, the next steps only need to be done once.
* However, there are settings in infill and support, that might change depend on your object you want to print.

  <img src="images/4_2_1_browserslicer_kirimoto_1.png" width="960"/><br>

* Lets start with the setup by choosing ***>> Setup >> Machine***.

  <img src="images/4_2_1_browserslicer_kirimoto_2.png" width="960"/><br>

* Choose the standard devices ***Creality Ender 6***, as it is the closed to EasyThreeD printers.
* Then press ***Customize***.

  <img src="images/4_2_1_browserslicer_kirimoto_3.png" width="512"/><br>

* Now you can change the name to create your own Device. You will see any customized printer in the list **My Devices**.
* Next, change the Width, Depth, and Height to ***100***.
* Press ***Save***.

  <img src="images/4_2_1_browserslicer_kirimoto_4.png" width="512"/><br>

* As you can see, the printing space is has changed.
* Now go to the right side panel and choose the section step-by-step.
* Change the values as shown in the pictures below.
* This will setup the printing settings.

  <img src="images/4_2_1_browserslicer_kirimoto_5.png" width="960"/><br>

* **Layers:** Here you can choose how fine the print is going to be printed.
* The higher the layer the more rough the print, and the shorter is the printing time.
* The lower the layer height the finer the print, but the longer is the printing time is going to be.

  <img src="images/4_2_1_browserslicer_kirimoto_6.png" width="256"/><br>

* **Base:** is all about the bottom of the print and printing speed.
* Many of the values are **0**, which will then be the default values set in other settings sections. e.g. Nozzle temperature in **Output**
* E.g. change the temperature, if you would like to print lower or higher than 210 degree Celcius.

  <img src="images/4_2_1_browserslicer_kirimoto_7.png" width="256"/><br>

* **Infill:** dedicates the filling of your object, what pattern and how dense it is going to be printed.
* With **Fill Type** you decide what pattern is the filling going to be.
* With **Fill Fraction** you will decide the density (0: No filling, 1: Full fill)

  <img src="images/4_2_1_browserslicer_kirimoto_8.png" width="256"/><br>

* **Support:** Sets the values of the supporting structure.
* Here you can experiment mainly with **Density**, and **Pillar Size**.
* Press **Detect** to see the estimation of the supporting material as soon you loaded your object.

  <img src="images/4_2_1_browserslicer_kirimoto_9.png" width="256"/><br>

* **Output:** are the values of nozzle temperature, fan speed, and printing speed.
* You will not change these values much.
* Do not go over a printing time of 40 mm/s in **Print Speed**, as this is the maximum printing speed of the EasyThreeD X1/2 printer.

  <img src="images/4_2_1_browserslicer_kirimoto_10.png" width="256"/><br>

* **Expert:**

  <img src="images/4_2_1_browserslicer_kirimoto_11.png" width="256"/><br>

* **Profile:** saves your printing settings, so you can call it in the future.

  <img src="images/4_2_1_browserslicer_kirimoto_12.png" width="960"/><br>

  **Now that the printer settings are applied, lets import an object and slice it:**

1. Go to **>> Files >> Import**, and choose your object file in .stl format.

  <img src="images/4_2_1_browserslicer_kirimoto_13.png" width="960"/><br>

2. It should show up in the center of your printing space. You can import more objects, that will then rearrange on the printing platform.

  <img src="images/4_2_1_browserslicer_kirimoto_14.png" width="960"/><br>

3. In the example of Marvin, you will need support printed. Choose **>> Support >> Detect** to see the estimation. Test different values, if you have too little or too much support material.

  <img src="images/4_2_1_browserslicer_kirimoto_15.png" width="960"/><br>

4. Now go to **>> Slice >> Preview**, and click to start to render a preview.

  <img src="images/4_2_1_browserslicer_kirimoto_16.png" width="960"/><br>

  **The preview rendering will calculate every print layer according to the printing settings in your profile.**

  <img src="images/4_2_1_browserslicer_kirimoto_17.png" width="960"/><br>

5. Use the slide on the bottom to check on infill density and support material.

  **Check, if it is printable (nothing starts in thin air, but is build from below up)**

  **This option can be slow, as this will stress your devices graphics. Be patient. Move the slider a bit, then wait till the view is updated!**

  <img src="images/4_2_1_browserslicer_kirimoto_18.png" width="960"/><br>

  **If you are satisfied with the rendered preview, you can export it as .gcode file.**

6. Press **>> Slice >> Export**.

  <img src="images/4_2_1_browserslicer_kirimoto_19.png" width="960"/><br>

  **Now it will again calculate each layer, then open a Pop-up window with details, and an export button.**

7. Press ***Download***, and save the file.

  <img src="images/4_2_1_browserslicer_kirimoto_20.png" width="960"/><br>


**Next you can proceed with section 4.3.**

---

### 4.2.2 Use slic3r software to slice objects

If you have a PC or Laptop, you can use slic3r to prepare your exported 3D object for 3D printing.

[Slic3r - Free open-source software](https://slic3r.org/download/)

  <img src="images/slic3r.png" width="960"/><br>

#### Import the configuration file for your EasyThreedX2 printer (only once)

1. Open slic3r
2. Import the configuration file [Slic3r_config_EasyThreedX2.ini](/slic3r/files/EasyThreedX2.ini) by choosing **File >> Load Config**.

  <img src="images/4_2_PrintIt_1.png" width="512"/><br>

3. Now you should see the configuration in the right side panel of Slic3r.

  <img src="images/4_2_PrintIt_2.png" width="960"/><br>

  **Lets go through the most important settings. I will point you at settings, that you can change. This will be mostly in "Print settings".**

  ***If you use a different printer, you might need to change the settings and build your own configuration profile!***

4. Click on **Print settings**:

* This is the ***How to print*** section.
* Most important are the first 4 subsections **Layers and perimeters**, **Infill**, **Skirt and brim**, and **Support material**.


  * Layers and perimeters:

      <img src="images/4_2_PrintIt_3.png" width="512"/><br>

      * Layer height >> Indicates thickness of each layer printed (0.2).
      * First layer height >> Thickness of the first layer, that is often thicker than all other layers (0.25).
      * **The lower the layer height the finer your print result, but the higher the printing time.**


  * Infill:

    <img src="images/4_2_PrintIt_4.png" width="512"/><br>

      * Fill density >> Sets the density of the material within the printed object (20 %).
      * **0 % density will keep the object hollow, thus only print the walls.**
      * **100 % density will print the object as a full solid.**


  * Skirt and Brim:

    <img src="images/4_2_PrintIt_5.png" width="512"/><br>

      * Loops >> will print a band around the bottom layer of the object.
      * **Set to 0 will switch it off. Otherwise the loops will be higher than 1.**
      * Distance from object >> is the distance between skirt and objects
      * Skirt height >> how many layer will be printed
      * Brim is a method to prevent wrapping of printed material when cooling down.
      * Brim width >> how many mm width should go around the objects bottom layer


  * Support material:

    <img src="images/4_2_PrintIt_6.png" width="512"/><br>

      * Generate support material >> on / off activation
      * Overhang threshold >> Degree that dedicates, when to start adding support.
      * Pattern >> in what shape will the support material be printed.
      * Pattern spacing >> distance between pattern support material


  * Speed:

    <img src="images/4_2_PrintIt_7.png" width="512"/><br>

    * Do not go over 40 mm/s in printing speed. This is the maximum the EasyThreedX2 can do!
    * Travel speed is ok to be the double.


   5. Click on **Plater** to get back to the 3D view, then click **Filament**:


  * Filament:

    <img src="images/4_2_PrintIt_10.png" width="512"/><br>

    * Filament thickness will stay 1.75 mm with EasyThreedX2.
    * If there are problems with printing you can try to change temperature, as the optimum is depend on the filament quality of PLA.


  * Cooling:

    <img src="images/4_2_PrintIt_11.png" width="512"/><br>

    * Also here, best is to leave it.
    * But sometimes it is better to switch off the cooling fan, or to keep it going all the time.


6. Click on **Plater** to get back to the 3D view, then click **Printer**:


  * General:

    <img src="images/4_2_PrintIt_12.png" width="512"/><br>

    * Bed shape stays the same with 10 x 10 cm.

    <img src="images/4_2_PrintIt_13.png" width="512"/><br>

  * Extruder 1:

    <img src="images/4_2_PrintIt_14.png" width="512"/><br>

    * Settings stay the same for EasyThreedX2

7. Each time you change a value in all 3 main sections, the name will be automatically to ***name(modified)***.
8. Click the floppy disk symbol to save the changed version.

    <img src="images/4_2_PrintIt_8.png" width="135"/><br>

   *This comes handy, if you want to use a version with different fillings, with or without support materials s.o.*


9. Rename it before you save, so you know, what has changed.

    <img src="images/4_2_PrintIt_9.png" width="256"/><br>


The configuration is saved, and will be called each time you start Slic3r.

#### Slice your model in Slic3r

1. Open slic3r
2. Press the **Add** button and choose your .stl file and open it.

  <img src="images/4_2_PrintIt_15.png" width="960"/><br>

3. Make sure you have all 3 parts of the EasyThreedX2 configuration set.

  <img src="images/4_2_PrintIt_2.png" width="960"/><br>

4. Press **Preview** to see settings executed on your object.
5. Use the vertical slider to move through each layer. Check if there are impossible print situations (like the printer starts printing in middle of nowhere)
6. If everything looks good (also check, if you need support or not), the press **Export G-code**.

  <img src="images/4_2_PrintIt_16.png" width="960"/><br>

7. Save it.

**Thats it! Next we will go through the possible ways to control the printer making it print the object using the gcode file.**


---

#### [Back to Index](#index)

## 4.3. How to print it? <a name="howtoprint"></a>

Now, that we have our gcode file, I will go through 3 ways to actually print your object with the EasyThreeD X2 printer.

#### First things first

No matter which way you will control the printer. It is good to know how to do the basic procedures with the EasyThreeD X1/2 printer.

First the basic setup of the printer, which you only need to do occasionally:

1. Use blue tape (or other paper tape) to protect your magnetic print bed surface. This is optional, but highly recommended.

  <img src="images/BlueTapeOnPrintBed.jpg" width="512"/><br>

2. Level your bed to prevent the nozzle scratching into the print bed, or distance too far from the print bed.

* Take a piece of printing paper, and put it onto your printers printing bed. You might cut the A4 paper in half.
* Go in the menu to ***>> Prepare >> Auto home***. The printer head is going into the starting position.

  <img src="images/4_3_1_printer_preparation_1.png" width="128"/><br>

* Then move one down in the menu and press ***>> Disable Steppers*** to be able to move the print bed and printer head around with your hands.

  <img src="images/4_3_1_printer_preparation_2.png" width="128"/><br>

  **Start with the screw underneath the starting position, so you do not need to move anything for now.**

* Try to move the piece of printing paper.

  **You should feel a soft resistance when moving the paper. You can use both hands holding both ends of the paper to make it easier to move.**

  <img src="images/4_3_1_printer_preparation_3.png" width="960"/><br

* Turn the screw in one or the other direction, if you can not move the paper or it is super easy to move.

  <img src="images/4_3_1_printer_preparation_4.png" width="960"/><br>

* Go through the next 3 corners and do the same thing.

  **Be careful, be gentle when moving the print bed and printers head, even if it is not so easy to move.**

*After you did the leveling once, you barely need to repeat the procedure. It will be necessary once in a while depend on room temperature, or after you renewed the blue tape.*

#### *Blue tape set? Printer filament ready? Lets go!*

<img src="images/4_3_1_printer_preparation_5.png" width="960"/><br>


#### *Ok, lets feed your printer with filament!*

  **First, we want to move the printing head up so we can see the incoming filament and protect the printing bed from overheating.**

The first screen you will see, after you start the printer is called ***Info Screen***.

1. Click once to open the main menu, using the knob that has an embedded button. You can turn the knob and press it.
2. Click through following path: ***>> Prepare >> Move Axis >> Move Z >> Move 10 mm >>***.

  <img src="images/4_3_1_printer_preparation_7.png" width="128"/><br>

  <img src="images/4_3_1_printer_preparation_8.png" width="128"/><br>

  <img src="images/4_3_1_printer_preparation_9.png" width="128"/><br>

  <img src="images/4_3_1_printer_preparation_10.png" width="128"/><br>

  <img src="images/4_3_1_printer_preparation_11.png" width="128"/><br>

3. Now turn the wheel clock-wise 2 - 3 steps.

  **The head will move up 20 - 30 mm. This should be enough.**

4. Go up to the first option in the sub menu and click to move back up in to the main menu.

  **If you wait for a while, the menu will reset automatically.**

  **Next, we want to add, or replace the filament.**

5. Go to ***>> Prepare >> In/Out Filament >>***.

  <img src="images/4_3_1_printer_preparation_7.png" width="128"/><br>

  <img src="images/4_3_1_printer_preparation_13.png" width="128"/><br>

  **As soon your press In/Out Filament the printer will start heating up its printer head to 200 degree Celsius.**

  **Do not touch the nozzle :-)**

6. Now choose the option you want to do:

  ***In Filament*** >> The extruder of the printer head will move the filament towards the nozzle, as soon 200 Degree Celsius is reached.

  ***Out Filament*** >> The extruder of the printer head will move the filament away from the nozzle, as soon 200 Degree Celsius is reached.

  ***Stop In/Out Filament*** >> Stops the extruder motor and the heater, thus cooling down the nozzle.

  **As soon you press In Filament or Out Filament you will see the temperature on top of the display.**

7. When the nozzle temperature reaches 200 degree Celsius, and you hear the Extruder motor moving:

   **Push a bit the filament till you feel it gets drawn in.**

   **On the other hand pull it a bit when removing the filament till it releases it from the Extruder.**

  <img src="images/4_3_1_printer_preparation_12.png" width="512"/><br>

8. Press Stop In/Out Filament as soon the filament comes out of the nozzle, or the filament is released from the Extruder.

  **Make sure the nozzle is clean and no filament is sticking out, before you offset the printer head to X: 0, Y: 0, Z: 0 by choosing *Auto home*.**

  **Use a piece of paper or a tweezers to remove any filament that sticks out.**

  **Do it before it cools down, as it will be more difficult to remove it by then.**

Now that the filament is in, and the printing bed is leveled, lets print!


#### 1. First way....just use an SD card


* Check, if you have enough filament in your printer. Is it the color you want to use?

1. Go through the menu as follows ***>> Prepare >> Auto home***.

<img src="images/4_3_1_printer_preparation_7.png" width="128"/><br>

<img src="images/4_3_1_printer_preparation_1.png" width="128"/><br>

  **This will set your printers head to X: 0, Y: 0, Z: 0, which is in the corner on the bottom Left.**

2. Stick in your SD card with the .gcode file of your sliced object.

  <img src="images/4_3_1_sdcard_print_2.png" width="512"/><br>

3. Go to ***>> Print from SD >> your_object.gcode***.

  <img src="images/4_3_1_sdcard_print_1.png" width="128"/><br>

  ***your_object.gcode represents the filename of your file.***

4. Click on your file.

  **And up it goes!**

* The printer nozzle will heat up to the temperature you choose, and start printing.
* You can pause or stop the print in case something happens.

---

#### 2. Use Slic3r to control printer

In this section, you will learn how to use Slic3r to control your printer, print your object directly.

You need to connect your printer via USB to your Laptop/PC first.

#### Setup

The next steps you only need to do once in order to set up Slic3r as a print controller.

* First go to **File >> Preferences...**, and activate ***Show Controller Tab***.
* Press **OK**, and restart Slic3r.

<img src="images/4_3_2_slic3r_print_1.png" width="360"/><br>

* Make sure the EasyThreedX configuration is imported, then go to **Printer:**

<img src="images/4_3_2_slic3r_print_2.png" width="960"/><br>

* Check, if your printer is recognized under **Serial port:**.
* Use the drop-down menu in case you have different USB devices plugged in.
* Check the **Speed:** option. It should be ***115200***.
* Press **Test** to see, if your printer is going to connect to the computer.

<img src="images/4_3_2_slic3r_print_3.png" width="960"/><br>

**If your printer is connected, you are ready to print your object from within Slic3r.**

#### Print

First, make sure your printer is on, and connect to your Laptop/PC.

1. Press **>> Open STL/OBJ/AMF/JMF...**, or press **Add**.

  **Check via Preview, if all is good with support material and infill.**

2. Press **Print...**

  <img src="images/4_3_2_slic3r_print_4.png" width="960"/><br>

3. Click on the tab **Controller**.
4. There you will find **Manual control**. Press this button.

  **This will open a window, in which you have manual control options for your printer (need to be connected).**

  <img src="images/4_3_2_slic3r_print_5.png" width="960"/><br>

5. Type in ***210*** in **Extruder** and press **Set**.
6. Close the window.

  <img src="images/4_3_2_slic3r_print_6.png" width="512"/><br>

  **You can see the temperature raising?**

7. Press **Print This**.

  <img src="images/4_3_2_slic3r_print_7.png" width="960"/><br>

  **The printer should now start printing**

  **The printing process is indicated under "Printing..."**

  <img src="images/4_3_2_slic3r_print_8.png" width="960"/><br>


---

#### 3. Use GCodePrintr on an Android Tablet

This section will show you how to use an Android Tablet as printer control device.

#### Setup

* First and foremost you will need an adapter, that makes it possible to connect your printer to a mini usb socket on your tablet.

  One example of an adapter:

  <img src="images/OTG_USB_Adapter.jpg" width="256"/><br>


* Next, you will need to buy this software in Google Play, and install it on your device:

  [GCodePrintr](https://play.google.com/store/apps/details?id=de.dietzm.gcodesimulatorprinter&hl=en_US&gl=US)

* Now connect your printer to your tablet, and start GCodePrintr.

  <img src="images/4_3_3_gcodeprintr_1.png" width="960"/><br>

* Now press the **Gear** symbol in the left panel to open **Settings**.

  <img src="images/4_3_3_gcodeprintr_2.png" width="960"/><br>

* First, lets setup the printer settings by pressing **Settings >> Printer**

  <img src="images/4_3_3_gcodeprintr_3.png" width="960"/><br>

* Press **Bed size**, and change it to ***100***.
* Press **OK**.

  <img src="images/4_3_3_gcodeprintr_4.png" width="360"/><br>

* Press **Heatbed Temperature Preset 1**, and type in ***185***.
* Press **OK**.
* Press **Heatbed Temperature Preset 1**, and type in ***210***.
* Press **OK**.
* Press **Heatbed Temperature Preset 3 / Max Temperature**, and type in ***230***.
* Press **OK**.

  <img src="images/4_3_3_gcodeprintr_5.png" width="360"/><br>

* Now press on **Material**, and press ***PLA (1.25g/cm3)***.

  <img src="images/4_3_3_gcodeprintr_6.png" width="360"/><br>

* Use the backward triangled symbol 2 times to get back to **Settings**.
* Then press **Communication**.

  <img src="images/4_3_3_gcodeprintr_7.png" width="960"/><br>

* Press on **Printer Connection Type**, and choose ***USB-OTG***.

  <img src="images/4_3_3_gcodeprintr_8.png" width="360"/><br>

* Go back, then click on **Baud rate for USB OTG**.
* Choose ***115200***.

  <img src="images/4_3_3_gcodeprintr_9.png" width="360"/><br>

**Thats about it! The printer should work now properly under control of GCodePrintr!**


#### Print

1. Press on the folder icon in the left panel to import your file.

  <img src="images/4_3_3_gcodeprintr_10.png" width="960"/><br>

2. Search and select your .gcode file.

  **This is different to Slic3r. You will need a .gcode file, not the .stl file.**

  ***If you only have the .stl go to "4.2.1 Use kiri:moto website to slice objects", and take it from there.***

  <img src="images/4_3_3_gcodeprintr_12.png" width="960"/><br>

  **As soon the objects gcode is imported, the software will start to simulate the printing.**

3. Press the printer icon in the right side panel.

  **This will open the printer control panel.**

  <img src="images/4_3_3_gcodeprintr_13.png" width="960"/><br>

4. Press **Connection Test** to see if your printer is connecting to your tablet correctly.

  **You sould get a message in your console on the right side, that the printer is now connected.**

  ***Also the "i" section indicates the connection with "Ready to print".***

  <img src="images/4_3_3_gcodeprintr_14.png" width="960"/><br>

5. Now move the temperature slide up, to start heating up the nozzle to 210 degrees Celsius.

  ***The nozzle temperature is indicated on the right side as second line beneath the console.***

6. When the temperature is reached, press the printing icon to start the printing process.

  <img src="images/4_3_3_gcodeprintr_15.png" width="960"/><br>

7. Press the print icon now on the left side of the screen to close the printer control panel.

  <img src="images/4_3_3_gcodeprintr_16.png" width="960"/><br>

  **The preview is now in "Print Mode", which shows you the printing process graphically.**

  <img src="images/4_3_3_gcodeprintr_17.png" width="960"/><br>

  **After the print is finished a Pop-up window will appear.**

  <img src="images/4_3_3_gcodeprintr_18.png" width="360"/><br>

#### *That is it! I hope you will have some great fun in printing your designs!*

---

#### After the print process

* After the print is finished, take of the magnetic print bed surface with the blue tape on it, and bend it. It makes it easier to remove it from it.
* If you used support material, you will get it off your object by using a miniture pocket pliers.

  <img src="images/ChainNosePocketPliers.jpg" width="256"/><br>


---

#### Printed Examples

| <img src="images/Necklace2.jpg" width="400"/><br> | <img src="images/ShoeTag2.jpg" width="400"/><br> |
|----------------------|-----------------------|
| <img src="images/Robot2.jpg" width="400"/><br>  |  <img src="images/CuteRobot2.jpg" width="400"/><br> |

| <img src="images/Fingerring2.jpg" width="840"/><br>   |
|-------------------------------------------------------|
| <img src="images/Fingerpuppet2.jpg" width="840"/><br> |


---
## THE END

### I hope you enjoyed this course. Have fun with being a Maker :-)

***Many thanks for taking the time, and if you got feedback and/or suggestions, it is much appreciated!***

#### contact@jensmeisner.net
