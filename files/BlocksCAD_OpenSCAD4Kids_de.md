# BlocksCAD >> OpenSCAD für Kids

<img src="../images/BlocksCAD_OpenSCAD4Kids_FeatureImg.png" width="960"/>

## Beschreibung

BlocksCAD4Kids ist ein Kurs, der Kindern beibringt, 3D-Modelle mit BlocksCAD zu erstellen. BlocksCAD verwendet visuelle Bausteine oder Puzzleteile, die modifiziert werden können, um aus verschiedenen Elementen ein Objekt zusammenzubauen. Diese Blöcke generieren im Hintergrund den Code, der die 3D-Modelle erstellt.

Wir werden gemeinsam kleine Übungen machen, um die verschiedenen Blöcke des bunten Modellierungsprogramms zu erlernen. Jeder Tag endet mit einer Übung, in der die Kinder ihr neues Wissen festigen.

Der erste Teil hilft, durch einfache Modellierungsübungen Vertrauen in die Arbeit mit BlockSCAD aufzubauen. Der zweite Teil geht dann auf etwas fortgeschrittenere Beispiele ein, um zu zeigen, wie man 3D-druckbare Modelle erstellt. Der dritte Teil zeigt, wie man mit BlockSCAD erstellte Designs in reale Dinge verwandelt, die mit einem kleinen 3D-Drucker für Kinder gedruckt werden.

---

## Index <a name="index"></a>

[Vorbereitung](#preparation)

1. [Introduction](#introduction)

   1.1. [Was ist OpenSCAD?](#whatisopenscad)

   1.2. [Was ist BlocksCAD?](#whatisblockscad)

   1.3. [Warum Code benutzen?](#whycodeto3d)

   1.4. [Wie funktioniert BlocksCAD?](#howblockscadwork)

2. [Erste Schritte mit einfachen Beispielen](#firststeps)

   2.1. [Lutscher](#lollipop)

   2.2. [Regenschirm](#umbrella)

   2.3. [Haus](#house)

   2.4. [Tasse](#cup)

   2.5. [Herz](#heart)

   2.6. [Schlüsselanhänger](#keychain)

   2.7. [Rechner](#calculator)

   2.8. [Pavillon](#pavilion)

   2.9. [Nadelbaum](#pinetree)

   2.10. [Wald](#forest)

3. [Wie erzeuge ich 3D druckbare Modelle?](#howfor3d)

   3.1. [Fingerring](#fingerring)

   3.2. [Halskette](#necklace)

   3.3. [Kopf für Fingerpuppe](#fingerpuppet)

   3.4. [Schuhbrosche](#shoetag)

   3.5. [Roboter 1 & 2](#robots)

4. [Drucke es aus!](#printing)

   4.1. [Mein 3D Drucker](#3dprinter)

   4.2. [Druckvorbereitung](#prepareprint)

   4.3. [Wie drucke ich?](#howtoprint)


---

## Vorbereitung <a name="preparation"></a>

### BlocksCAD

There is not much preparation necessary to prepare for this course. The simplest way is using the online version of BlocksCAD. The browser you are going to use needs WebGL support. Simply test the online version by dragging a sphere onto the workspace and press "RENDER". The sphere then should appear in the 3D preview.

[BlocksCAD Online](https://www.blockscad3d.com/editor/)

Eine andere Möglichkeit besteht darin, BlocksCAD von der Github-Projektseite herunterzuladen.

[BlocksCAD Offline](https://github.com/einsteinsworkshop/blockscad)

Damit der Text mit der Offline-Version funktioniert, musst du Folgendes tun:

BlocksCAD muss auf einem Webserver ausgeführt werden, damit der Textblock funktioniert, anstatt auf dem lokalen Dateisystem ausgeführt zu werden. Wenn Du Python oder Python3 installiert hast, kannst du dessen simpleHTTPserver verwenden. Öffne einen Terminal und führe die folgenden Befehle aus:

    cd /path/to/blockscad-dir-with-index.html-in-it


    python -m SimpleHTTPServer 9000

oder im Falle von Python3:

    python3 -m SimpleHTTPServer 9000


Dann gebe folgendes in deinem Webbrowser als Adresse ein:

    http://localhost:9000/


### 3D Drucker

Falls du bereits einen 3D-Drucker besitzt und/oder lernen möchtest, wie du deine 3D-Modelle für den Druck vorbereitest.

Du musst deine 3D-Modelle auf jeden Fall entsprechend deiner Druckereinstellungen in Scheiben schneiden.

Eine Steuerungssoftware ist nicht erforderlich, wenn du eine SD-Karte verwendest, um die geschnittene Modelldatei .gcode direkt auf den Drucker zu übertragen. Die meisten Drucker können dies über ihre integrierte Software verarbeiten.

Mehr zu diesem Thema findest du im Abschnitt 4.

**Alle Plattformen** ====================================================

Die einfachste Art, deine Modelle zu schneiden, erfordert keine Installation. Gehe zur Webseite, lade die aus BlocksCAD exportierte .stl-Datei hoch, gebe die Details deines Druckers ein und schneide das Objekt. Die resultierende .gcode Datei kannst du dann direkt oder mit einer Druckersteuerungssoftware auf deinen Drucker hochladen.

Online-GCODE-Slicer **kiri:moto** mit einem Webbrowser:

[kiri:moto - Kostenlos](https://grid.space/kiri/)

<img src="images/kirimoto.png" width="800"/><br>

**PC - Linux, Windows, Mac** ==========================================

Andernfalls kannst du eine erweiterte Software herunterladen und installieren, um Modelle zu schneiden und / oder deinen Drucker von einem PC/Laptop aus (über USB oder über das Netzwerk verbunden) steuern:

[Slic3r - Freie open-source software](https://slic3r.org/download/)

<img src="images/slic3r.png" width="800"/><br>

Es gibt ein [Druckerprofildatei](files/slic3r/files/Slic3r_config_EasyThreedX2.ini), die ich auf gitlab gespeichert habe, die Slic3r für einen EasyThreedX2-Drucker einrichtet. Dies ist der Drucker, den ich dir als Einstiegsdrucker für kleine Maker vorstellen werde.

Eine Alternative zu Slic3r ist [CUDA](https://ultimaker.com/software/ultimaker-cura). Für Anfänger ist das meiner Meinung nach jedoch zu viel.


**Android** ===========================================================

Derzeit gibt es keine geeignete Slicing-Software für Android, daher ist [kiri:moto - Kostenlos](https://grid.space/kiri/) immer noch die beste Herangehensweise, um deine Modelle für deinen Drucker zu schneiden.

Es gibt jedoch eine günstige und gut gemachte 3D-Drucker-Steuerungssoftware für Android. Wenn du einen USB-Adapter verwendest, kannst du deinen 3D-Drucker direkt an dein Android-Tablet anschließen und den 3D-Drucker steuern.

Adaptor:

<img src="images/OTG_USB_Adapter.jpg" width="256"/><br>


Software:

[GCodePrintr](https://play.google.com/store/apps/details?id=de.dietzm.gcodesimulatorprinter&hl=en_US&gl=US)

<img src="images/gcodeprintr.png" width="800"/><br>



---

#### [Zurück zum Index](#index)

## 1. Einführung <a name="introduction"></a>

Bevor wir beginnen, werfen wir einen Blick in die Hintergründe von BlocksCAD und OpenSCAD.

---

#### [Zurück zum Index](#index)

## 1.1. Was ist OpenSCAD? <a name="whatisopenscad"></a>

>*OpenSCAD ist eine kostenlose Softwareanwendung zum Erstellen von 3D-CAD-Objekten (Computer-Aided Design). Es ist ein nur auf Skripten basierender Modellierer, der seine eigene Beschreibungssprache verwendet. Teile können in der Vorschau angezeigt, aber nicht interaktiv in der 3D-Ansicht ausgewählt oder mit der Maus geändert werden. Ein OpenSCAD-Skript spezifiziert geometrische Primitive (wie Kugeln, Boxen, Zylinder usw.) und definiert, wie sie modifiziert und kombiniert werden (zum Beispiel durch Schnittpunkt, Differenz, Hüllkurvenkombination und Minkowski-Summen), um ein 3D-Modell zu rendern. Als solches führt das Programm konstruktive Volumengeometrie (CSG) aus. OpenSCAD ist für Windows, Linux und macOS verfügbar..*

[Wiki Referenz](https://en.wikipedia.org/wiki/OpenSCAD)

---

#### [Zurück zum Index](#index)

## 1.2. Was ist BlocksCAD? <a name="index"></a>

>*BlocksCAD ist ein Unternehmen für Bildungstechnologie, das sich der Entwicklung von Tools verschrieben hat, die Schülern jeden Alters dabei helfen, ihrer Liebe zu Wissenschaft, Technologie, Ingenieurwesen, Kunst und Mathematik (STEAM) nachzugehen. Unser Signaturprodukt, BlocksCAD, ist ein cloudbasiertes 3D-Modellierungstool, das Benutzer dazu ermutigt, Mathematik, computergestütztes Denken und Codierungskonzepte durch Visualisierung und Design von Modellen für den 3D-Druck zu erlernen.*

* BlocksCAD ist eine Scratch-ähnliche Programmierumgebung zum Erstellen von 3D-Objekten.
* Es wurde mit Unterstützung der Defense Advanced Research Projects Agency (DARPA) entwickelt und der Code ist unter GPLv3 oder höher lizenziert.
* BlocksCad-Code generiert OpenSCAD-Code, den Sie exportieren können (Code-Schaltfläche oben rechts oder im Designfenster). Andernfalls wird das STL-Modell auf dem Server generiert. Das Spielen mit BlocksCAD ist daher auch eine Möglichkeit, OpenSCAD zu erlernen.

[EduWiki Referenz](https://edutechwiki.unige.ch/en/BlocksCAD)

---

#### [Zurück zum Index](#index)

## 1.3. Warum Code zum 3D Modellieren nutzen? <a name="whycodeto3d"></a>

Es gibt grundlegende Vorteile beim Erlernen der Codierung:

1. Die Verwendung von Python oder anderen menschenähnlichen Sprachen ist leicht zu erlernen, besonders wenn sie jünger sind.
2. Verbessert die Fähigkeiten zur Problemlösung durch logisches Denken auf lineare und nichtlineare Weise.
3. Programmierkenntnisse sind auf viele verschiedene Lebensbereiche übertragbar.
4. Öffnet Möglichkeiten für Erfindungen und Innovationen, indem es ein Werkzeug zur Verfügung stellt, um Gedanken und Ideen auszudrücken.
5. Die Verwendung von Codierung mit 3D-Modellierung hilft Kindern, Spaß mit Mathematik zu haben.

Es gibt viele CAD-Software (Computer Aided Design). OpenSCAD ist mit seiner eigenen Skriptsprache einzigartig und ein brillanter Einstieg in die CAD-Modellierung und -Codierung. Der von dir verwendete Code ist so nah am menschlichen Verständnis, dass du theoretisch keinen Computer benötigen würdest, um daraus einen Blauplan / Skizzen zu erstellen. Einfach den Skriptcode ohne eine der Achsen (x, y, z) lesen und so verschiedene Ansichten, wie Frontansicht, Seitenansicht, Draufsicht s.o., erstellen.

Hier einige grundlegende Vor- und Nachteile der Verwendung von OpenSCAD als CAD-Modellierungswerkzeug:

| Vorteil                        | Nachteil                                 |
|--------------------------------|------------------------------------------|
| Leicht zu lernen               | Schwierig organische Formen zu erstellen |
| Einfache Präzisionsmodellierung| Nicht-traditionelle Benutzeroberfläche   |
| Kleine Dateigröße              | Verschieben von Objekten nur über Code   |       
| Lebendige helfende Gemeinschaft| Zuerst musst du Codierung lernen         |
| Komplexe Modellierung mit Modulen| Eingeschränkte Vorschaufunktion        |
| Kann in FreeCAD importiert werden| Animation sehr knifflig bis nicht möglich|

#### **Vor allem: Es macht Spaß, damit zu arbeiten, wenn man es einmal herausgefunden hat!**

---

#### [Zurück zum Index](#index)

## 1.4. Wie funktioniert BlocksCAD? <a name="howblockscadwork"></a>

BlocksCAD ist eine Webbrowser-Anwendung, die nicht auf Ihrem Computer installiert werden muss. Es ist eine Offline-Version verfügbar, die ein einfaches Webserver-Skript benötigt, um 3D-/2D-Text in BlocksCAD zu erstellen.

Die grafische Benutzeroberfläche von BlocksCAD ist ziemlich einfach, also lass uns das durchgehen.

<img src="images/1_Introduction_1.png" width="960"/><br>

#### Kopfteil

Der Kopfteil ist das Hauptmenü mit mehreren Unterabschnitten. Ich werde nur den Abschnitt erwähnen, den wir verwenden werden.

* Verdrahtete Kugel >> Sprache ändern
* Projekte >> Neues Projekt erstellen, Dateien mit verschiedenen Formaten speichern und öffnen
* Lernen >> Beispiele, Tutorials und Videos
* Projektname >> Gebe deinem aktuellen Projekt einen Namen (Standard: unbenannt)
* Pfeil nach links >> Rückgängig machen
* Pfeil nach rechts >> Wiederholen
* Mülleimer-Symbol >> Arbeitsbereich löschen
* *Blöcke | Code* >> Wechseln Sie zwischen Blockeditor und OpenSCAD-Codeansicht

#### Linkes Panel

Dieses Panel ist das Blockmenü, das mehrere Unterabschnitte hat. Ein Block enthält eine bestimmte Funktion, die den dargestellten OpenSCAD-Code generiert.

* 3D-Formen >> Grundlegende 3D-Primitive
* 2D-Formen >> Grundlegende 2D-Primitive
* Transformationen >> Transformiert eingebettete Formen
* Mengenoperationen >> Boolesche Operatoren
* Mathematik >> Mathematische Operatoren
* Logik >> Bedingte Operatoren
* Schleifen >> Schleifenblock
* Text >> 3D- und 2D-Textbezogene Blöcke
* Variablen >> Variablenblöcke für parametrische Designs
* Module >> Blöcke zum Erstellen von Modulen

#### Mittelteil

Das mittlere Panel ist im Grunde der Arbeitsbereich. Du ziehst alle Blöcke an dieser Stelle hinein und kombinierst sie. Wenn du Blöcke löschen möchtest, ziehe sie einfach ganz nach links aus dem Fenster oder drücke die Entf-Taste.

* Symbol Kreuzpunkt >> Zentriert die Ansicht des Arbeitsbereichs
* Symbol + >> Vergrößert
* Symbol - >> Verkleinert

***Du kannst die linke Maustaste zum Schwenken, das Scrollrad (oder die mittlere Maustaste) zum Zoomen und die rechte Maustaste zum Öffnen eines Untermenüs verwenden.***

***Verwende einen Finger zum Schwenken, zwei Finger zum Vergrößern und Verkleinern und halte den Finger 2-3 Sekunden lang auf dem Block gedrückt, um ein Untermenü zu öffnen.***

#### Rechter Teil

Dies ist der 3D-Ansichtsport zum Anzeigen der generierten 3D-Objekte. Die Änderungen erscheinen, nachdem die Schaltfläche *Render* gedrückt wurde.

***Du kannst die linke Maustaste zum Drehen, das Scrollrad (oder die mittlere Maustaste) zum Vergrößern und Verkleinern und die rechte Maustaste zum Schwenken verwenden.***

***Verwende den Finger zum Drehen, zwei Finger zum Vergrößern und Verkleinern.***

* Farbiges Quadrat >> Ändern Sie die Farbe, mit der Ihr Objekt in der Vorschau angezeigt wird
* 3 Zeilen Symbol >> Schaltet die Rasteransicht ein/aus
* Symbol + >> Vergrößern
* Symbol - >> Verkleinern
* Symbol Kreuzpunkt >> Zentriert die Ansicht der 3D Vorschau
* Rendern >> Erstellt eine Vorschauversion von allem auf der Arbeitsfläche
* STL generieren >> Exportiert die Vorschau als 3D-Mesh im .stl-Format

Lass uns die Dinge ausprobieren, damit du den grundlegenden Arbeitsablauf in BlocksCAD kennenlernst:

* Ändern die Sprache mit der Auswahl unter dem Symbol der verdrahteten Kugel (Standard: Englisch) zu ***Deutsch***.

  <img src="images/1_Introduction_2_de.png" width="960"/><br>

* Probiere das erste Beispiel unter **Lernen >> Beispiele >> Würfel mit Ausschnitten** aus.

  <img src="images/1_Introduction_3_de.png" width="960"/><br>

* Wenn BlocksCAD fragt, ob du den vorherigen Code speichern möchtest, drücke **Nicht speichern**.

  <img src="images/1_Introduction_4_de.png" width="960"/><br>

* Drücke im rechten Bereich auf **Rendern**, um zu sehen, was der Code macht.

  <img src="images/1_Introduction_5_de.png" width="960"/><br>

* Speichere die Blöcke auf deinem Arbeitsplatz **Projekt >> Lade Blöcke auf deinen Computer herunter**.

  <img src="images/1_Introduction_6_de.png" width="960"/><br>

* Öffne nun dein gespeichertes Projekt erneut **Projekte >> Lade Blöcke von deinem Computer hoch**

  <img src="images/1_Introduction_7_de.png" width="960"/><br>

* Wie du siehst, wurde es mit dem Projektnamen gespeichert. Wähle es aus und drücke **Öffnen**

  <img src="images/1_Introduction_8_de.png" width="960"/><br>

* Drücke nun ***Code*** auf der rechten Seite des Kopfbereichs **Blöcke | Code**. Du siehst den eigentlichen Code, der mit den Blöcken erstellt wird.

  <img src="images/1_Introduction_9_de.png" width="960"/><br>

* Drücke nun ***Blöcke*** und dann **Projekte >> Lade OpenSCAD code herunter**, um den angezeigten Code auf Ihren Computer zu exportieren.

  <img src="images/1_Introduction_10_de.png" width="960"/><br>

* Da du OpenSCAD wahrscheinlich noch nicht installiert hast, sehe dir das Bild an, um zu sehen, wie es in OpenSCAD aussehen wird.

  <img src="images/1_Introduction_11_de.png" width="960"/><br>


* Wenn du die gerenderte Vorschau als Objekt exportieren möchtest, drücke im rechten Bedienfeld auf **Generieren STL**. Vergiss nicht, dein Projekt zuerst zu benennen. Dies wird den Dateinamen bestimmen.

* Die exportierte STL-Datei ist relevant, wenn du das Objekt mit einem 3D-Drucker, CNC-Router oder Laserschneider drucken/schneiden möchtest.

---

#### [Zurück zum Index](#index)

## 2. Erste Schritte mit einfachen Beispielen <a name="firststeps"></a>

Bevor wir uns mit druckbaren Designs befassen, lass uns die verschiedenen Blöcke ansehen. Du lernst anhand der folgenden Beispiele, wie du verschiedene Blöcke zu einem Objekt kombinieren kannst.

**Neben der Verwendung von Zahlen zum Zählen oder wahr oder falsch (1 oder 0), verwendest du meistens Zahlen als Einheiten, wie Millimeter und Grad.**

Beginnen wir mit unserem ersten Modell...einem Lutscher.

## 2.1. Lutscher <a name="lollipop"></a>

<img src="images/Lollipop.jpg" width="512"/><br>

1. Starte mit einem neuen Projekt, indem du auf **Projekte >> Neu*** klickst.
2. Ziehe eine **kugel** aus **3D-Formen** per Drag & Drop in den mittleren Teil, dem Arbeitsbereich.
3. Drücke **Rendern** und ändere deine Ansicht, indem du auf die linke Maustaste klickst und die Maus bewegst.
4. Wenn dein Objekt außer Sicht ist, verwende das Symbol Kreuzpunkt, um es zurückzubekommen.

  **Die Kugel zeigt 10, was 10 mm oder 1 cm Radius bedeutet. Der Durchmesser der Kugel beträgt also 2 cm oder 20 mm.**

5. Ändere die Nummer der **kugel** auf ***30*** und rendere erneut.

  **Jetzt hat die Kugel einen Durchmesser von 6 cm.**

  **Aber warte, wie kann ich den Unterschied sehen?**

  **Um den Unterschied zu sehen, legen wir eine zweite Kugel beiseite.**

  **Aber zuerst, um die Kugel zur Seite zu bewegen, brauchen wir ein weiteren Block namens "verschieben".**

6. Ziehe einen Block **verschieben** in den Arbeitsbereich. Du findest ihn in **Transformationen**.
7. Gebe nun ***50*** in die erste Zelle mit dem Namen **X** ein.
8. Ziehe nun eine weitere **kugel** aus der **3D-Formen** Kategorie und stecke sie in den **verschieben** Block. Drücke erneut **Rendern**!

  **Siehst du!? Die neue Kugel wird jetzt 50 mm neben unserer ersten Kugel entlang der roten Linie erstellt.**

  **Die rote Linie ist die x-Achse, die grüne Linie ist die y-Achse und die blaue Linie ist die z-Achse.**

  ***Ändere die Zahlen in *verschieben* und sehe, wie sich die Kugel mit den von dir vorgenommenen Änderungen bewegt. Vergiss nicht jedesmal auf "Rendern" zu drücken.***

9. Lösche die erste Kugel, indem du darauf klicken und sie nach links aus der Szene ziehst, oder drücke die Entf-Taste. Wir behalten die Kugel innerhalb des **verschieben** Blocks.
10. Gebe nun neue Werte für **verschieben** wie folgt ein: ***X = 0, Y = 0, Z = 35***.
18. Ändere den Radius von **kugel** zu ***8*** Millimeter.
19. Ziehe nun einen weiteren **zylinder** Block aus **3D-Formen** auf deinen Arbeitsbereich.
20. Gebe die Zahl ***1*** für **radius1** und ***28*** für **höhe** ein.
21. Drücke erneut **Rendern**.

  #### A voila, ein Lutscher!

22. Benenne dein Projekt unter **Projektname:** als ***Lutscher***.
23. Speichere dein Projekt unter **Projekt >> Lade Blöcke auf deinen PC runter**.

<img src="images/2_1_FirstSteps_1_de.png" width="960"/><br>


---

#### [Zurück zum Index](#index)

## 2.2. Regenschirm <a name="umbrella"></a>

<img src="images/Umbrella.jpg" width="512"/><br>

1. Öffne dein Lutscher-Projekt unter **Projekt >> Lade Blöcke von deinem PC hoch**.
2. Ändere den Namen dieses Projekts in ***Regenschirm*** unter **Projektname:**.
3. Klicke nun auf die **kugel** und verschiebe sie aus der Arbeitsfläche.
4. Behalte den **verschieben** Block.
5. Ziehe nun einen weiteren **zylinder** aus **3D-Formen** in die Szene und stecke ihn in **verschieben**, und drücke dann **Rendern**.

  **Kannst du den Raum zwischen den 2 Zylindern sehen?**

6. Ändern wir es, indem wir die Höhe des ersten Zylinders im Block **verschieben** zu ***Z = 25*** ändern.
7. Press **Render**.

  **Ok, jetzt machen wir einen Regenschirm daraus!**

  **Der Zylinderblock hat radius1 und radius2 für den unteren Kreis und den oberen Kreis. Der Wert von radius2 ist standardmäßig an radius1 gebunden. Das Schlosssymbol zwischen den 2 Werten steht für diese Verbindung.**  

8. Drücke das kleine Schlosssymbol, um die Verbindung zwischen **radius1** und **radius2** des **zylinder** Block zu entriegeln, der im **verschieben** Block eingesteckt ist.
9. Ändere den Wert von **radius1** zu ***20*** und **radius2** zu ***1***.
10. Drücke erneut **Rendern**!

  **Jetzt haben wir es! Da ist unser Regenschirm.**

9. Speichere dein Projekt unter **Projekt >> Lade Blöcke auf deinen PC runter**.

<img src="images/2_2_FirstSteps_1_de.png" width="960"/><br>

---

#### Übung:

***Probiere alle verschiedenen 3D-Formen aus, ändere die Werte und verwende "verschieben", um sie im Raum zu bewegen! Mal sehen, was du daraus machen kannst!***

**Tipp: Du kannst "verschieben" Blöcke ineinander stecken. Drücke dafür *+* im Block, um weitere Slots für Objekte hinzuzufügen. Auf diese Weise kannst du  auch Objekte gruppieren und alle gleichzeitig bewegen!**

<img src="images/2_2_FirstSteps_2_de.png" width="512"/><br>

---

#### [Zurück zum Index](#index)

## 2.3. Haus <a name="house"></a>

<img src="images/House.jpg" width="512"/><br>

1. Beginnen wir wieder mit **Projekt >> Neu**.
2. Nenne dieses Projekt ***Haus***.
3. Ziehen Sie einen **würfel** aus **3D-Formen** auf den Arbeitsbereich und lege ihn dort ab.
4. Gib folgende Werte ein: ***X = 30, Y = 40, Z = 20***.
5. Drücke **Rendern**.

  **Nun wollen wir unser Haus überdacht bekommen.**

6. Ziehe einen **würfel** Block aus **3D-Formen**, einen **verschieben** und einen **rotieren** aus **Transformationen** per Drag & Drop auf deinen Arbeitsbereich.
7. Ändere die Werte des neuen Würfels zu ***X = 25, Y = 40, Z = 25***.
8. Stecke als nächstes den **würfel** Block, den du gerade geändert hast, in den **rotieren** Block.
9. Ändere die Rotation von **Y** auf ***45*** Grad.

  **Du siehst das Rad, das du um 45 Grad entlang der ausgewählten Achse drehen kannst.**

  **Du kannst die Nummer auch direkt eingeben.**

10. Stecke nun diesen **rotieren** Block in den **verschieben** Block.
11. Ändere die Werte von **verschieben** zu ***X = -3, Y = 0, Z = 20***.
12. Drücke **Rendern**.

  **Das Dach ist an der richtigen Stelle, aber da stimmt etwas nicht!**

  **Wir müssen den unteren Teil des Dachwürfels wegschneiden, damit er wirklich wie ein Dach aussieht.**

13. Ziehe einen **würfel** Block aus **3DShapes**, einen **verschieben** Block aus **Transformationen** und einen **differenz** Block aus **Mengenoperationen** auf deinen Arbeitsbereich.
14. Ändere zuerst die Werte des neuen **würfels** in ***X = 36, Y = 40 und Z = 20***.
15. Stecke es dann in den neuen **verschieben** Block und ändere seine Werte nur in **X** zu ***-3***.
16. Drücke **Rendern**.

  **Dieser neue Würfel stellt den Teil dar, den wir vom Dachwürfel wegschneiden möchten.**

  **Dafür haben wir den "differenz" Block. Es schneidet den 2. Teil namens "minus" vom 1. Teil ab.**

17. Stecke den **würfel** Block inklusive **verschieben** und **rotieren** in den ersten Abschnitt des **differenz** Blocks.
18. Stecke dann den **würfel** Block einschließlich seines **verschieben** Blocks in den **differenz** Abschnitt namens **minus**.
19. Drücke erneut **Rendern**.

  **Hat es den unteren Teil weggeschnitten? Sieht es jetzt aus wie ein Dach?**

  **Jetzt können wir Haus und Dach eine Farbe geben!**

20. Ziehen Sie zwei **farbe** Blöcke aus **Transformationen** per Drag & Drop in deinen Arbeitsbereich.
21. Stecke den Haus **würfel** und den Dach **differenz** Block jeweils in einen **farbe** Block.
22. Klicke auf die kleinen Farbquadrate und wähle deine eigene Farbe aus.
24. Drücke **Rendern**.

  **Um die Übersicht zu behalten, kannst du jeden der Blöcke benennen.**

25. Wähle jeden **farbe** Block aus, klicke mit der rechten Maustaste oder verwende die Fingerberührung, um ein kleines Popup-Menü zu öffnen.
26. Wähle **Kommentar hinzufügen** aus. Danach siehst du ein kleines Fragezeichen im Block.
27. Klicke darauf und gebe jeweils ***Haus*** und ***Dach*** als Kommentar ein.

  **Kommentiere weiterhin die von dir erstellten Teile. Es ist hilfreich für dich und andere, deine Designs besser zu verstehen, insbesondere wenn es in der Zukunft komplizierter wird.**

  **Das Kommentieren ist ein sehr wichtiger Teil bei der Programmierung. Es ist wie ein Freund, der dir die richtige Nachricht zuflüstert, wenn du sie brauchst.**

28. Speichere dein Projekt unter **Projekt >> Lade Blöcke auf deinen PC runter**.

<img src="images/2_3_FirstSteps_1_de.png" width="960"/><br>

---

#### Übung:

***Verwende würfel, verschieben, rotieren und differenz, um dem Haus Türen, Fenster oder einen Schornstein hinzuzufügen.***

**Tips:**

**1. Du kannst das „+“-Symbol verwenden, um der Differenz weitere Slots hinzuzufügen, sodass mehr Boxen auf einmal ausgeschnitten werden.**

<img src="images/2_3_FirstSteps_2_de.png" width="960"/><br>

**2. Du kannst RMB (rechte Maustaste) oder Fingerberührung verwenden, um das Popup-Menü zu öffnen und „Kopieren“ wählen, um Kopien deiner Blöcke zu erhalten.**

<img src="images/2_3_FirstSteps_3_de.png" width="960"/><br>

#### Hier ein Beispiel für den Einstieg:

<img src="images/2_3_FirstSteps_4_de.png" width="960"/><br>

---

#### [Zurück zum Index](#index)

## 2.4. Teetasse <a name="cup"></a>

<img src="images/Cup.jpg" width="512"/><br>

**Mit diesem Design stelle ich dir eine neue Methode namens Schnittmenge vor, die du verwenden kannst.**

1. Ziehe einen **schnittmenge** Block aus **Mengenoperationen**, einen **verschieben**- und einen **skalieren** Block aus **Transformationen**, einen **kugel** Block und einen **würfel** aus **3D-Formen**.
2. Beginnen wir mit der **Kugel**, indem wir den **radius** auf ***40*** ändern.
3. Stecke die **kugel** in den **skalieren** Block und ändere die Werte dort auf ***X = 0.6, Y = 0.6, Z = 1***.
4. Drücke **Rendern**.

  **Um eine Tasse zu erstellen, möchten wir die Kugel oben und unten abschneiden.**

  **Während "differenz" die repräsentierten Blöcke wegschneidet, schneidet "schnittmenge" alles andere als den repräsentierten Teil weg.**

  **Lass uns einen Würfel der Fläche bauen, der nach dem Schnitt übrig bleiben soll.**

5. Ändere die Werte des **würfels** zu ***X = 80, Y = 80, Z = 50***.
6. Ändere die **würfel** Option ***nicht zentriert*** zu ***zentriert***.

  **Der Blockschnittpunkt behält alles, was vom zweiten Objekt unter "mit" abgedeckt wird.**

7. Stecke den **skalieren** Block mit der **kugel** in die erste Position von **schnittmenge**.
8. Verschiebe dann den **würfel** an die zweite Position namens **mit**.
9. Drücke **Rendern**.

  **Sieh!? Es sollte jetzt wie ein Fass aussehen.**

  **Ok, lass uns das Fass mit einem "verschieben" Block nach oben bewegen.**

10. Verbinde den **schnittmenge**-Block mit **verschieben**.
11. Ändere den Wert **Z** des **verschieben** Blocks zu ***25***.
12. Drücke **Rendern**.

  **Als nächstes verwenden wir einen Zylinder, um das Innere der Tasse auszuschneiden. Dies ist ein Block, den Sie bereits verwendet haben. Was ist es? … Differenz!**

13. Ziehe einen **zylinder** Block aus **3D-Formen**, einen **verschieben** aus **Transformationen** und einen **differenz** Block aus **Mengenoperationen** auf deinen Arbeitsbereich.
14. Ändere die Werte des **zylinders** in ***radius1 = 18*** und ***höhe = 50***.
15. Stecke den **zylinder** in den neuen **verschieben** Block.
16. Ändere den **Z** Wert des **verschieben** Blocks zu ***5***.
17. Nehme nun den **verschieben** Block mit dem **schnittmenge** und stecke ihn in die erste Position des **differenz** Blocks.
18. Stecke den **verschieben** Block mit dem **zylinder** in den zweiten Teil mit dem Namen **minus**.
19. Drücke **Rendern**.

  **Zu guter Letzt brauchen wir noch einen Henkel für unsere Tasse.**

20. Ziehe einen **ring** Block aus **3D-Formen**, einen **rotieren** und einen **verschieben** Block aus **Transformationen** per Drag & Drop auf deinen Arbeitsbereich.
21. Ändere die Werte des **ring** Blocks in ***radius1 = 12, radius2 = 3, seiten = 40 und flächen = 20***.
22. Stecke den **ring** in den **rotieren** Block und ändere seinen **X** Wert zu ***90*** Grad.
23. Verbinde dies mit **verschieben** und ändere die Werte in ***X = 25, Y = 0, Z = 2***.
24. Drücke **Rendern**.

  **Ok, du siehst den Griff auf der Unterseite. Der Ring geht durch die Tasse, aber auch diesen möchten wir im Innern wegschneiden. Wir müssen den Ring in den Differenzblock implementieren.**

  **Du kannst dies tun, indem du einen Slot im "verschieben" block der "differenz" hinzufügst.**

25. Drücke das **+**-Zeichen des **verschieben** Blocks an der ersten Position vom **differenz** Block und stecke den gesamten **ring** Block inklusive **rotieren** und **verschieben** ein.
26. Füge einen **farbe** Block aus **Transformationen** hinzu und füge den gesamten Blockabschnitt darin hinein.
27. Drücke **Rendern**.
28. Speichere dein Projekt unter **Projekt >> Lade Blöcke auf deinen PC runter**, nachdem du es benannt hast.

<img src="images/2_4_FirstSteps_1_de.png" width="960"/><br>

---

**Bevor du in der nächsten Übung eigene Objekte erstellst, möchte ich dir etwas zeigen, das sehr nützlich sein kann.**

**Lass uns ein neues Projekt erstellen, vergiss nicht, dein Tassen-Projekt zu speichern.**

1. Starte mit einem neuen Projekt, indem du **Projekte >> Neu*** drückst.
2. Ziehe eine **kugel** und einen **zylinder** aus **3D-Formen**, einen **verschieben** Block und zwei **seiten** Blöcke aus **Transformationen** auf deinen Arbeitsbereich.
3. Stecke die **kugel** in **verschieben** und ändere den **X** Wert zu ***40***.
4. Dann stecke **verschieben** mit der **kugel** in einen der **seiten** Blocks und ändere dessen Wert zu ***4***.
5. Drücke **Rendern**.

  **In der Geometrie nennt man diese Form Oktaeder.**

  <img src="images/2_4_FirstSteps_6_de.png" width="960"/><br>

6. Stecke nun den **zylinder** in die anderen **seiten** und ändere den Wert auf ***3***.
7. Drücke **Rendern**.

  **In der Geometrie nennt man diese Form ein Dreiecksprisma.**

  <img src="images/2_4_FirstSteps_7_de.png" width="960"/><br>

8. Ändere den Wert von **Seiten** zu ***5***.
9. Drücke **Rendern**.

  **In der Geometrie nennt man diese Form ein fünfeckiges Prisma.**

  <img src="images/2_4_FirstSteps_8_de.png" width="960"/><br>

10. Ändere den Wert von **Seiten** zu ***7***.
11. Drücke **Rendern**.

  **In der Geometrie nennt man diese Form ein sechseckiges Prisma.**

  <img src="images/2_4_FirstSteps_9_de.png" width="960"/><br>

12. Klicke nun auf das Schlosssymbol des **zylinder** Blocks und ändere **radius2** zu ***0***.
13. Ändere den Wert von **Seiten** zu ***4***.
14. Drücke **Rendern**.

  **In der Geometrie nennt man diese Form eine Pyramide.**

  <img src="images/2_4_FirstSteps_10_de.png" width="960"/><br>

15. Benenne dieses Project und speichere dein Projekt unter **Projekt >> Lade Blöcke auf deinen PC runter**.
16. Öffne dein Tassen-Projekt erneut unter **Projekt >> Lade Blöcke von deinem Computer hoch**.

---

#### Übung:

***Erstelle ein ähnliches Objekt, z.B. ein Teller, eine andere Tasse, eine Schüssel oder andere Gegenstände. Du kannst den "seiten" Block verwenden, um verschiedene Formen zu erhalten.***

**Tips:**

**1. Verwende einen "verschieben" Block, um deine Tasse zur Seite zu bewegen.**

<img src="images/2_4_FirstSteps_2_de.png" width="960"/><br>

**2. Die Renderzeit erhöht sich, je mehr Seiten oder Polygone du in der Szene hast. Um das Rendern zu beschleunigen, reduziere in diesem Tutorial "seiten" vorübergehend.**

<img src="images/2_4_FirstSteps_3_de.png" width="960"/><br>

**3. Was ist, wenn du die Tasse nicht benötigst, sie aber als Referenz auf deinem Arbeitsplatz behalten möchtest? Klicke einfach mit der rechten Maustaste darauf oder verwende eine Fingerberührung 2 - 3 Sekunden, um das Popup-Menü zu öffnen. Wähle dann "Blaustein deaktivieren".**

<img src="images/2_4_FirstSteps_4_de.png" width="960"/><br>

**4. Du kannst es genauso umkehren. Es wird "Baustein aktivieren" angezeigt.**

<img src="images/2_4_FirstSteps_5_de.png" width="960"/><br>


---

#### [Zurück zum Index](#index)

## 2.5. Herz <a name="heart"></a>

<img src="images/Heart.jpg" width="512"/><br>

In diesem Tutorial lernen Sie 3 neue Blöcke kennen: **vereinigung**, **hülle** und **spiegeln**. Lass uns anfangen!

1. Starte mit einem neuen Projekt, indem du **Projekte >> Neu*** drückst.
2. Ziehe per Drag & Drop einen **würfel** Block und einen **zylinder** Block aus **3D-Formen**, einen **verschieben** und einen **rotieren** Block aus **Transformationen** und einen **hülle** Block von **Mengenoperationen** auf deinen Workspace.
3. Ändere die Werte des **würfels** zu ***X = 10, y = 10, z = 10, zentriert*** und stecke ihn in den **rotieren** Block.
4. Ändere den **Z** Wert von **rotatieren** zu ***45*** Grad, damit aus dem Würfel die untere Kante des Herzens wird.
5. Ändere nun die Werte des **zylinders** zu ***radius1 = 10, höhe = 10, zentriert*** und stecke ihn in den **verschieben** Block.
6. Ändere nun die Werte des **translate** Block zu ***X = 7, Y = 12, Z = 0***.
7. Stecke beide Blöcke **würfel** mit **rotieren** und **zylinder** mit **verschieben** in den **hülle** Block.
8. Drücke **Rendern**.

  **Aber das ist nur eine Hälfte des Herzens. Wie werden wir den zweiten Teil hinzufügen?**

9. Ziehe einen **spiegeln quer** aus **Transformationen** und einen **vereinigung** Block aus **Mengenoperationen** auf deinen Arbeitsbereich.
10. Ändere im **spiegel quer** Block den Wert von ***XY*** zu ***YZ***.
11. Klicke mit der rechten Maustaste (Finger 2-3 Sekunden) auf den **hülle** Block, um das Popup-Menü zu öffnen und drücke ***Kopieren***.
12. Stecke dann das Duplikat in den **spiegeln quer** Block.
13. Stecke nun beide Hauptblöcke in den **vereinigung** Block.
14. Drücke **Rendern**.

  **Da hast du ein volles Herz!**

15. Benenne dein Projekt und speichere es.

<img src="images/2_5_FirstSteps_1_de.png" width="960"/><br>

---

#### [Zurück zum Index](#index)

## 2.6. Schlüsselanhänger <a name="keychain"></a>

<img src="images/KeyChain.jpg" width="512"/><br>

**Zuerst erstellen wir die Grundplatte für unseren Schlüsselanhänger.**

1. Ziehe drei **zylinder** Blöcke aus **3D-Formen**, drei **verschieben** Blöcke aus **Transformationen** und einen **hülle** aus **Mengenoperationen** per Drag & Drop.
2. Ändere die Werte des ersten **zylinder** zu ***radius = 10 und höhe = 2, zentriert***.
3. Stecke es in ein **verschieben** Block und ändere den **X** Wert zu ***-30***.
4. Ändere die Werte des zweiten und dritten **zylinder** zu ***radius1 = 2, höhe = 2, zentriert***.
5. Stecke den zweiten **zylinder** in **verschieben** und ändere den **X** Wert zu ***30*** und den **Y** Wert zu ***8***.
6. Stecke den dritten **zylinder** in den letzten **verschieben** Block und ändere den **X** Wert zu ***30*** und den **Y** Wert zu ***-8***.
7. Drücke **Rendern**.

  **Du solltest auf einer Seite eine große Scheibe und auf der anderen Seite 2 kleine Knöpfe sehen.**

  **Wir möchten aus diesen drei Objekten eine Form erstellen.**

8. Klicke auf das **+**-Symbol im **hülle** Block und stecke alle drei **translate** Blöcke mit jeweiligen **zylinder** in **hülle**.
9. Drücke **Rendern**.

  **Sieh, das ist die Grundplatte!**

  **Jetzt fügen wir ein Loch hinzu, damit wir es an unser Schlüsselbund einhaken können.**

10. Ziehe einen **zylinder** aus **3D-Formen**, einen **verschieben** aus **Transformationen** und einen **differenz** aus **Mengenoperationen** auf deinen Arbeitsbereich.
11. Die Werte für den **zylinder** sind ***radius1 = 3, höhe = 3, zentriert***.
12. Stecke den **zylinder** in den **verschieben** Block und ändere die Werte dort auf ***X = -33, Y = 0, Z = 3***.
13. Stecke nun den **verschieben** Block mit dem **hülle** Block, der die anderen drei **zylinder** enthält, in die erste Position des **differenz** Blocks.
14. Stecke dann den **verschieben** Block mit dem Zylinder in den zweiten Abschnitt der **differenz** namens **minus** hinein.
15. Drücke **Rendern**.

  **Jetzt ist die Schlüsselanhängerplatte fertig!**

  **Jetzt kannst du eigene Grafiken oder Namen hinzufügen. Fahren wir mit einem Namen fort.**

16. Ziehe einen **verschieben** Block aus **Transformationen** und einen **3D-text** Block aus **Text** auf deinen Arbeitsbereich.

  **Um den Namen in die Platte zu schneiden, müssen wir ihn zu der "differenz" hinzufügen.**

17. Drücke das **+** Symbol im **differenz** Block.
18. Ändere die Werte vom **3D-text** Block wie folgt ***3D-text: BlocksCAD, größe: 11, dicke: 3, schriftart: Nimbus Sans***.
19. Setze den **3D-text** Block in **verschieben** ein und ändere die Werte zu ***X = -30, Y = -4, Z = 3***.
20. Füge dann **verschieben** mit dem **3D-text** Block in den dritten Abschnitt von **differenz** namens **minus** ein.
21. Drücke **Rendern**.
22. Benenne dein Projekt und speichere es.

<img src="images/2_6_FirstSteps_1_de.png" width="960"/><br>

---

<img src="images/IHeartU.jpg" width="512"/><br>

**Verwenden wir das Herz, das wir zuvor modelliert haben, um einen zweiten Schlüsselanhänger zu erstellen.**

1. Öffne das Schlüsselanhängerprojekt erneut (falls du es geschlossen hattest).
2. Benenne es als ***IHeartU*** um.
3. Trennen Sie den letzten **verschieben** Block mit dem **3D-text** Block, indem du ihn beiseite auf Ihren Arbeitsbereich ziehst.
4. Ziehe einen **vereinigung** Block aus **Mengenoperationen**, einen **verschieben** Block und einen **skalieren** aus **Transformationen** auf deinen Arbeitsplatz.
5. Stecke den **vereinigung** Block in den dritten Abschnitt mit dem Namen **minus** des **differenz** Blocks.
6. Ziehe nun den **verschieben** Block mit dem **3D-text** Block in den ersten Abschnitt von **vereinigung**.
7. Ändere die Werte dieses **verschieben** Blocks zu ***X = -15, Y = -5, Z = 0***.
8. Ändere die Werte des **3D-text** Blocks zu ***3D-text: I      U, größe: 14, dicke: 10, schriftart: Roboto***.
9. Drücke **Rendern**.

  **Im Text gibt es 6 Leerzeichen zwischen I und U. Wir wollen das Herz zwischen I und U einfügen.**

10. Stecke **skalieren** in den verbleibenden **verschieben** Block.
11. Ändere die Werte von **skalieren** zu ***X = 0,4, Y = 0,4, Z = 0,3***.
12. Ändere nun die Werte von **verschieben** zu ***X = 0, Y = -3, Z = 3***.
13. Ziehe nun den **verschieben** Block mit dem **skalieren** Block in den zweiten Abschnitt von **vereinigung** namens **plus**.

  **Jetzt können wir das Herzprojekt importieren!**

14. Gehen Sie zu **Projekte >> Importiert Blöcke in dieses Projekt**, wähle deine Herzprojektdatei aus und drücke **Öffnen**.

  **Möglicherweise musst du herauszoomen, um den importierten Block zu sehen!**

15. Ziehe den gesamten Block in den **skalieren** Block hinein.
16. Drücke **Rendern**.

  **Ihr Schlüsselanhänger sollte wie im Bild unten aussehen.**

17. Speichere dein Projekt.


<img src="images/2_6_FirstSteps_2_de.png" width="960"/><br>

---

#### [Zurück zum Index](#index)

## 2.7. Rechner <a name="calculator"></a>

<img src="images/Calculator.jpg" width="512"/><br>

Lassen Sie uns einen kleinen Taschenrechner in BlocksCAD erstellen. Sie werden in der Lage sein, alle Blöcke im Mathematik-Bereich zu verwenden, um Hausaufgaben in Mathe zusammenzustellen. Ein echter Taschenrechner ist vielleicht einfacher, aber aus deinem Ergebnis kannst du kein Modell drucken :-D.

  **Zuerst bauen wir einen Anzeige**.

1. Ziehe per Drag & Drop einen **würfel** aus **3D-Formen**, einen **verschieben** und einen **farbe** Block aus **Transformationen** und einen **vereinigung** Block aus **Mengenoperationen** auf deinen Arbeitsbereich.
2. Ändere den Wert vom **würfel** zu ***X = 80, Y = 16, Z = 1***.
3. Dann stecke **würfel** in den **verschieben** Block.
4. Ändere die Werte vom **verschieben** Block zu ***X = -45, Y = -4, Z = 0***.
5. Stecke jetzt **verschieben** mit **würfel** in den **farbe** Block und ändere die Farbe.
6. Drücke **Rendern**.

  **Du solltest ein kleines Kästchen sehen, das wie eine Anzeige aussieht.**

  **Jetzt legen wir den Text "Ergebnis: " oben drauf**.

7. Ziehe einen **3D-text** Block aus **Text**, einen **verschieben** Block und einen Block **farbe** aus **Transformationen** und einen **vereinigung** aus **Mengenoperationen** auf deinen Arbeitsbereich.
8. Ändere die Werte von **3D-text** wie folgt: ***3D-text: Ergebnis: , größe: 10, dicke: 2, schriftart = Open Sans***, und stecke es in den **verschieben** Block.
9. Ändere die Werte vom **verschieben** Block zu ***X = -40, Y = 0, Z = 0***.
10. Ziehe den **translate** Block mit **3D-text** in den ersten Abschnitt von **vereinigung**.
11. Ziehe nun diesen Block **vereinigung** in **farbe** hinein. Ändere die Farbe.
12. Drücke **Rendern**.

  **Fertig ist der leere Rechner! Jetzt brauchen wir den eigentlichen mathematischen Teil!**

13. Ziehe einen **3D-Text** Block aus **Text**, zwei **1 + 1** Blöcke aus **Mathematik** und einen **setze etwas auf** Block aus **Variablen** auf deinen Arbeitsplatz.
14. Stecke einen **1 + 1** Block in die erste Ziffernstelle des zweiten **1 + 1** Blocks, was dadurch ***1 + 1 + 1*** ergibt.

  **Du kannst Zahlen, Operatoren ändern oder Blöcke durch andere Blöcke aus Mathematik ersetzen. Dies hängt davon ab, was du berechnen möchtest!**

15. Ziehe nun diesen mathematischen Blockteil in den **setze etwas auf** Block.
16. Klicke auf **etwas** und wähle aus dem Dropdown-Menü **Variable umbenennen**.
17. Benenne es in ***Mathe*** um.
18. Nehme nun den **3D-text** Block und stecke ihn in den zweiten Teil von **vereinigung** namens **plus**.
19. Ziehe den neuen **mathe** Block unter **Variablen** auf den ersten Wert vom **3D-text**, wo der sonst der Text stehen soll.

  **Jedes Mal, wenn du eine neue Variable erstellen, generiert BlocksCAD seine eigenen Blöcke "setze DEIN_NAME auf" und "DEIN_NAME". Dies ist sehr wichtig für parametrisches Design. Wir werden jetzt tatsächlich unser erstes parametrisches Design erschaffen.**

20. Ändere die restlichen Werte von **3D-text** wie folgt ***größe: 10, dicke: 2, schriftart: Open Sans***.
21. Drücke **Rendern**.

  **Du solltest jetzt "3" als Ergebnis von 1 + 1 + 1 sehen. Spiele ein wenig mit den Zahlen und Operatoren, um die Ergebnisse zu sehen, nachdem du die Vorschau gerendert hast.**

<img src="images/2_7_FirstSteps_1_de.png" width="960"/><br>


  **Und genau darum geht es beim parametrischen Design. Du kannst die Zahlen in der Variablen ändern, und das Programm passt die neuen Zahlen im Design an.**

  **Lass uns noch ein kleines Beispiel bauen, damit du die parametrische Modellierung etwas besser verstehst.**

#### Lassen Sie uns ein Rohr erstellen, das wir einfach ändern können.

1. Starten Sie ein neues Projekt.
2. Nenne es ***Parametrisches Rohr***.
3. Ziehe zwei **zylinder** aus **3D-Formen** und einen **differenz** Block aus **Mengenoperationen** in deinen Arbeitsbereich.
4. Stecken Sie beide **zylinder** in den **differenz** Block.
5. Öffne mit der rechten Maustaste oder mit dem Finger das Pop-up-Fenster und wähle ***externe Eingänge***.

  **Dadurch wird die Anzeige des Blocks geändert. Da wir bald Variablen verwenden, würde es ein sehr langer Block werden. Auf diese Weise wird es einfacher, damit zu arbeiten.**

  <img src="images/2_7_FirstSteps_3.png" width="960"/><br>

6. Ziehe drei **setze etwas auf** Blöcke aus **Variablen**, zwei **1 + 1** Blöcke und drei **0** Blöcke aus **Mathematik**.
7. Stecke jeweils einen **0** Block in jeden **setze etwas auf** Block.
8. Verwende das Dropdown-Menü von **etwas**, und wähle **Neue Variable ...**. Verwende jeweils folgenden Namen ***Länge, Innenradius, Dicke***.
9. Ändere den Wert in **0** von **setze Länge auf** zu ***20***.
10. Ändere den Wert in **0** von **setze Innenradius auf** zu ***30***.
11. Ändere dann den Wert in **0** von **setze Dicke auf** zu ***2***.
12. Stecke nun einen **1 + 1** Block in den **zylinder** für **radius1** des **zylinders** im ersten Abschnitt von **differenz**.
13. Stecke im selben **Zylinder** den zweiten **1 + 1** Block für **radius2** hinein.
14. Ziehe vier **Innenradius** Blöcke, zwei **Dicke** Blöcke und zwei **Länge** Blöcke aus **Variablen** auf Ihren Arbeitsbereich.
15. Stecke einen **Innenradius** Block in jede erste Position von **1 + 1** vom **radius1** und **radius2** Wert des **zylinders** im ersten Abschnitt von **differenz** .
16. Setze die verbleibenden zwei **Innenradius** Blöcke als **radius1** und **radius2** des **zylinders** in den zweiten Abschnitt des **differenz** Blocks mit dem Namen **minus** ein.
17. Stecke einen **Dicke** Block in jede zweite Position von **1 + 1** von **radius1** und **radius2** des **zylinders** im ersten Abschnitt von **differenz** .
18. Schließe schließlich einen **Längen** Block für den Wert **höhen** jedes **zylinder** Blocks an.
19. Drücke **Rendern**.

 **Damit hast du vollparametrisches Design für ein Rohr erstellt!**

Ändere die Werte von ***Länge***, ***Innenradius*** und ***Dicke***, während du nach jeder Änderung auf **Rendern** drückst, um die verschiedenen Ergebnisse anzuzeigen.

<img src="images/2_7_FirstSteps_2_de.png" width="960"/><br>

---

#### [Zurück zum Index](#index)

## 2.8. Pavillon <a name="pavilion"></a>

Bevor wir den Pavillon modellieren, werde ich dir in die Verwendung von Schleifen beim Modellieren nahebringen. Schleifen sind in der Programmierung unverzichtbar. Die Schleife wiederholt eine bestimmte Aufgabe, bis das Programm eine bestimmte Bedingung erreicht hat.

**Im folgenden Beispiel erstellen wir 10 Zylinder nebeneinander durch eine Schleife.**

<img src="images/CountWith.jpg" width="512"/><br>

1. Ziehe per Drag & Drop einen **zähle** Block aus **Schleifen**, einen **verschieben** aus **Transformationen**, einen **1 + 1** aus **Mathematik** und **zylinder** Block von **3D-Formen** auf deinen Arbeitsbereich.
2. Ändere die Werte von **zylinder** in ***radius1: 4*** und ***höhe: 10*** und füge sie in den **verschieben** Block ein.
3. Ziehe den **i** Block von **Variablen** auf den Arbeitsbereich und stecke diesen dann in die erste Position vom **1 + 1** Block anstelle der **1**.
4. Ändere das **+** Zeichen zu ***x*** und ändere die zweite **1** zu **10**.
5. Stecke nun den **i x 10** Block in den **X** Wert von **verschieben**.
6. Setze den **verschieben** Block in den **zähle** Block ein.
7. Drücke **Rendern**.

  **Die Schleife zählt von 1 bis 10 mal 1. "i" ist die Variable, die die aktuelle Zahl der Zählung enthält. Bei jedem Schritt erzeugt die Schleife einen Zylinder und verschiebt ihn um 10 entlang der x-Achse. Es gibt 10 Zylinder mit einem Abstand von 10 mm entlang der x-Achse als Ergebnis.**

  **Lass uns dem Zylinder einige Zufallsfarben hinzufügen!**

8. Ziehe den **zylinder** Block aus **verschieben** heraus.
9. Ziehe einen **farbe HSV** Block aus **Transformationen** und einen **ganzzahlige Zufallszahl** Block aus **Mathematik** auf den Arbeitsbereich.
10. Stecke **ganzzahlige Zufallszahl** in **farbwert** des **color HSV** Blocks und stecke diesen dann in den **verschieben** Block.
11. Stecke nun den **zylinder** Block in **farbe HSV**.
12. Drücke **Rendern**.

  **Jedes Mal, wenn du Rendern drückst, werden die Zylinder mit anderen Farben neu erstellt**.

<img src="images/2_8_FirstSteps_1_de.png" width="960"/><br>

---

<img src="images/Pavilion.jpg" width="512"/><br>

**Jetzt können wir einen Pavillon modellieren.**

1. Ziehe per Drag & Drop drei **zylinder** aus **3D-Formen** und zwei **verschieben** Blöcke aus **Transformationen**.
2. Ändere den Wert des ersten **zylinder** zu ***radius1 = 40, höhe = 5***.
3. Drücke **Rendern**.

  **Das ist die Fussboden! Jetzt bauen wir das Dach mit den anderen Zylindern.**

4. Ändere die Werte des zweiten **zylinder** zu ***radius1 = 38, höhe = 5*** und stecke ihn in den ersten **verschieben** Block. Dann ändere die Werte dieses **verschieben** Blocks zu ***X = 0, Y = 0, Z = 40***.
5. Klick wie im Regenschirmbeispiel auf das **Schlosssymbol** des dritten **zylinder**, um den 2. Radius zu entriegeln.
6. Ändere die Werte dieses **zylinder** in ***radius1 = 38, radius2 = 1 und höhe = 20***.
7. Stecke es dann in den zweiten **verschieben** Block und ändere dessen **Z** Wert zu ***45***.
8. Drücke **Rendern**.

  **Sieh! Der leere Raum zwischen Fundament und dem Dach wird bald mit Säulen gefüllt, die in der Nähe der Außenkante kreisen.**

  **Um zu vermeiden, dass jede Säulen einzeln modelliert wird, verwenden wir wie im Ausgangsbeispiel eine Schleife nutzen, um alle auf einmal zu erstellen.**

  **Jeder Zyklus der Schleife erstellt eine Säule und wendet eine Verschiebung und Rotation darauf an.**

9. Ziehe per Drag & Drop einen **zylinder** Block aus **3D-Formen**, einen **verschieben** Block und einen **rotieren** Block aus **Transformationen**, einen **zähle** Block aus **Schleifen**, und den gerade erstellten Block **i** in **Variablen** auf deinen Arbeitsbereich.
10. Ändere die Werte des **zylinders** zu ***radius1 = 3, höhe = 40***, und stecke ihn in den **verschieben** Block.
11. Gebe dann ***30*** als **X** Wert in den **verschieben** Block ein.
12. Jetzt stecken wir diesen in den **rotieren** Block und verwenden den Variablenblock **i** als **Z** von **rotieren**.
13. Drücke **Rendern**.

  **Wie du siehst, die erste Säule ist erzeugt!**

  **i ist eine Variable, die eine Zahl darstellt. Wir wollen 10 Säulen bauen, die in der Nähe der Außenkante positioniert werden. Dazu müssen wir es mit jeder Schleife drehen, damit die 10 Säulen rund um den äußeren Rand verteilt werden.**

  **Wir werden dies mit dem Block „zähle“ tun. Es verwendet bereits "i", daher müssen wir dies nicht ändern. Der Zähler beginnt mit 1 und endet mit 360, was einer vollen Umdrehung entspricht. Da wir 10 Spalten erstellen möchten, erhöhen wir den Wert „i“ in jeder Schleife um 36.**

14. Ändere die Werte vom **zähle** Block zu ***von: 1 bis: 360 in Schritten von: 36***.
15. Stecke nun den **rotieren** Block mit **verschieben** und **zylinder** in den **zähle** Block.
16. Drücke **Rendern**.
17. Speichere dein Projekt.

<img src="images/2_8_FirstSteps_2_de.png" width="960"/><br>

#### Übung:

**Erstelle ein kleines Modell und multipliziere es mit „zähle“. Du kannst die Verschiebung in alle Richtungen vornehmen oder verschiedene Drehungen verwenden.**

---

#### [Zurück zum Index](#index)

## 2.9. Nadelbaum <a name="pinetree"></a>

<img src="images/PineTree.jpg" width="512"/><br>

**Wir werden nun einen Baum erstellen, der später zum Bauen eines Waldes verwendet werden kann.**

**Damit führe ich dich in die Funktionsweise von Modulen ein. Module sind wie eine Blockbox, die viele viele Blöcke auf einmal enthält. Diese Blockbox wird dann durch einen einzelnen Block repräsentiert.**

**Lass uns zuerst den Baumstamm bauen, dann den Rest mit einem Modul, das wir dadurch kennenlernen werden.**

1. Ziehe einen **zylinder** Block aus **3D-Formen** und eine **farbe** aus **Transformationen** in deinen Arbeitsbereich.
2. Ändere den Wert des **zylinder** Blocks auf ***radius1 = 2, höhe = 10***.
3. Stecke es in den **farbe** Block und ändere die Farbe zu Braun.
4. Drücke **Rendern**.

  **Lassen Sie uns nun die Baumkrone in einer einfachen Form erzeugen. Ach hier werden wir eine Schleife zum Modelieren nutzen.**

5. Ziehe einen **zylinder** aus **3D-Formen**, einen **verschieben** und eine **farbe** aus **Transformationen**, einen **zähle** aus **Schleifen** , einen **1 + 1** Block aus **Mathematik** und den generierten **i** Block aus **Variablen** auf den Arbeitsbereich.
6. Ändere den Wert von **zylinder**, nachdem du auf das Schlosssymbol geklickt hast zu ***radius1 = 10, radius2 = 1, höhe = 10***, und füge den Block dann in **verschieben** ein.
7. Füge die Variable **i** als die erste Position des **1 + 1** Blocks hinzu.
8. Ändere das Operatorsymbol **+** zu **x** und die zweite Zahl zu ***10***.
9. Stecke es nun in den **Z** Wert von **verschieben**, sodass das nächste erstellte Objekt mit jeder Schleife um 10 nach oben verschoben wird.
10. Jetzt stecken wir alles in den **zähle** Block.

  **Wir wollen 3 Teile erstellen, also machen wir 3 Schleifenzyklen.**

11. Ändere die Werte **zähle** zu ***von: 1 bis: 3 in Schritten von: 1***.
12. Stecke als nächstes den gesamten Block, den du gerade erstellt hast, in den **farbe** Block und ändere die Farbe zu Grün.
13. Drücke **Rendern**.

  **Du solltest jetzt eine schöne Kiefer sehen!**

  **Aber es ist nur ein Baum. Ich möchte denselben Baum viele Male verwenden und möchte den Baum nicht jedes Mal duplizieren, wenn ich einen neuen benötige.**

  ***Alle Blöcke, die du in BlocksCAD auswählen kannst, sind eigentlich Module, die du zum Erstellen deiner Objekte verwendet kannst. Du kannst jedoch auch eigenen Module erstellen, um benutzerdefinierte Blöcke zu nutzen.***

  **Deshalb zeige ich dir das mit der Kiefer. Wir werden einen Wald bauen, der das Kiefernmodul als Block verwendet.**

14. Ziehe einen **etwas tun** Block aus **Module** und einen **vereinigung** Block aus **Mengenoperationen** auf deinen Arbeitsbereich.
15. Stecke den **farbe** Block des Stammes einschließlich aller eingebetteten Blöcke und den Baumkronenblock **farbe** mit allen Blöcken in den **vereinigung** Block.
16. Ändern Sie den Namen des Blocks **etwas tun** ***etwas tun*** zu ***Kiefer***, und stecke den kompletten **vereinigung** hinein.
17. Press **Render**.

  **Was!? Nichts zu sehen? Das ist richtig, denn die Kiefer ist jetzt ein Modul.**

  **Wie alle anderen Blöcke musst du ihn zuerst per Drag & Drop auf deinen Arbeitsbereich ziehen.**

18. Ziehe einen **Kiefer** Block aus **Module** auf deinen Arbeitsbereich.
19. Drücke **Rendern**.

  **Der Baum sollte wieder auftauchen.**

  **Wir werden dieses Projekt jetzt speichern und benennen, damit wir es in unserem nächsten Projekt verwenden können**.

20. Lösche nun den Block **Kiefer**, den du gerade hineingezogen hattest.
21. Benenne das Projekt nach dem Namen deines Modules ***Kiefer*** und speichere es.

<img src="images/2_9_FirstSteps_1_de.png" width="960"/><br>


#### Übung:

***Erstelle ähnliche Module, z.B. Blumen, Steine oder ähnliche Dinge, die man in einem Wald findet. Speichere jedes separat. Vergiss nicht, das Modul, das den Block mit dem Modulnamen repräsentiert, vor dem Speichern zu löschen.***

**Tipp: Erstelle zuerst das Objekt, ohne es in den Block "etwas tun" einzufügen. Stecke es erst ein, bevor du den letzten Schritt vor dem Speichern abgeschlossen hast.**

---

#### [Zurück zum Index](#index)

## 2.10. Wald <a name="forest"></a>

<img src="images/Forest.jpg" width="512"/><br>

**Jetzt verwenden wir das Modul "Kiefer", das wir im letzten Tutorial erstellt haben, um einen Wald zu erstellen.**

1. Öffne ein neues Projekt und nenne es ***Wald***.

  **Als nächstes importieren wir unser Baummodul.**

2. Gehe zu **Projekt >> Import Blöcke in dieses Projekt**, wähle ***DEIN_BAUM_PROJEKT_NAME.xml*** und klicke auf **Öffnen**.

  **Da wir das Modul Präsentationsblock nicht auf unserem Arbeitsbereich hatten, sollte beim Drücken von "Rendern" nichts angezeigt werden.**

  **Lass uns den Waldboden bauen.**

3. Ziehe einen **würfel** aus **3D-Formen** und einen **farbe** Block aus **Transformationen** auf deinen Arbeitsbereich.
4. Ändere die Werte von **würfel** zu ***X = 1000, Y = 1000, Z = 1, zentriert***, und stecke es in **farbe**.
5. Wähle nun die Farbe für deinen Untergrund aus, indem du **farbe** änderst. Ich wähle ein helles Grün.
6. Drücke **Rendern**.

  **Dies ist der Ort, an dem wir hundert Bäume nach dem Zufallsprinzip verteilen werden. Der Boden beträgt 1000 mm von Seite zu Seite, der Zufallsgenerator verteilt den Baum innerhalb von 800 mm von beiden Seiten.**

  **Lasse uns 100 Bäume erstellen, die zufällig über den Boden verteilt werden!**

7. Ziehe einen **zähle** aus **Schleifen**, einen **verschieben** aus **Transformationen**, zwei **ganzzahlige Zufallszahl** aus **Mathematik** und **Kiefer** aus **Modulen**.
8. Stecke den **Kiefer** Block in **verschieben** hinein.
9. Stecke eine **ganzzahlige Zufallszahl** als Wert **X** von **verschieben** und eine **ganzzahlige Zufallszahl** als Wert **Y** von **verschieben** ein.
10. Ändere die Werte beider **ganzzahlige Zufallszahl** zu ***zwischen: -400, und: 400***.
11. Setze den **verschieben** Block mit **Kiefer** in **zähle** ein.

  **Jedes Mal, wenn du ein neues "zähle" auf deinen Arbeitsbereich ziehst, ändert sich die Indexvariable. Es beginnt immer mit "i", dann "j", "k", usw. Du kannst auch deinen eigenen Namen oder eine eigene Variable verwenden. Wir bleiben vorerst bei "j".**

12. Ändere den Wert **bis** in **zähle** zu ***100***, da wir 100 Bäume generieren wollen.
13. Drücke **Rendern**.

  **Sei geduldig! Es wird eine Weile dauern. Dies ist eine ziemliche Aufgabe für BlocksCAD.**

  **Da ist der Wald! Aber alle Bäume haben die gleiche Höhe. Schaffen wir noch mehr Abwechslung!**

  **Dazu müssen wir unser "Kiefer" Modul modifizieren.**

  14. Drücke auf das Zahnradsymbol des Blocks **Kiefer** mit allen Blöcken des Modules.
  15. Ziehe **Variable** auf der linken Seite in **Parameter** auf der rechten Seite hinein.
  16. Benenne die Eingabevariable in ***Höhe*** um.

  **Überprüfe nun deinen Modulblock in "zähle j". Kannst du den neuen Schlitz namens "Höhe" sehen?**

  **Aber das ist nicht die einzige neue Sache, die entstanden ist.**

  17. Ziehe drei **Höhe** aus **Variablen** und eine **ganzzahlige Zufallszahl** aus **Mathematik** auf deinen Arbeitsbereich.
  18. Stecke nun alle drei **Höhe** Blöcke in dein **Kiefer** Modul. Zwei für jeden **höhe** der beiden **zylinder** und einer unter **verschieben** anstelle der ***10*** im **i x 10** Block.
  19. Stecke den **ganzzahlige Zufallszahl** Block in den neuen Steckplatz des Moduls, der den Block **Kiefer** als **Höhe** Eingang darstellt.
  20. Ändere die Werte von **ganzzahlige Zufallszahl** in ***zwischen: 10, und: 18***.
  21. Drücke **Rendern**.

  **Sei noch einmal geduldig, es wird eine Weile dauern bis der Wald generiert ist.**

<img src="images/2_10_FirstSteps_1_de.png" width="960"/><br>

---

#### Übung:

***Importiere dein Haus, Pavillon, Regenschirm und deine eigenen Module in die Szene und erstelle ein Bild aus deiner modellierten "Haus im Wald"-Szene. Bringe die Kamera in eine schöne Position und drücke das Kamerasymbol unterhalb des 3D-Viewers, um das Bild zu speichern.***

**Tip:**

**1. Reduziere die Anzahl der Bäume, damit du eine freie Fläche auf dem Boden hast.**

**2. Benutze "verschieben", "rotieren" und "skalieren"  per Drag & Drop, um deine Module und Objekte zu verschieben zu verschieben.**

**3. Deaktiviere den darstellenden "Kiefer" Block, um die langen Renderzeiten zu vermeiden. Kannst du dich erinnern, wie?... Wähle den Block aus und drücke die rechte Maustaste oder halte deinen Finger gedrückt, bis das Popup-Menü erscheint. Drücke auf "Baustein deaktivieren". Mache dasselbe, um es wieder zu aktivieren. Die Auswahl lautet dann "Baustein aktivieren".**

---

#### [Zurück zum Index](#index)

## 3. Wie erzeuge ich 3D druckbare Modelle? <a name="howfor3d"></a>

Modellierungsobjekte, die du mit einem 3D-Drucker drucken kannst, müssen bestimmten Richtlinien folgen, um einen erfolgreichen Druck zu gewährleisten.

* Prüfe, wie klein deine Objekte oder die Einzelheiten deiner Objekte sind. Mache es nicht zu klein, da es beim Drucken ungenau oder unkenntlich werden könnte.
* Stelle dir ein gedrucktes Modell wie eine Schichttorte vor. Der Drucker druckt dein Objekt Schicht für Schicht, von unten nach oben.
* Modelliere dein Objekt mit der größten ebenen Fläche nach unten. Das einfachste Beispiel ist eine Pyramide. Modelliere die Pyramide nicht mit der Spitze nach unten!
* Mache die Wand deines Objektes dick genug, damit der Drucker diese drucken kann. Dies ist besonders wichtig, wenn du Hohlkörper oder Trägermaterial modellierst. Andernfalls werden diese Wände nicht gedruckt.
* Modelliere Teile deines Objekts nicht zu dünn, da diese leicht brechen können.

---

#### [Zurück zum Index](#index)

## 3.1. Fingerring <a name="fingerring"></a>

<img src="images/Fingerring1.jpg" width="512"/><br>

Unser erstes Objekt, das du drucken kannst, wird ein Fingerring sein.

Du kannst mit dem Lineal deinen Fingerdurchmesser messen. Noch einfacher geht es, wenn du bereits einen Ring hast.

**Lass uns anfangen!**

1. Lass uns wie immer ein neues Projekt erstellen und es ***Fingerring*** nennen.

  **Zuerst erstellen wir eine Ringbasis.**

2. Ziehe zwei **zylinder** aus **3D-Formen**, einen **verschieben** aus **Transformationen** und einen **vereinigung** aus **Mengenoperationen** in deinen Arbeitsbereich.
3. Stecke einen **zylinder** in den **verschieben** Block.
4. Ändere die Werte von **zylinder** zu ***radius1: 16, höhe: 3***.
5. Ändere die Werte von **verschieben** zu ***X: 0, Y: 0, Z: 2***.
6. Stecke nun den **verschieben** Block mit dem eingebetteten **zylinder** in den ersten Abschnitt von **vereinigung**.
7. Stecke den zweiten **zylinder** in den zweiten Abschnitt der **vereinigung** mit dem Namen **plus**.
8. Klicke auf das Schlosssymbol dieses **zylinder**, um **radius2** von **radius1** zu trennen.
9. Ändere die Werte dieses **Zylinder** in ***radius1: 14, radius2: 16, höhe: 2***.
10. Drücke **Rendern**.

  **Du solltest jetzt die Basis des Rings sehen.**

  **Lasse uns das Wort "Fun" herausschnitzen.**

11. Ziehe einen **3D-text** Block aus **Text**, einen **verschieben** Block und einen **rotatieren** Block aus **Transformationen** und einen **differenz**-Block aus **Mengenoperationen**.
12. Stecke den **3D-text** Block in den **rotieren** Block hinein.
13. Ändere die Werte von **3D-text** in ***text: fun, größe: 19, schriftart: Chewy, dicke: 3***.

  **Wie du vielleicht schon erkannt hast, steht die Basis des Rings auf dem Kopf. Dies liegt an der Druckanleitung: Beginne mit der Seite, die die größte ebene Fläche hat.**

  **Daher müssen wir den Text um 180 Grad drehen.**

14. Ändere den **Y** Wert von **rotieren** zu ***180*** Grad.
15. Um den Text in die Mitte der Basis zu bringen, ändere die Werte des **verschieben** Block zu ***X: 12, Y: -6, Z: 2***.
16. Drücke **Rendern**.

  **Fun auf dem Knopf!**

  **Lass uns nun den eigentlichen Ring erstellen. Dies ist dem vorherigen parametrischen Rohrbeispiel sehr ähnlich.**

17. Ziehe zwei **zylinder** aus **3D-Formen**, einen **verschieben** und einen **rotieren** aus **Transformationen** und einen **differenz** aus **Mengenoperationen** auf deinen Arbeitsbereich.
18. Stecke einen **zylinder** in den ersten Abschnitt von **differenz** und ändere dessen Wert zu ***radius1: 13, höhe: 5***.
19. Stecke den zweiten **zylinder** in den zweiten Abschnitt von **differenz** namens **minus** und ändere die Werte zu ***radius: 11, höhe: 3***.
20. Stecke den **differenz** Block in **rotieren** und ändere den Wert von **X** zu ***90***.
21. Stecke nun den **rotieren** Block in **verschieben** und ändere den **Z** Wert von **verschieben** zu ***16***. Dies ist die Höhe des gedrehten Rings.
22. Drücke **Rendern**.

  **Du solltest jetzt den Ring mit der Basis und dem eingeschnitztem Text sehen. Allerdings sind die Kanten noch eckig. Lassen Sie uns etwas Glätte hinzufügen!**

23. Ziehe einen **seiten** Block aus **Transformationen** und einen **vereinigung** Block aus **Mengenoperationen** auf deinen Arbeitsbereich.
24. Stecke das Basisteil des Rings in den ersten Abschnitt von **vereinigung**, dann den Ringteil in den zweiten Abschnitt namens **plus**.
25. Ziehe nun den gesamten **vereinigung** Block in **seiten**. Ändere den Wert zu ***80***.
26. Drücke **Rendern**.
27. Benenne dein Projekt und speichere es auf deinem Computer.

<img src="images/3_1_3DPrints_1_de.png" width="512"/><br>

---

#### Übung:

***1. Ändere den Innenradius des Rings auf die Variable "Größe" und ändere den Außenring entsprechend. Dies muss auch mit der Höhe des Rings verbunden sein, welche "Z" vom "verschieben" Block ist.***

***2. Ändere Text, Schriftgröße und Schriftart in "3D-text", passe "verschieben" des Textes so an, dass der Text in der Mitte deiner Basis liegt. Vielleicht möchtest du andere Formen ausprobieren oder Formen anstelle von Buchstaben mischen.***

<img src="images/3_1_3DPrints_2_de.png" width="512"/><br>

---

#### [Zurück zum Index](#index)

## 3.2. Halskette <a name="necklace"></a>

In diesem Beispiel werden wir wieder eine Schleife verwenden, um schöne geometrische Muster zu erstellen, die wir verwenden werden, um eine Halskettenbrosche zu erstellen.

<img src="images/Necklace1.jpg" width="512"/><br>

1. Zuerst erstellen wir ein neues Projekt.
2. Ziehe zwei **zylinder** aus **3D-Formen** und einen **differenz** Block aus **Mengenoperationen** auf deinen Arbeitsbereich.
3. Stecke beide **zylinder** in den **differenz** Block.
4. Ändere die Werte des ersten **zylinders** zu ***radius: 20, höhe: 4*** und die Werte des **zylinders** darunter zu ***radius: 19, höhe: 4***.
5. Press **Render**.

  **Du solltest jetzt einen schönen Ring sehen. Nun, nicht wirklich, aber wir werden die Kanten später glätten.**

  **Jetzt können wir Formen in diesem äußeren Ring hinzufügen! Wir beginnen mit Ringen.**

6. Wiederhole **2.** und **3**.
7. Ändere nun die Werte des ersten **zylinders** zu ***radius: 12, höhe: 4***, und die Werte des zweiten **zylinders** zu ***radius: 11, höhe: 4***.
8. Drücke **Rendern**.

  **Du solltest einen kleineren Ring innerhalb des größeren Rings sehen.**

9. Ziehe einen **verschieben** Block und eine **rotieren** Block aus **Transformationen** auf deinen Arbeitsbereich.
10. Füge den **differenz** Block in **verschieben** ein und ändere die **X**- und **Y**-Werte von **verschieben** zu **5**.

**Du solltest sehen, dass der Ring mit dem äußeren Ring ausgerichtet ist, während er etwas abgesetzt ist.**

11. Stecke nun diesen gesamten Block in **rotieren**.
12. Ändere den **Z** Wert mit anderen Zahlen, drücken Sie jedes Mal auf **Render**.

  **Der Innenring sollte nun um den Außenring „rollen“.**

  **Dies ist, was wir tun möchten, aber jeden Ring soll sich zu einer anderen Position bewegen.**

13. Ziehe einen **zähle** Block aus **Schleifen**, einen **1 + 1** Block aus **Mathematik** und die neue Variable **i**, die in **Variablen** generiert wurde auf deinen Arbeitsplatz.

  **Wir werden etwas Ähnliches tun, was wir beim Bau des Pavillons getan haben.**

  **Was wollen wir also tun? Wir möchten einen Ring erstellen und jeden um das Zentrum drehen. Bei 6 Ringen muss sich jeder Ring um 360 / 6 = 60 Grad drehen.**

14. Setze die Variable **i** in die zweite Position des **1 + 1** Blocks ein und ändere den Wert so, dass er wie **60 x i** aussieht.
15. Stecke nun den gesamten Block **rotatieren** in **zähle**, und ändere die Werte wie folgt >> ***von: 1 bis: 6 in Schritten von: 1***.
16. Drücke **Rendern**.

  **Sieh!? Ein schönes Blumenmuster.**

  **Aber wie befestigen wir es an der Kette? Wir brauchen einen weiteren Ring über dem äußeren Ring.**

17. Gehe zu einem der **differenz** Block, wähle ihn aus und verwende dann die rechte Maustaste oder halte deinen Finger darauf. Wähle **Kopieren** aus dem Popup-Menü.
18. Verschiebe es an den unteren Rand der gesamten Blöcke.
19. Ziehe einen **verschieben** Block aus **Transformationen**. Ändere die Werte zu ***X: 22, Y: 6, Z: 0***.
20. Stecke dann den duplizierten **differenz** Block in **verschieben**. Ändere die Werte des ersten **zylinder** zu ***radius1: 4, höhe: 4***, und die Werte des zweiten **zylinder** in ***radius: 2, höhe: 4***.
21. Drücke **Rendern**.

  **Du solltest jetzt einen Ring am äußeren Rand des großen Rings sehen.**

  **Jetzt können wir die Kanten wieder glätten!**

22. Ziehe einen **vereinigung** Block aus **Mengenoperationen** und einen **seiten** aus **Transformationen** auf deinen Arbeitsbereich.
23. Drücke das **+**-Symbol im **vereinigung** Block, um einen weiteren Steckplatz hinzuzufügen.
24. Setze alle drei Hauptblöcke **differenz**, **zähle** und **verschieben** in jeweils einen Platz von **vereinigung** ein.
25. Stecke dann diesen **vereinigung** Block in **seiten** und ändere die Zahl zu ***80***.
26. Drücke **Rendern**.

  **Beendet! Bereit zum Drucken. Es sollte ähnlich wie im Bild unten aussehen.**

27. Benenne und speichere dein Projekt wie immer.

<img src="images/3_2_3DPrints_1_de.png" width="960"/><br>

---

#### Übung:

**Kopiere deinen „zähle“ Block und deaktiviere den ursprünglichen Block über „Baustein deaktivieren“. Verschiebe die duplizierte Version nach unten und fange an zu experimentieren. Probiere verschiedene Formen aus.**

***Tip:***

***1. Verwende "seiten" zwischen "verschieben" und "differenz" mit 3, 4, 6, um verschiedene Formen einfach zu erstellen. Ändere die Werte in "verschieben", um andere Ergebnisse zu erhalten.***

***2. Erhöhe oder verringere "bis" in "zähle" und Grad in "rotieren" darunter, um das Ergebnis der Muster zu ändern.***

***3. Reduziere die Anzahl der ganzen Seiten (von 80 auf 20 oder noch weniger), wenn du mit Formen experimentierst. Andernfalls dauert das Rendern länger und du musst länger warten. Wenn du mit deinem Design zufrieden bist, erhöhe die Seitenzahl wieder.***

#### Beispiel für Quadrate:

<img src="images/3_2_3DPrints_2_de.png" width="960"/><br>

#### Beispiel für Sechsecke:

<img src="images/3_2_3DPrints_3_de.png" width="960"/><br>

---

#### [Zurück zum Index](#index)

## 3.3. Kopf für Fingerpuppe <a name="fingerpuppet"></a>

<img src="images/Fingerpuppet1.jpg" width="512"/><br>

**Zuerst musst du deinen Zeigefinger messen, um zu wissen, was du später als Werte eingeben musst.**

**Verwende dazu dein Lineal und notiere den Durchmesser deines Zeigefingers knapp über dem zweiten Gelenk. Messe von vorne und von der Seite und nehme dann die größere Zahl.**

**Wenn gerade kein Lineal in der Nähe ist, verwende einfach die Grundwerte in diesem Tutorial. Wir werden den Hals der Fingerpuppe parametrisch modellieren, damit du es später leichter ändern kannst.**

1. Erstelle zunächst ein neues Projekt.
2. Ziehe drei **zylinder** aus **3D-Formen**, einen **differenz** und einen **vereinigung** Block aus **Mengenoperationen** und einen **verschieben** aus **Transformationen**.
3. Füge den **vereinigung** Block in den ersten Abschnitt von **differenz** ein.
4. Füge dann den **verschieben** Block in den zweiten Abschnitt von **vereinigung** namens **plus** ein und ändere die Werte zu ***X: 0, Y: 0, Z: 10***.
5. Stecke nun einen **zylinder** in den ersten Abschnitt von **vereinigung**, einen **zylinder** in den **verschieben** Block im zweiten Abschnitt von **vereinigung** und einen **zylinder** in den zweiten Abschnitt von **differenz** mit dem Namen **minus**.

  **Jetzt können wir den parametrischen Hals deiner Fingerpuppe vorbereiten!**

6. Ziehe einen **setze etwas auf** Block aus **Variablen**, einen **0** Block und einen **1 + 1** Block aus **Mathematik** auf deinen Arbeitsplatz.
7. Klicke auf **etwas** des **setze etwas auf** und benenne es in ***Größe*** um.
8. Ziehe nun den neuen Block namens **Größe** aus **Variablen** in deinen Arbeitsbereich.
9. Stecke den **0** Block in **setze Größe auf** ein und ändere es zu ***10***.
10. Dupliziere nun den **Größe** Block einmal, indem du mit der rechten Maustaste darauf klickst oder deinen Finger auf dem Block hälst, bis sich das Popup-Menü öffnet, damit du **Kopieren** auswählen kannst.
11. Nehme diesen kopierten **Größe** Block und stecke ihn in **radius1** vom **zylinder** im zweiten Abschnitt von **differenz**, genannt **minus**.
12. Klicke im selben **zylinder** auf das Schlosssymbol.
13. Stecke dann den restlichen **Größe** Block in **1 + 1** als erste Zahl ein. Ändere es so, dass er wie **Größe - 1** aussieht.
14. Stecke nun **Größe - 1** in **radius2** des **zylinders** und ändere die **Höhe** auf ***50***.
15. Wähle die **Größe - 1** in **radius2** des **zylinder** Blocks.
16. Kopiere diesen viermal und stecke diese 4 Blöcke für jeden **radius1** und **radius2** der verbleibenden zwei **zylinder**.
17. Klicke im ersten Abschnitt von **vereinigung** auf das Schlosssymbol des **zylinder**. Ändere die Werte, bis sie wie folgt aussehen: ***radius1: Größe + 2, radius2: Größe + 1, höhe: 50***.
18. Ändere nun die Werte des **zylinder** des zweiten Abschnitts von **vereinigung** namens **plus** so, dass er wie folgt aussieht: ***radius1: size + 5, radius2: size + 5, höhe: 4***.
19. Drücke **Rendern**.

  **Du solltest einen Zylinder haben, der ähnlich wie das Rohrbeispiel aussieht, außer dass es einen zusätzlichen Kragenzylinder gibt.**

  **Versuche eine andere Zahl in "Größe" und drücke "Rendern", um die Änderungen anzuzeigen. Hier kannst deine Fingerdicke eingeben.***

  **Der Kragen ist der Teil, der die Puppenkleidung an Ort und Stelle hält.**

  **Jetzt geht es weiter mit dem Kopf. Folge erstmal den Anweisungen, aber du kannst von hier aus wieder beginnen, wenn du deinen eigenen Kopf erstellen möchten.**

20. Drücke viermal auf das **+**-Symbol von **vereinigung**, um vier zusätzliche Spalten zu erstellen. Die Gesamtzahl der Spalten ist nun sechs.

  **Spalte Eins und Zwei sind bereits mit Hals und Kragen belegt. Nummer Drei ist für die Kopfform, Nummer Vier ist für die Nase, Nummer Fünf für die Augen und Nummer Sechs für die Ohren.**

  **Beginnen wir mit dem Kopf.**

21. Ziehe eine **kugel** aus **3D-Formen**, einen **verschieben** und einen **skalieren** Block aus **Transformationen** in deinen Arbeitsbereich.
22. Ändere die Zahl in **kugel** zu ***21*** und stecke sie in **skalieren**.
23. Halte **skalieren** bei ***1, 1, 1*** und stecke es in **verschieben**.
24. Ändere die Werte von **verschieben** zu ***X: 0, Y: 0, Z: 46*** und stecke es in die dritte Spalte von **vereinigung**.
25. Drücke **Rendern**.

  **Super....Charlie Browns Fußballkopf!**

  **Als nächstes die Nase!**

26. Wähle **verschieben** der Kopfkugel und klicke mit der rechten Maustaste oder halte deinen Finger drauf, bis das Popup-Menü erscheint.
27. Wähle **Kopieren** und stecke den gesamten Block in den vierten Steckplatz von **vereinigung**.
28. Ändere nun die Werte von **kugel** zu ***21***, von **verschieben** zu ***X: 0, Y: -18, Z: 46*** und von **skalieren** auf ***X: 1, Y: 1, Z: 0,5***.
29. Dieser **verschieben** Block mit beiden Duplikaten geht in den vierten Spalt von **vereinigung**.
30. Drücke **Rendern**.

  **Sei nicht so ein Schnüffler...die Nase ist nicht das Einzige!**

  **Machen wir weiter mit den Augen!**

31. Ziehe zwei **kugel** Blöcke aus **3D-Formen** und drei **verschieben** aus **Transformationen** auf deinen Arbeitsbereich.
32. Stecke nun jede **kugel** in einen **verschieben** Block, dann beide **verschieben** Blöcke mit ihrem **kugel** Block in den verbleibenden **verschieben** Block.
33. Drücke **+**, um dem Hauptblock **verschieben** eine Spalte hinzuzufügen, sodass beide **verschieben** Blöcke hineinpassen.
34. Ändere den Wert beider **kugel** Blöcke zu ***4***.
35. Ändere dann den **X** Wert von beiden **verschieben** jeweils zu ***5*** und ***-5***.
36. Ändere schließlich die Werte des Hauptblocks **verschieben** zu ***X: 0, Y: -17, Z: 49***.
37. Dieser **verschieben** Block geht nun in den fünften Spalt von **vereinigung**.
38. Drücke **Rendern**.

  **Beide Augen sollten erscheinen. Die Kugeln sind identisch, außer dass die Verschiebung für ein Auge positiv 5 und für ein Auge negativ 5 ist.**

  **Das Kapseln von Blöcken ist sehr nützlich, damit du sie in Beziehung zueinander halten kannst, während du diese Blöcke als Gruppe verwenden kannst.**

  **In diesem Beispiel können wir uns nun mit dem Hauptblock "verschieben" beide Augen gleichzeitig bewegen.**

  **Weiter gehts mit den Ohren!**

39. Ziehe zwei **kugel** Blöcke aus **3D-Formen**, zwei **verschieben** Blöcke und einen **skalieren** Block aus **Transformationen** und einen **differenz** aus **Mengenoperationen** auf deinen Arbeitsbereich.
40. Stecke eine **kugel** in einen **verschieben** Block und ändere den Wert zu ***4***.
41. Nehme diesen **verschieben** Block mit der **kugel** und ziehe ihn in den zweiten Abschnitt von **differenz** namens **minus**. Ändere den Wert **Y** von **verschieben** zu ***-4***.
42. Stecke nun die zweite **kugel** in den ersten Abschnitt von **differenz** und ändere den Wert zu ***5***.
43. Stecke diesen **differenz** Block jetzt in den verbleibenden **verschieben** Block und ändere die Werte von **verschieben** zu ***X:20, Y:0, Z:46***.
44. Wähle diesen **verschieben** Block aus und kopiere ihn.
45. Ändere in der Kopie nun die Werte des **verschieben** Blocks zu ***X:-20, Y:0, Z:46***.
46. Stecke nun diese beiden **verschieben** Blöcke mit ihrem Inhalt in **skalieren** und ändere den **Y** Wert von **skalieren** zu ***0.8***.
47. Der **skalieren** Block kann endlich in den sechsten Spalt von **vereinigung** gehen.
48. Drücke **Rendern**.

  **Okay, soweit so gut!**

  **Aber alles ist ein bisschen "Minecrafty". Lasst es uns wieder ein wenig glätten!**

  **Welchen Block haben wir das letzte Mal verwendet?**

49. Ziehe einen **seiten** Block von **Transformationen** in die Szene und lege ihn dort ab.
50. Nehme nun den gesamten **differenz** Block und stecke ihn in **seiten**.
51. Ändere den Wert zu ***80***.

  **Du kannst eine andere Nummer verwenden, wenn du möchtest. Denke daran, dass das Rendern umso länger dauert, je höher die Zahl ist, da es mehr Quadrate und Dreiecke in der Szene vorhanden sind.**

52. Drücke **Rendern**.
53. Benenne das Projekt und speichere es.

<img src="images/3_3_3DPrints_1_de.png" width="960"/><br>

---

#### Übung:

**Erstelle deinen eigenen Kopf. Beginne damit, das Projekt anders zu benennen, damit du alles behalten kannst, was wir bisher in diesem Projekt gemacht haben.**

***Tip:***

***1. Zeichne zuerst einen Kopf auf Papier, damit du es vor dir siehst, was du erstellen möchtest.***

***2. Erstelle zuerst nur eine Seite der Augen und Ohren und kopiere diese Teile später.***

---

#### [Zurück zum Index](#index)

## 3.4. Schuhbrosche <a name="shoetag"></a>

<img src="images/ShoeTag1.jpg" width="512"/><br>

**Die Shuhbrosche hat Löcher, um deine Schnürsenkel einzufädeln. So kannst du es mit deinen Schuhen tragen.**

1. Beginnen wir wie immer mit einem neuen Projekt.
2. Nennen Sie es ***Schuhbrosche***.

  **Wie beim Fingerring erstellen wir zuerst die Basis.**

  **Auch dieses Mal werden wir es kopfüber aufbauen, da es so einfacher wird, es so zu drucken.**

  **Beginnen wir mit der Basis. Wir werden "hülle" verwenden, das aus allen Objekten innerhalb dieses Blocks ein Objekt erstellt.**

3. Ziehe einen **zylinder** aus **3D-Formen**, einen **verschieben** aus **Transformationen** und einen **hülle** Block aus **Mengenoperationen** auf deinen Arbeitsbereich.
4. Klicke auf das Schlosssymbol bei **zylinder**, um die Verbindung zwischen **radius1** und **radius2** zu entsperren, und ändere die Werte zu ***radius1: 11, radius2: 12, höhe: 7***.
5. Stecke nun den **zylinder** in den **verschieben** Block und ändere seinen **X** Wert zu ***-18***.
6. Wähle den Block **verschieben** aus und dupliziere ihn.
7. Stecke beide **verschieben** Blöcke in **hülle**.
8. Drücke **Rendern**.

  **"hülle" kombinierte beide Zylinder zu einem Element. Der unterschiedliche Radius gibt ihm ein wenig Neigung.**

  **Jetzt schneiden wir einen Teil aus der Basis heraus, um Platz für ein Wort zu schaffen.**

9. Ziehe einen **differenz** Block aus **Mengenoperationen** auf deinen Arbeitsbereich.
10. Wähle die Blockgruppe **hülle** aus und dupliziere sie.
11. Stecke beide **hülle** Blockgruppen in **differenz** hinein.
12. Ändere die Werte von **zylinder** im zweiten Abschnitt von **differenz** namens **minus** zu ***radius1: 9, radius2: 9, höhe: 3***.
13. Drücke **Rendern**.

  **Jetzt solltest du einen Bereich sehen, der aus der Basis herausgeschnitten ist.**

  **Fügen wir das Wort "Flower" hinzu.**

14. Ziehe einen **3D-text** Block aus **Text**, einen **verschieben** und einen **rotieren** Block aus **Transformationen** und einen **vereinigung** aus **Mengenoperationen** auf deinen Arbeitsplatz.
15. Lass uns zuerst **vereinigung** für alle Blockgruppen vorbereiten, indem du das **+**-Zeichen verwendest, um zwei weitere Spalten hinzuzufügen, sodass wir vier komplette Spalten haben.
16. Stecke die Basisblockgruppe in den ersten Steckplatz.
17. Als nächstes füge den **3D-text** Block in **verschieben** ein und ändern die Werte in ***3D-text: FLOWER, größe: 17, dicke: 4, schriftart: Chewy***.
18. Ziehe diesen **verschieben** Block mit dem Text in **rotieren** und ändere die Werte von **verschieben** zu ***X: -26, Y: -6, Z: -4***.
19. Ändere den **Y** Wert von **rotieren** zu ***180***.
20. Stecke die Blockgruppe **rotieren** in den zweiten Steckplatz von **vereinigung**.
21. Drücke **Rendern**.

  **Jetzt steht das Wort.**

  **Aber da ist nichts, an den wir unsere Schnürsenkel befestigen können.**

  **Lass uns das jetzt machen!**

22. Ziehe einen **torus** aus **3D-Formen**, einen **skalieren** Block + einen **verschieben** Block + einen **rotieren** Block aus **Transformationen** auf deinen Arbeitsbereich.
23. Stecke den **torus** Block in **skalieren** und ändere seine Werte zu ***radius1: 8, radius2: 2, seiten: 40, flächen: 20***.
24. Ziehe **skalieren** mit **torus** in den **rotieren** Block und ändere die Werte von **skalieren** zu ***X: 0.5, Y: 1, Z: 2***.
25. Als nächstes nehme diesen **rotieren** Block und stecke in **verschieben**.
26. Ändere den **Y** Wert von **rotieren** zu ***90***.
27. Ändere schließlich die Werte von **verschieben** auf ***X: 18, Y: 0, Z: 8***.
28. Drücke **Rendern**.

  **Wir haben ein Senkelloch einer Seite erstellt.**

  **Bevor wir mit dem zweiten Loch fortfahren, ändere die Positionen von "verschieben", "rotieren" und "skalieren" innerhalb der Blockgruppe. Rendere es, nachdem du es geändert hast.**

  **Kannst du sehen!? In diesem Fall ist die Reihenfolge wichtig. Das Programm beginnt mit der niedrigsten Instanz, dem "torus", dann führt es die nächste Aktion darüber aus, so viele wie in einer gekapselten Gruppe sind.**

  **Stelle die ursprüngliche Reihenfolge wieder her, aber denke daran, wenn du deine eigenen Designs modellierst.**

  **Zurück zum zweiten Loch für deine Schnürsenkel.**

29. Wähle die Blockgruppe **verschieben** aus, an der wir gearbeitet haben, und dupliziere sie.
30. Da alles ausser der Position gleich bleibt, ändere den **Y** Wert eines **verschieben** Blocks zu ***-18***.
31. Stecke beide **verschieben** Blockgruppen in den dritten und vierten Spalt von **vereinigung**.

  **Bevor wir ein endgültiges Rendering durchführen, erhöhen wir die Glätte durch mehr Quadrate, die zum Erstellen des Modells verwendet werden.**

32. Ziehe eine **seiten** von **Transformationen** in deinen Arbeitsbereich.
33. Stecke die gesamte Blockgruppe **vereinigung** in **seiten** und ändere dann den Wert zu ***80***.
34. Drücke **Rendern**.

<img src="images/3_4_3DPrints_1_de.png" width="960"/><br>

---
#### Übung:

**Füge dem Schuhbrosche oder Schuhtag deine eigenen Worte hinzu oder verwende andere Formen, um eine Grafik zu erstellen.**

---

#### [Zurück zum Index](#index)

## 3.5. Robot 1 & 2 <a name="robots"></a>

#### Robot 1

<img src="images/Robot1.jpg" width="512"/><br>

Dieser Roboter hat die meisten Teile aller anderen Modelle, die du bisher gebaut hast.

Ich zeige dir, wie du die Dinge einfacher machen kannst, wenn Sie zuerst nachdenken. Auch eine Skizze einer neuen Sache zu erstellen kann sehr hilfreich sein.

**Also lasst uns anfangen!**

1. Öffne zuerst ein neues Projekt und nennen es ***Roboter***.

  **Wir machen den Körper und den Kopf, einschließlich der Antenne.**

  **Beginnen wir mit dem Kopf von Robby.**

2. Ziehe einen **zylinder** und einen **kugel** Block aus **3D-Formen**, eine **skalieren** und zwei **verschieben** Blöcke aus **Transformationen** und einen **hülle** Block aus **Mengenoperationen** auf deinem Arbeitsbereich.

  **Wir verwenden hülle, um eine Form aus einer Kugel und einem Zylinder zu erstellen.**

3. Füge dem **zylinder** folgende Werte hinzu: ***radius1 = 17, höhe = 10***.
4. Der **kugel** Block hat ebenfalls einen Radius von ***17***.
5. Stecke die **kugel** in den **skalieren** Block und ändere den Wert von **Z** zu ***0,7***.
6. Stecke den **skalieren** Block in einen **verschieben** Block.
7. Ändere dessen Werte von **Z** zu ***15***.
8. Drücke **Rendern**.

  **Dies sind also die beiden Objekte, die wir zu einem kombinieren möchten.**

9. Stecke **verschieben** mit der Kugel darin in die erste Position von **hülle**.
10. Setze dann den **zylinder** in die zweite Position, die **mit** genannt wird.
11. Rendere nochmal! Sieh!?
12. Stecke nun den **hülle** Block in den zweiten **verschieben**, damit wir den Kopf einsetzen können.
13. Ändere den **Z** Wert von **verschieben** zu ***45***.

 **Als nächstes erstellen wir den Körper, indem wir den gesamten Block duplizieren**

14. Kopiere den gesamten soeben erstellten Block.

  **Beginne damit, die Werte der Kopie von oben nach unten zu ändern.**

15. Ändere den **Z** Wert des oberen **verschieben** zu ***18***.
16. Ändere den **Z** Wert des **verschieben** Blocks an der ersten Position des **hülle** Blocks in ***19***.
17. Ändere den Radius der **kugel** zu ***14***.
18. Ändere **radius1** des **zylinders** im zweiten Spalt von **hülle** zu ***15*** und die **höhe** ebenfalls zu ***15***.
19. Drücke **Rendern**.

  **Jetzt solltest du den Körper und den Kopf sehen!**

  **Als nächstes erstellen wir die Antenne auf dem Kopf und gruppieren alle zusammen.**

20. Ziehe einmal **vereinigung** aus **Mengenoperationen**, einen **zylinder** und eine **kugel** Block aus **3D-Formen** und zwei **verschieben** aus **Transformationen** auf deinen Arbeitsplatz.
21. Ändere die Werte von **zylinder** zu ***radius1 = 2, höhe = 10***.
22. Stecke es nun in ein **verschieben** und ändere die **Z** Achse zu ***71***.
23. Fahre mit der **kugel** fort, indem du den Radius zu ***3,5*** änderst.
24. Stecke es in den zweiten **verschieben** Block und ändere seinen **Z** Wert zu ***83***.
25. Lasse uns alle 4 Blöcke gruppieren, indem du 4 freie Spalten in **vereinigung** erstellst. Benutze dazu zweimal das **+**-Symbol.
26. Stecke alle Teile ein.
27. Drücke **Rendern**.

  **Jetzt sollte eine Antenne am Kopf auf seinem Rumpf sehen.**

  **Lasse uns aus diesem gesamten *vereinigung* Block ein Modul erstellen.**

28. Ziehe einen **etwas tun** Block aus **Modulen** in deinen Arbeitsbereich.
29. Stecke nun den gesamten **vereinigung** Block in den **etwas tun** Block.
30. Benenne **etwas tun** in ***Kopf*** um.
31. Ziehe nun den neuen Block namens **Kopf** aus **Modulen** auf deinen Arbeitsbereich.
32. Drücke **Rendern**.

  **Jetzt erstellen wir das Bein, den Arm und das Auge auf einer Seite, beginnend mit dem Bein.**

  **Da der Körper und der Kopf dem Bein ähnlich sind, kannst du erneut eine Kopie von beiden erstellen, indem du auf das Blockgruppe *Kopf* klicken, welche die Blöcke *verschieben* und *hülle* enthält.**

33. Benenne die Kopie von **Kopf** zu ***Linke Seite*** um.
34. Ändere die Werte von **verschieben** zu ***X = 8, Y = 0, Z = 0***.
35. Ändere im zweiten ***verschieben*** Block die Werte zu ***X = 0, Y = 0, Z = 5***.
36. Der Radius des **kugel** Blocks wird ***7***, ebenso des **zylinder** Blocks **radius1** und einer **höhe** von ***5***.
37. Drücke **Rendern**.

  **Dies ist der Fuß. Nun müssen wir den Rest des Beins hinzufügen.**

38. Ziehe einen **zylinder** aus **3D-Formen** auf deinen Arbeitsbereich.
39. Ändere die Werte von **zylinder** zu ***radius1 = 3*** und ***höhe = 30***.
40. Verwende nun das **+**-Symbol des **translate** Blocks vom Fuss, um einen weiteren Spalt für das Bein zu erhalten.
41. Stecke den **zylinder** Block dort hinein.
42. Drücke **Rendern**.

  **Du solltest jetzt das Bein mit dem Fuß sehen, der mit dem Körper verbunden ist.**

  **Weiter gehts mit dem Arm!**

37. Ziehe zwei **kugel** und einen **zylinder** Block aus **3D-Formen**, zwei **verschieben** + einen **rotieren** Block aus **Transformationen** und einen **vereinigung** Block von **Mengenoperationen** auf deinen Arbeitsraum.

  **Beginne mit der Schulter.**

38. Ändere den Radius einer **kugel** zu ***6*** und stecke sie dann in **rotieren**.
39. Ändere den Wert **Y** Achse von **rotieren** zu ***80*** Grad.

  ***Du musst die Zahl hineinschreiben, da das grafische Rad keine 80-Grad-Einstellung hat.***

40. Stecke als nächstes den **rotieren** Block in einen **verschieben** Block und ändere dort die Werte zu ***X = 14, Y = 0, Z = 37***.
41. Für den Rest des Arms nehme den anderen **verschieben** Block und nutze das **+**-Symbol, um einen zweiten Spalt hinzuzufügen.
42. Ändere die Werte dieses **verschieben** Block zu ***X = 18, Y = 0 und Z = 20***.
43. Nehme die zweite **kugel** und ändere ihren Radius zu ***5***, dann stecke sie in einen Spalt dieses **verschieben** Blocks mit 2 Spalten.
44. Ändere nun die Werte von **zylinder** in ***radius1 = 3 und höhe = 16***.
45. Stecke den **zylinder** dann in den zweiten **rotieren**-Block. Die **Y**-Achse muss hier ***350*** Grad betragen.
46. Stecke nun den **rotieren** Block mit dem **zylinder** in den zweiten freien **verschieben** Spalt.
47. Dann gruppiere die Schulter und den Arm mit der Hand, indem du alles in den **vereinigung** Block steckst. Füge mit **+** einen weiteren Steckplatz für **vereinigung** hinzu.

  **Jetzt erstellen wir das Auge auf dieser Seite!**

48. Ziehe einen **kugel** und einen **zylinder** Block aus **3D Shapes**, zwei **verschieben** + zwei **rotieren** + einen **skalieren** Block von **Transformationen** auf die Arbeitsfläche.
49. Beginnen wir damit, die **zylinder** Werte in ***radius1 = 5, höhe = 8*** zu verändern.
50. Stecke den **zylinder** Block in einen **rotieren** Block.
51. Ändere die **X**-Drehung zu ***90*** Grad.
52. Stecke es dann in einen **verschieben** Block und ändere diese Werte zu ***X = 7, Y = -10, Z = 54***.
53. Füge diesem **verschieben** Block einen weiteren Slot hinzu, indem du das **+**-Symbol verwendest.

  **Wir bauen jetzt den anderen Block, der dort eingesteckt wird.**

54. Ändere den Radius der **kugel** auf ***4*** und stecke sie dann in den zweiten **rotieren** Block.
55. Ändere die **X**-Drehung auf ***90*** Grad und stecke diese in den **skalieren** Block.
56. Der **skalieren** Wert von **Y** muss zu ***0.6*** geändert werden.
57. Stecke dieses Modul, das die **kugel** enthält, in den zweiten **verschieben** Block.
58. Ändere dort den **Y** Wert zu ***-8***.
59. Dieser Block geht nun in den freien Steckplatz des ersten **verschieben** Blocks.
60. Lasse uns nun das Bein, den Arm und das Auge gruppieren, indem du den übrig gebliebenen **vereinigung** Block verwendest. Du musst einen neuen Steckplatz hinzufügen.
61. Ziehe den neuen Block **Linke Seite** von **Modulen** auf den Arbeitsbereich.
62. Drücke nun **Rendern**.

  **Jetzt solltest du auch das Auge mit allen anderen Teilen auf einer Seite sehen.**

  **Wir müssen nicht alles für die andere Seite neu modellieren.**

63. Ziehe einfach einen **spiegeln quer** von **Transformationen** in den Arbeitsbereich.
64. Klicke auf **XY** und ändere es zu ***YZ***.
65. Erstelle dann eine Kopie des representierenden Modulblocks **Linke Seite**.
66. Stecke es in den **spiegeln quer** Block.
67. Drücke **Rendern**.

  **Der Roboter ist fertig!**

  **Aber halt, wir wollen den Roboter etwas glatter haben. Also gruppieren wir alle Teile wieder in einem "seiten" Block!**


68. Ziehe eine **vereinigung** von **Mengenoperationen** und einen **seiten** Block von **Transformationen** in den Raum.
69. Verwende die **+**-Taste, um einen Steckplatz hinzuzufügen und füge alle 3 Modulblöcke **Kopf**, **Linke Seite** und **spiegeln quer** mit dem zweiten **Linke Seite** Block in **vereinigung** hinzu.
70. Stecke nun den **vereinigung** Block in **seiten** und ändere den Wert zu ***40***.
71. Drücke **Rendern**.

  **A voila!**

<img src="images/3_5_3DPrints_1_de.png" width="960"/><br>

---

#### Robot 2

<img src="images/CuteRobot1.jpg" width="512"/><br>

Hier ist ein zweiter Roboter, der dem Roboter Marvin aus "Per Anhalter durch die Galaxis" ein wenig ähnelt.

  **Beginnen wir mit dem Kopf!**

1. Ziehe eine **kugel** aus **3D-Formen**, einen **verschieben** und einen **rotieren** Block aus **Transformationen** auf deinen Arbeitsbereich.
2. Setze den Wert von **kugel** zu ***17*** und stecke sie in **rotieren**.
3. Ändere die Werte vom **rotieren** Block zu ***X: 5, Y: 355, Z: 0***, und füge ihn in **verschieben** ein.
4. Ändere die Werte von **verschieben** zu ***X: -2, Y: 0, Z: 48***.
5. Drücke **Rendern**.

  **Jetzt können wir die Augen erstellen.**

6. Kopiere den gesamten Block, den du gerade für den Kopf erstellt hast.
7. Ändere die Werte wie folgt: **kugel** zu ***3***, **rotieren** zu ***X: 0, Y: 90, Z: 90*** und **verschieben** zu ***X: 0, Y: -18, Z: 45***.
8. Kopiere nun diesen gesamten Block, behalte den Wert von **kugel** bei, während du die Werte von **rotieren** zu ***X: 0, Y: 90, Z: 60*** und **verschieben** zu ***X: -14, Y: -13, Z: 44,5*** veränderst.
9. Ziehe nun einen **vereinigung** Block und einen **differenz** Block aus **Mengenoperationen** in deinen Arbeitsbereich.
10. Stecke beide **kugel** Augenblöcke in den **vereinigung** Block.
11. Stecke dann diesen **vereinigung** Block in die zweite Spalte von **differenz** namens **minus**.
12. Stecke den Kopfblock **verschieben** in den ersten Slot von **differenz**.
13. Drücke **Rendern**.

  **Dies ist der Kopf, in den jetzt zwei Augen eingraviert sind!**

  **Weiter geht es mit der Torsokugel!**

14. Ziehe eine **kugel** aus **3D-Formen** und eine **verschieben** Block aus **Transformationen** auf deinen Arbeitsbereich.
15. Setze den Wert von **kugel** auf ***15*** und stecke ihn in den **verschieben** Block.
16. Ändere die Werte von **verschieben** zu ***X: 0, Y: 2, Z:20***.
17. Drücke **Rendern**.

  **Lass uns nun das Bein mit Fuß unseres kleinen Roboters erstellen, beginnend mit dem Bein!**

  **Auch hier verwenden wir später *spiegeln quer* für das zweite Bein.**

18. Ziehe einen **zylinder** Blokc aus **3D-Formen**, einen **rotieren** und einen **verschieben** Block aus **Transformationen** auf deinen Arbeitsbereich.
19. Ändere den Wert von **zylinder** in ***radius1: 2.5, höhe:8*** und stecke ihn in **rotieren**.
20. Ändere die Werte vom **rotieren** Block zu ***X: 0, Y: 330, Z: 0***.
21. Stecke dann **rotieren** in den **verschieben** Block und ändere dessen Werte zu ***X: 11, Y: 2, Z: 2***.
22. Drücke **Rendern**.

  **Das Bein ist fertig. Jetzt erstellen wir den Fuß, indem wir eine Kugel verwenden und dies halbieren.**

23. Ziehe eine **kugel** sowie einen **würfel** aus **3D-Formen**, zwei **verschieben** Blöcke + einen **rotieren** Block + einen **skalieren** Block aus **Transformationen** und ein **differenz** Block von **Mengenoperationen** auf deinen Arbeitsplatz.
24. Ändere den Wert von **kugel** zu ***12*** und stecke ihn in **skalieren**.
25. Ändere die Werte von **skalieren** zu ***X: 0.6, Y: 1, Z: 0.6***, und stecke es in den ersten Steckplatz von **differenz**.
26. Nimm den **würfel** Block, ändere die Werte zu ***X: 30, Y: 30, Z: 10, zentriert*** und stecke ihn in den zweiten Steckplatz von **differenz**.
27. Stecke nun den gesamten **differenz** Block in **rotieren** ein.
28. Ändere den **Z** Wert von **rotieren** zu ***30***.
29. Dann setze **rotieren** mit **differenz** in **verschieben** ein und ändere den **X** Wert von **verschieben** zu ***11***.
30. Drücke **Rendern**.

  **Jetzt solltest du ein Bein und einen Fuß auf einer Seite sehen.**

  **Lasse uns aus dem Bein ein Modul machen und es spiegeln!**

31. Ziehe einen **vereinigung** Block aus **Mengenoperationen** und einen **etwas tun** Block aus **Module** auf den Arbeitsbereich.
32. Benenne den Block **etwas tun** in **Bein** um und ziehe den neu generierten representierenden **Bein** Block aus **Module** auf deinen Arbeitsbereich.
33. Stecke nun die Blocksammlung des Fußes **verschieben**  und **verschieben** des Beines in **vereinigung**, dann den **vereinigung** Block in das Blockmodul **Bein**.
34. Drücke **Rendern**.

  **Jetzt ist das Bein bereit, gespiegelt zu werden.**

  ***Wenn du das Bein nicht sehen kannst, hast du bestimmt vergessen, den representierenden Block "Bein" aus "Module" einzufügen!***

35. Kopiere jetzt den representierenden Block **Bein** einmal und ziehe **spiegeln quer** von **Transformationen** auf deinen Arbeitsplatz.
36. Ändere den Wert von **spiegeln quer** zu ***YZ*** und stecke ein representierenden **Bein** Modulblock hinein.
37. Drücke **Rendern**.

  **Du solltest jetzt 2 Beine mit Füßen in den richtigen Positionen sehen.**

  **Bereit, ein weiteres Modul für den Arm zu erstellen? Lass uns loslegen!**

38. Ziehe einen **zylinder**, einen **würfel** und eine **kugel** aus **3D-Formen** auf deinen Arbeitsplatz.
39. Ziehe außerdem einen **skalieren**-, zwei **verschieben** und zwei **rotieren** Blöcke aus **Transformationen** hinein.
40. Ziehe schließlich einen **vereinigung**- und einen **differenz** Block aus **Mengenoperationen** + einen **etwas tun** Block aus **Module** auf deinen Arbeitsbereich.
41. Benenne **etwas tun** zu ***Arm*** um und ziehe es in den **vereinigung** Block.
42. Ändere die Werte von **zylinder** in ***radius1: 2,5, höhe: 8*** und stecke es in den zweiten Steckplatz von **vereinigung**.

  **Jetzt können wir etwas Ähnliches wie den Fuß erstellen. Dieses Mal werden wir es als Arm verwenden.**

43. Ändere den Wert von **kugel** zu ***12*** und stecke ihn in **skalieren**.
44. Setze die Werte von **skalieren** auf ***X: 0.6, Y: 1, Z: 0.6***, und stecke es in den ersten Slot von **differenz**.
45. Ändere nun die Werte von **würfel** zu ***X: 30, Y: 30, Z: 10, zentriert***, und füge es in **verschieben** ein.
46. Ändere den Wert **Z** von **verschieben** zu ***-5*** und stecke diesen in den zweiten Spalt von **differenz** namens **minus**.

  **Wir müssen den Arm etwas nach oben bewegen, da die Mitte des Arms die Unterseite des Zylinders ist.**

47. Setze **differenz** in den zweiten **verschieben** Block ein und ändere diese Werte zu ***X: 0, Y: -6, Z: 8***.
48. Stecke nun den **verschieben** Block in den ersten Steckplatz von **vereinigung**.
49. Ziehe den representierenden **Arm** Modulblock, der in **Module** generiert wurde, auf deinen Arbeitsbereich.
50. Drücke **Rendern**.

  **Nun, das sieht seltsam aus, nicht!?**

  **Lass uns den Arm in eine schöne Position bringen**

51. Ziehe einen **verschieben** Block und einen **rotieren** Block von **Transformationen** auf deinen Arbeitsplatz.
52. Nehme den **Arm** Modulblock und stecke ihn in **rotieren**.
53. Ändere die Werte von **rotieren** zu ***X: 255, Y: 210, Z: 24***, und füge den gesamten Block in **verschieben** ein.
54. Ändere die Werte von **verschieben** nun zu ***X: -7, Y: 2, Z: 24***.
55. Drücke **Rendern**.

  **Siehst du!? Verwenden wir nun das Modul "Arm" für die andere Seite, und ändern wir die Position ein wenig, damit die Arme nicht die selbe Position haben.**

56. Kopiere den **verschieben** Block, einschließlich **rotieren** und Modulblock **Arm**.
57. Ziehe einen **spiegeln quer** Block von **Transformationen** in den Arbeitsbereich und ändere den Wert zu ***YZ***.
58. Stecke das Duplikat des Arm hinein und ändere die **rotieren** Werte zu ***X: 0, Y: 300, Z: 330***.
59. Drücke **Rendern**.

  **Nun, da ist Marvin. Aber hey, lass uns die Oberfläche glätten. Der Druck wird dann viel besser aussehen!***

60. Ziehe einen **vereinigung** Block aus **Mengenoperationen** und einen **seiten** Block aus **Transformationen** auf deinen Arbeitsbereich.
61. Verwende das **+**-Symbol, um **vereinigung** vier weitere Spalten hinzuzufügen.
62. Stecke Kopf, Rumpf, beide Arme und beide Beine in den **vereinigung** Block.
63. Stecke dann den gesamten **vereinigung** Block in **seiten** und ändere den Wert zu ***80***.
64. Drücke **Rendern**.

  **Sei geduldig, das sind viele Polygone (Viele Winkel), die BlocksCAD rendern muss.**

  ***Wenn du die Pose deines Roboters verändern möchtest, ändere den Wert für "seiten" auf etwa 12.***

  ***Das verkürzt die Renderzeit enorm!***

<img src="images/3_5_3DPrints_2_de.png" width="960"/><br>

---

#### [Zurück zum Index](#index)

## 4. Drucke es aus! <a name="printing"></a>

In diesem Abschnitt werde ich den den eigentlichen Prozess des 3D-Drucks vorstellen.

Bevor wir ein 3D-Objekt drucken, das als STL-Datei exportiert wurde, müssen wir es in Scheiben schneiden und es als Skript exportieren (Hier gibt es eine Ausnahme: Wenn du Slic3r als Druckerkontrollsoftware benutzt, ist es nichtwendig eine Skript datei zu speichern).

Das Skript heißt auch GCode, das dem Drucker die genauen Informationen gibt, wohin er sich bewegen soll, wie schnell und viele andere Informationen.

Das Slicing oder Scheibenschneiden hängt von Faktoren ab, die wir mit einer Software anpassen, die über einen Browser aufgerufen oder auf deinem Computer installiert kannst.

Nachdem du den GCode deines Objekts erfolgreich generiert hast, zeige ich dir, wie du den Drucker steuerst, um das Design auszudrucken.

---

#### [Zurück zum Index](#index)

## 4.1. Mein 3D Drucker <a name="3dprinter"></a>

Der Drucker, den ich als Einsteigerdrucker für Kinder wähle, ist der EasyThreedX2 (ca. 100 Euro).

<img src="images/4_1_PrintIt_1.png" width="960"/><br>

* Der Drucker ist einfach zu bedienen und hat ziemlich gute Bewertungen.
* Es ist ein kleiner Drucker, der eine Druckfläche von 10 x 10 x 10 cm hat und nur 30 Watt Energie verbraucht. Es hat erstaunliche Druckergebnisse für seinen niedrigen Preis.
* Wenn etwas nicht funktioniert, ist es leicht zu beheben (aus eigener Erfahrung).
* Der Drucker druckt PLA-Kunststoff, einen Bio-Kunststoff auf Stärke basisierend.
* Mit seinem geringen Gewicht und seiner Größe passt der Druck in Taschen oder Faltkörbe, um ihn zur Schule oder zu Freunden zu mitzunehmen.

<img src="images/4_1_PrintIt_2.png" width="960"/><br>

---

#### [Zurück zum Index](#index)

## 4.2. Den Druck vorbereiten <a name="prepareprint"></a>

Checkliste für den Drucker:

* Hast du genug Filament auf deiner Spule?
* Haben Sie das blaue Klebeband ersetzt, das nicht wiederverwendet werden kann?
* Gibt der Druckkopf beim Vorheizen Plastik ab?
* Ist die Druckplatte sauber?


### 4.2.1 Benutze die kiri:moto webseite

Dieser Abschnitt zeigt dir, wie du die STL Datein in ein GCODE-Skript mit einer Webbrowser-Anwendung transformierst.

[kiri:moto - Free to use](https://grid.space/kiri/)

  <img src="images/kirimoto.png" width="960"/><br>

* Verwende die folgenden Einstellungen, wenn du einen EasyThreed X1 oder EasyThreed X2 Drucker besitzt:
* Der Webbrowser speichert deine Einstellungen auf deinem Gerät und verwendet sie automatisch, wenn Sie diese URL das nächste Mal öffnest.
* Daher müssen die nächsten Schritte nur einmal durchgeführt werden.
* Es gibt jedoch Einstellungen in Infill (Füllung) und Support (Unterstützung), die sich abhängig von deinem zu druckenden Objekt ändern können.

  <img src="images/4_2_1_browserslicer_kirimoto_1.png" width="960"/><br>

* Beginnen wir mit der Einrichtung, indem du ***>> Setup >> Machine*** auswählst .

  <img src="images/4_2_1_browserslicer_kirimoto_2.png" width="960"/><br>

* Wählen aus den Standardgeräten ***Creality Ender 6*** aus, da es dem EasyThreeD X1/2 Drucker ähnelt.
* Drücke dann ***Customize***.

  <img src="images/4_2_1_browserslicer_kirimoto_3.png" width="512"/><br>

* Jetzt kannst du den Namen ändern, um dein eigenes Gerät zu erstellen. Du wirst die benutzerdefinierten Drucker in der Liste **My Devices** wiederfinden.
* Ändere als Nächstes die Breite, Tiefe und Höhe des Druckraumes zu ***100***.
* Drücke ***Save***.

  <img src="images/4_2_1_browserslicer_kirimoto_4.png" width="512"/><br>

  * Wie du siehst hat sich der Druckbereich geändert.
  * Gehe nun zum rechten Seitenbereich und wähle jeden Abschnitt Schritt-für-Schritt aus.
  * Ändere die Werte wie in den Bildern unten gezeigt wird.
  * Dadurch werden die Druckeinstellungen eingerichtet.

  <img src="images/4_2_1_browserslicer_kirimoto_5.png" width="960"/><br>

* **Layers:** Hier können Sie wählen, wie fein der Druck gedruckt werden soll.
* Je höher die Schicht, desto rauer der Druck und desto kürzer ist die Druckzeit.
* Je niedriger die Schichthöhe, desto feiner der Druck, aber desto länger dauert die Druckzeit.

  <img src="images/4_2_1_browserslicer_kirimoto_6.png" width="256"/><br>

* **Base:** dreht sich alles um das untere Ende des Drucks und der Druckgeschwindigkeit.
* Viele der Werte sind **0**, was dann die Standardwerte sind, die in anderen Einstellungsbereichen festgelegt werden. z.B. Düsentemperatur in **Output**
* Ändere zum Beispiel die Temperatur, wenn du niedriger oder höher als 210 Grad Celsius drucken möchtest.

  <img src="images/4_2_1_browserslicer_kirimoto_7.png" width="256"/><br>

* **Infill:** bestimmt die Füllung Ihres Objekts, welches Muster und wie dicht es gedruckt werden soll.
* Mit **Fill type** bestimmst du, welches Muster die Füllung haben soll.
* Mit **Fill fraction** bestimmst du die Dichte (0: Keine Füllung, 1: Vollfüllung)

  <img src="images/4_2_1_browserslicer_kirimoto_8.png" width="256"/><br>

* **Support:** legt die Werte der Tragestruktur fest.
* Hier können Sie hauptsächlich mit **Density** (Dichte) und **Pillar size** (Säulengröße) experimentieren.
* Drücke **Detect**, um die Schätzung des Trägermaterials anzuzeigen, sobald du dein Objekt geladen hast.

  <img src="images/4_2_1_browserslicer_kirimoto_9.png" width="256"/><br>

* **Output:** sind die Werte von Düsentemperatur, Lüftergeschwindigkeit und Druckgeschwindigkeit.
* Du wirst diese Werte nicht viel ändern.
* Überschreite nicht den Wert von 40 mm/s in **Print speed**, da dies die maximale Druckgeschwindigkeit des EasyThreeD X1/2 Druckers ist.

  <img src="images/4_2_1_browserslicer_kirimoto_10.png" width="256"/><br>

* **Expert:** bleibt wie es ist.

  <img src="images/4_2_1_browserslicer_kirimoto_11.png" width="256"/><br>

* **Profile:** speichert deine Druckeinstellungen, damit du sie in Zukunft aufrufen kannst, besonders wenn du unterschiedliche Drucker nutzt.

  <img src="images/4_2_1_browserslicer_kirimoto_12.png" width="960"/><br>

  **Nachdem du die Druckereinstellungen übernommen hast, importiere ein Objekt und schneiden es in Scheiben:**

1. Gehe zu **>> Files >> Import**, und wähle deine Objektdatei im .stl Format.

  <img src="images/4_2_1_browserslicer_kirimoto_13.png" width="960"/><br>

2. Es sollte in der Mitte deines Druckbereichs angezeigt werden. Du kannst weitere Objekte importieren, die dann auf der Druckplattform neu angeordnet werden.

  <img src="images/4_2_1_browserslicer_kirimoto_14.png" width="960"/><br>

3. Im Beispiel von Marvin benötigst du gedruckte Unterstützung. Wähle **>> Support >> Detect**, um die Schätzung anzuzeigen. Teste verschiedene Werte, wenn du zu wenig oder zu viel Trägermaterial hast. So viel wie nötig, so wenig wie möglich!

  <img src="images/4_2_1_browserslicer_kirimoto_15.png" width="960"/><br>

4. Gehe nun zu **>> Slice >> Preview** und klicke darauf , um eine Vorschau zu rendern.

  <img src="images/4_2_1_browserslicer_kirimoto_16.png" width="960"/><br>

  **Das Vorschau-Rendering berechnet jede Druckebene gemäß den Druckeinstellungen in deinem Profil.**

  <img src="images/4_2_1_browserslicer_kirimoto_17.png" width="960"/><br>

5. Verwende den Schiebern im unteren Teil, um die Fülldichte und das Trägermaterial zu überprüfen.

  **Überprüfe, ob es druckbar ist (nichts beginnt in der Luft, sondern wird von unten nach oben aufgebaut)**

  **Diese Option kann langsam sein, da dies die Grafik deines Geräts belastet. Sei geduldig! Verschiebe die Schieberegler ein wenig und warte, bis die Ansicht aktualisiert ist!**

  <img src="images/4_2_1_browserslicer_kirimoto_18.png" width="960"/><br>

  **Wenn du mit der gerenderten Vorschau zufrieden bist, kannst du sie als .gcode-Datei exportieren.**

6. Drücke **>> Slice >> Export**.

  <img src="images/4_2_1_browserslicer_kirimoto_19.png" width="960"/><br>

  **Jetzt wird jede Ebene erneut berechnet. Danach wird ein Popup-Fenster mit Details und einer Exportschaltfläche geöffnet.**

7. Drücke ***Download***, und speichere die Datei.

  <img src="images/4_2_1_browserslicer_kirimoto_20.png" width="960"/><br>


**Als nächstes kannst du mit Abschnitt 4.3 fortfahren.**

---

### 4.2.2 Benutze das Slic3r Programm um Objekte in Scheiben zu schneiden

Wenn du einen PC oder Laptop hast, kannst Sie mit slic3r dein exportiertes 3D-Objekt für den 3D-Druck vorbereiten.

[Slic3r - Free open-source software](https://slic3r.org/download/)

  <img src="images/slic3r.png" width="960"/><br>

#### Importiere die Konfigurationsdatei für den EasyThreedX2-Drucker (nur einmal notwendig)

1. Öffne slic3r.
2. Importiere die Konfigurationsdatei [Slic3r_config_EasyThreedX2.ini](/slic3r/files/EasyThreedX2.ini) indem du **File >> Load Config** drückst.

  <img src="images/4_2_PrintIt_1.png" width="512"/><br>

3. Jetzt sollten die Konfigurationen im rechten Seitenbereich von Slic3r zu sehen sein.

  <img src="images/4_2_PrintIt_2.png" width="960"/><br>

  **Lassen Sie uns die wichtigsten Einstellungen durchgehen. Ich werde Sie auf Einstellungen hinweisen, die Sie ändern können. Dies geschieht hauptsächlich in den "Printer settings".**

  ***Wenn du einen anderen Drucker verwendest, musst du möglicherweise die Einstellungen dementsprechend ändern und dein eigenes Konfigurationsprofil erstellen!***

4. Drücke auf **Print settings**:

* Dies ist der ***Wie wird gedruckt*** Teil der Einstellungen.
* Am wichtigsten sind die ersten 4 Unterabschnitte **Layers and perimeters**, **Infill**, **Skirt and brim** und **Support material**.


  * Layers and perimeters:

      <img src="images/4_2_PrintIt_3.png" width="512"/><br>

      * Layer height >> Zeigt die Dicke jeder gedruckten Schicht an (0,2).
      * First layer height >> Dicke der ersten Schicht, die oft dicker ist als alle anderen Schichten (0,25).
      * **Je niedriger die Schichthöhe, desto feiner Ihr Druckergebnis, aber desto länger die Druckzeit.**


  * Infill:

    <img src="images/4_2_PrintIt_4.png" width="512"/><br>

      * Fill density >> Legt die Dichte des Materials innerhalb des gedruckten Objekts fest (20 %).
      * **0 % Dichte hält das Objekt hohl, also druckst du nur die Wände.**
      * **100 % Dichte druckt das Objekt solide.**


  * Skirt and Brim:

    <img src="images/4_2_PrintIt_5.png" width="512"/><br>

      * Loops >> druckt ein Band um die unterste Ebene des Objekts.
      * **Auf 0 setzen schaltet es aus. Andernfalls sind die Schleifen höher als 1.**
      * Distance from object >> ist der Abstand zwischen Rock und Objekten
      * Skirt height >> wie viele Schichten werden gedruckt.
      * Brim ist eine Methode, um das Einwickeln von bedrucktem Material beim Abkühlen zu verhindern.
      * Brim width >> wie viel mm Breite sollte um die unterste Schicht des Objekts herumgehen.


  * Support material:

    <img src="images/4_2_PrintIt_6.png" width="512"/><br>

      * Generate support material >> Ein / Aus Aktivierung
      * Overhang threshold >> Grad, der angibt, wann mit dem Hinzufügen von Unterstützung begonnen werden soll.
      * Pattern >> in welcher Form wird das Trägermaterial gedruckt.
      * Pattern spacing >> Abstand zwischen dem Trägermaterial.

  * Speed:

    <img src="images/4_2_PrintIt_7.png" width="512"/><br>

    * Überschreite nicht die Druckgeschwindigkeit von 40 mm/s. Dies ist das Maximum, das der EasyThreedX2 Drucker leisten kann!
    * Die Reisegeschwindigkeit kann um das Doppelte zu sein.


   5. Klicke auf **Plater** um in die 3D Ansicht zu kommen, dann klicke **Filament**:


  * Filament:

    <img src="images/4_2_PrintIt_10.png" width="512"/><br>

    * Filament Dicke bleibt bei EasyThreedX2 bei 1,75 mm.
    * Bei Druckproblemen kannst du versuchen, die Temperatur zu ändern, da das Optimum von der Filamentqualität des PLAs abhängt.


  * Cooling:

    <img src="images/4_2_PrintIt_11.png" width="512"/><br>

    * Auch hier lässt man es am besten.
    * Aber manchmal ist es besser, den Lüfter auszuschalten oder ständig laufen zu lassen.


6. Klicke auf **Plater** um in die 3D Ansicht zu kommen, dann klicke auf **Printer**:


  * General:

    <img src="images/4_2_PrintIt_12.png" width="512"/><br>

    * Die Druckerbettform bleibt mit 10 x 10 cm gleich.

    <img src="images/4_2_PrintIt_13.png" width="512"/><br>

  * Extruder 1:

    <img src="images/4_2_PrintIt_14.png" width="512"/><br>

    * Einstellungen bleiben für EasyThreedX2 gleich

7. Jedes Mal, wenn du einen Wert in einen der 3 Hauptbereiche änderst, wird sich der Name automatisch auf ***name(modified)*** ändern.
8. Click the floppy disk symbol to save the changed version.

    <img src="images/4_2_PrintIt_8.png" width="135"/><br>

   *Dies ist praktisch, wenn du eine Version mit unterschiedlichen Füllungen, mit oder ohne Stützmaterial usw. verwenden möchten.*


9. Benenne es vor dem Speichern um, damit du weisst, was sich geändert hat.

    <img src="images/4_2_PrintIt_9.png" width="256"/><br>


Die Konfiguration wird gespeichert und bei jedem Start von Slic3r aufgerufen.

#### Schneide dein Model in Scheiben mit Slic3r

1. Öffne slic3r.
2. Drücke die Schaltfläche **Add**, wähle deine .stl-Datei aus und öffne sie.

  <img src="images/4_2_PrintIt_15.png" width="960"/><br>

3. Stelle sicher, dass du alle 3 Teile des EasyThreedX2-Konfiguration eingestellt hast.

  <img src="images/4_2_PrintIt_2.png" width="960"/><br>

4. Drücken Sie **Preview**, um die für Ihr Objekt ausgeführten Einstellungen anzuzeigen.
5. Verwende den vertikalen Schieberegler, um sich durch jede Ebene zu bewegen. Überprüfe, ob es unmögliche Drucksituationen gibt (z. B. der Drucker beginnt mitten im Nirgendwo zu drucken)
6. Wenn alles gut aussieht (auch prüfen, ob du Unterstützung benötigst oder nicht), drücke **Exoprt G-Code**.

  <img src="images/4_2_PrintIt_16.png" width="960"/><br>

7. Speichere es.

**Das ist es! Als nächstes werden wir die Möglichkeiten durchgehen, wie man den Drucker steuern kann, damit er das Objekt mit der gcode-Datei druckt.**


---

#### [Zurück zum Index](#index)

## 4.3. Wie drucke ich? <a name="howtoprint"></a>

Da wir nun unsere gcode-Datei haben, werde ich 3 Möglichkeiten durchgehen, um Ihr Objekt tatsächlich mit dem EasyThreeD X1/2 Drucker zu drucken.

#### Das wichtigste zuerst

Egal auf welche Weise du den Drucker steuerst. Es ist gut zu wissen, wie die grundlegenden Verfahren mit dem EasyThreeD X1/2 Drucker ausgeführt werden.

Zuerst die grundlegende Einrichtung des Druckers, die du nur gelegentlich durchführen musst:

1. Verwende blaues Klebeband (oder ein anderes Papierband), um deine magnetische Druckbettoberfläche zu schützen. Dies ist optional, aber ist sehr zu empfehlen.

  <img src="images/BlueTapeOnPrintBed.jpg" width="512"/><br>

2. Richte dein Druckbett aus, um ein Verkratzen der Düse im Druckbett oder einen zu großen Abstand vom Druckbett beim Drucken zu vermeiden.

* Nehme ein Blatt Druckpapier und lege es auf das Druckbett deines Druckers. Du kannst das A4-Papier halbieren.
* Gehen Sie im Menü auf ***>> Prepare >> Auto home***. Der Druckkopf geht in die Ausgangsposition.

  <img src="images/4_3_1_printer_preparation_1.png" width="128"/><br>

* Bewegen Sie dann im Menü einen nach unten und drücken Sie ***>> Disable Steppers***, um das Druckbett und den Druckkopf mit den Händen bewegen zu können.

  <img src="images/4_3_1_printer_preparation_2.png" width="128"/><br>

  **Beginne mit der Schraube unterhalb der Ausgangsposition.**

* Versuche das Stück Druckpapier zu verschieben.

  **Beim Verschieben des Papiers solltest du einen leichten Widerstand spüren. Du kannst das Papier mit beiden Händen an den Enden festhalten, um das Verschieben zu erleichtern.**

  <img src="images/4_3_1_printer_preparation_3.png" width="960"/><br>

* Drehe die Schraube an der Unterseite des Druckerbetts in die eine oder die andere Richtung, wenn du das Papier nicht bewegen kannst oder es sich super leicht bewegen lässt.

  <img src="images/4_3_1_printer_preparation_4.png" width="960"/><br>

* Gehe durch die nächsten 3 Ecken und mache dasselbe.

  **Sei vorsichtig, wenn du das Druckbett und den Druckkopf vorsichtig bewegst, auch wenn es nicht so leicht zu bewegen ist.**

  ***Nachdem du das Druckbett einmal justiert hast, musst du den Vorgang kaum wiederholen. Es wird von Zeit zu Zeit notwendig sein, abhängig von der Raumtemperatur oder nachdem du das blaue Klebeband erneuert hast.***

#### *Blaues Klebeband drauf? Druckfilament bereit? Los gehts!*

<img src="images/4_3_1_printer_preparation_5.png" width="960"/><br>

#### *Ok, füttern wir Ihren Drucker mit Filament!*

  **Zuerst möchten wir den Druckkopf nach oben bewegen, damit wir das eingehende Filament sehen und das Druckbett vor Überhitzung geschützt ist.**

* Der erste Bildschirm, den du nach dem Starten des Druckers sehen wirst, ist der ***Info Screen***.

  <img src="images/4_3_1_printer_preparation_6.png" width="128"/><br>

1. Klicke einmal auf den Drehknopf, um das Hauptmenü zu öffnen. Den Knopf kann man drehen und drücken.

2. Klicke dich durch folgenden Pfad: ***>> Prepare >> Move Axis >> Move Z >> Move 10 mm >>***.

  <img src="images/4_3_1_printer_preparation_7.png" width="128"/><br>

  <img src="images/4_3_1_printer_preparation_8.png" width="128"/><br>

  <img src="images/4_3_1_printer_preparation_9.png" width="128"/><br>

  <img src="images/4_3_1_printer_preparation_10.png" width="128"/><br>

  <img src="images/4_3_1_printer_preparation_11.png" width="128"/><br>

3. Drehe nun den Knopf 2 - 3 Schritte im Uhrzeigersinn.

  **Der Kopf bewegt sich 30 mm nach oben. Dies sollte reichen.**

4. Gehe im Untermenü zur ersten Option und klicke, um zum Hauptmenü zurückzukehren.

  **Wenn du eine Weile wartest, wird das Menü automatisch zurückgesetzt.**

  **Als nächstes möchten wir das Filament hinzufügen oder ersetzen.**

5. Gehe zu ***>> Prepare >> In/Out Filament >>***.

  <img src="images/4_3_1_printer_preparation_7.png" width="128"/><br>

  <img src="images/4_3_1_printer_preparation_13.png" width="128"/><br>

  **Sobald du In/Out Filament drückst, heizt der Drucker seinen Druckkopf auf 200 Grad Celsius auf.**

  **Berühre nicht die Düse :-)**

6. Wähle nun die gewünschte Option aus:

  ***In Filament*** >> Der Extruder des Druckkopfes bewegt das Filament in Richtung der Düse, sobald 200 Grad Celsius erreicht sind.

  ***Out Filament*** >> Der Extruder des Druckkopfes bewegt das Filament von der Düse weg, sobald 200 Grad Celsius erreicht sind.

  ***Stop In/Out Filament*** >> Stoppe den Extrudermotor und die Heizung, wodurch die Düse abgekühlt wird.

  **Sobald du In Filament oder Out Filament drückst, wird die Temperatur oben im Display angezeigt.**

7. Wenn die Düsentemperatur 200 Grad Celsius erreicht ist und du hörst, wie sich der Extrudermotor bewegt:

  **Drücke etwas das Filament, bis du spürst, dass es eingezogen wird.**

  **Auf der anderen Seite ziehe beim Entfernen des Filaments etwas daran, bis es sich vom Extruder löst.**

  <img src="images/4_3_1_printer_preparation_12.png" width="512"/><br>

8. Drücke Stop ***In/Out Filament***, sobald das Filament aus der Düse kommt oder das Filament aus dem Extruder freigegeben wird.

  **Stelle sicher, dass die Düse sauber ist und kein Filament herausragt, bevor du den Druckkopf auf X: 0, Y: 0, Z: 0 verschiebst, indem du *Auto home* wählst.**

  **Benutze ein Stück Papier oder eine Pinzette, um herausstehendes Filament zu entfernen.**

  **Tue dies bevor es abkühlt, da es danach schwieriger ist, es zu entfernen.**

Jetzt, da das Filament drin ist und das Druckbett nivelliert ist, kannst du mit dem Drucken beginnen!


#### 1. Erster Weg .... verwende einfach eine SD-Karte


* Überprüfe, ob du genügend Filament in deinem Drucker hast. Ist es die Farbe, die du verwenden möchtest?

1. Gehe wie folgt durch das Menü ***>> Prepare >> Auto home***.

  <img src="images/4_3_1_printer_preparation_7.png" width="128"/><br>

  <img src="images/4_3_1_printer_preparation_1.png" width="128"/><br>

  **Dadurch wird der Kopf deines Druckers auf X: 0, Y: 0, Z: 0 eingestellt, was sich in der Ecke unten links befindet.**

2. Stecke deine SD-Karte mit der .gcode-Datei deines geschnittenen Objekts ein.

  <img src="images/4_3_1_sdcard_print_2.png" width="512"/><br>

3. Gehe zu ***>> Print from SD >> your_object.gcode***.

  <img src="images/4_3_1_sdcard_print_1.png" width="128"/><br>

  ***your_object.gcode steht für den Dateinamen deiner Datei.***

4. Klicke auf deine Datei.

  **Und los gehts!**

* Die Druckerdüse erwärmt sich auf die von dir gewählten Temperatur und beginnt mit dem Drucken.
* Du kannst den Druck anhalten oder stoppen, falls etwas passiert.

---

#### 2. Benutze Slic3r um den Drucker zu kontollieren

In diesem Abschnitt erfahrst du, wie du mit Slic3r deinen Drucker steuern und dein Objekt direkt druckst.

Dazu musst du deinen Drucker zuerst über USB mit deinem Laptop/PC verbinden.

#### Konfiguration

Die nächsten Schritte musst du nur einmal ausführen, um Slic3r als Druckerbedienung einzurichten.

* Als Erstes gehe zu **File >> Preferences...**, und aktiviere ***Show Controller Tab***.
* Presse **OK**, und starte danach Slic3r neu.

<img src="images/4_3_2_slic3r_print_1.png" width="360"/><br>

* Stelle sicher, dass die EasyThreedX Konfiguration importiert wurde, und gehe dann zu **Printer:**

<img src="images/4_3_2_slic3r_print_2.png" width="960"/><br>

* Prüfe, ob dein Drucker unter **Serial port:** erkannt wird erkannt wird.
* Verwende das Dropdown-Menü, falls du verschiedene USB-Geräte angeschlossen haast.
* Check die **Speed:**-Option. Es sollte ***115200*** Baudrate betragen.
* Drücke **Test**, um zu sehen, ob dein Drucker eine Verbindung zum PC/Laptop herstellt.

<img src="images/4_3_2_slic3r_print_3.png" width="960"/><br>

**Wenn dein Drucker angeschlossen ist, kannst du dein Objekt aus Slic3r heraus drucken.**

#### Der Druck

Stelle zunächst sicher, dass dein Drucker eingeschaltet ist, und stelle eine Verbindung zu deinem Laptop/PC her.

1. Drücke **>> Open STL/OBJ/AMF/JMF...**, oder drücke **Add**.

  **Prüfe per "Preview", ob mit Stützmaterial und Füllung alles in Ordnung ist.**

2. Drücke **Print...**

  <img src="images/4_3_2_slic3r_print_4.png" width="960"/><br>

3. Klicke auf das Tab **Controller**.
4. Dort wirst du **Manual control** finden. Drücke diesen Knopf.

  **Es öffnet sich ein Fenster, in dem du manuelle Steuerungsoptionen für deinen Drucker hast (Drucker muss verbunden sein).**

  <img src="images/4_3_2_slic3r_print_5.png" width="960"/><br>

5. Tippe ***210*** in **Extruder** ein und drücke **Set**.
6. Schließe das Fenster.

  <img src="images/4_3_2_slic3r_print_6.png" width="512"/><br>

  **Siehst du, wie die Temperatur steigt?**

7. Drücke **Print This**.

  <img src="images/4_3_2_slic3r_print_7.png" width="960"/><br>

  **Der Drucker sollte jetzt mit dem Drucken beginnen.**

  **Der Druckvorgang ist unter "Printing..."  angegeben.**

  <img src="images/4_3_2_slic3r_print_8.png" width="960"/><br>


---

#### 3. Benutze GCodePrintr am Android Tablet

In diesem Abschnitt erfahrst du, wie du ein Android-Tablet als Druckersteuergerät verwenden kannst.

#### Einrichtung

* In erster Linie benötigst du einen Adapter, mit dem du deinem Drucker an eine Mini-USB-Buchse deines Tablets anschließen kannst.

  Ein Beispiel für einen solchen Adapter:

  <img src="images/OTG_USB_Adapter.jpg" width="256"/><br>


* Als nächstes musst du diese Software bei Google Play kaufen und auf deinem Gerät installieren:

  [GCodePrintr](https://play.google.com/store/apps/details?id=de.dietzm.gcodesimulatorprinter&hl=en_US&gl=US)

* Verbinde nun deinem Drucker mit deinem Tablet und starte GCodePrintr.

  <img src="images/4_3_3_gcodeprintr_1.png" width="960"/><br>

* Drücke nun das **Zahnrad**-Symbol im linken Bereich, um **Settings** zu öffnen.

  <img src="images/4_3_3_gcodeprintr_2.png" width="960"/><br>

* Lass uns zunächst die Druckereinstellungen einrichten, indem du **Settings >> Printer** drückst.

  <img src="images/4_3_3_gcodeprintr_3.png" width="960"/><br>

* Drücke **Bed size**, und gebe ***100*** ein.
* Drücke **OK**.

  <img src="images/4_3_3_gcodeprintr_4.png" width="360"/><br>

* Drücke **Heatbed Temperature Preset 1**, und gebe ***185*** ein.
* Drücke **OK**.
* Drücke **Heatbed Temperature Preset 1**, und gebe ***210*** ein.
* Drücke **OK**.
* Drücke **Heatbed Temperature Preset 3 / Max Temperature**, und gebe ***230*** ein.
* Drücke **OK**.

  <img src="images/4_3_3_gcodeprintr_5.png" width="360"/><br>

* Jetzt drück auf **Material**, und drücke dort auf ***PLA (1.25g/cm3)***.

  <img src="images/4_3_3_gcodeprintr_6.png" width="360"/><br>

* Verwende zweimal das dreieckige Rückwärts-Symbol, um zu **Settings** zurückzukehren.
* Drücke dann auf **Communication**.

  <img src="images/4_3_3_gcodeprintr_7.png" width="960"/><br>

* Drücke auf **Printer Connection Type**, und wähle ***USB-OTG***.

  <img src="images/4_3_3_gcodeprintr_8.png" width="360"/><br>

* Geh zurück und klicke dann auf **Baud rate for USB OTG**.
* Wähle ***115200***.

  <img src="images/4_3_3_gcodeprintr_9.png" width="360"/><br>

**Das ist alles! Der Drucker sollte jetzt unter der Kontrolle von GCode Printer ordnungsgemäß funktionieren!**


#### Der Druck

1. Drücke auf das Ordnersymbol im linken Bereich, um deine Datei zu importieren.

  <img src="images/4_3_3_gcodeprintr_10.png" width="960"/><br>

2. Suche und wähle deine .gcode Datei aus.

  **Dies ist bei Slic3r anders. Du benötigst eine .gcode-Datei, nicht die .stl-Datei.**

  ***Wenn du nur die .stl-Datei hast, gehe zu "4.2.1 Verwende kiri:moto-Website" und starte von dort.***

  <img src="images/4_3_3_gcodeprintr_12.png" width="960"/><br>

  **Sobald der gcode der Objekte importiert ist, beginnt die Software, den Druck zu simulieren.**

3. Drücke auf das Druckersymbol im rechten Seitenbereich.

  **Dadurch wird das Bedienfeld des Druckers geöffnet.**

  <img src="images/4_3_3_gcodeprintr_13.png" width="960"/><br>

4. Drücke **Connection Test** um zu sehen, ob dein Drucker richtig mit deinem Tablet verbunden ist.

  **Du solltest in der Konsole auf der rechten Seite eine Meldung erhalten, dass der Drucker nicht verbunden ist.**

  ***Auch die "i" Zeile zeigt, ob der Drucker bereits ist >> "Ready to print".***

  <img src="images/4_3_3_gcodeprintr_14.png" width="960"/><br>

5. Bewege nun den Temperaturschieber nach rechts, um die Düse auf 210 Grad Celsius aufzuheizen.

  ***Die Düsentemperatur wird auf der rechten Seite als zweite Zeile unterhalb der Konsole angezeigt.***

6. Wenn die Temperatur erreicht ist, drücke das Drucksymbol, um den Druckvorgang zu starten.

  <img src="images/4_3_3_gcodeprintr_15.png" width="960"/><br>

7. Drücke danach auf das Drucksymbol auf der linken Seite des Bildschirms, um das Bedienfeld des Druckers zu schließen.

  <img src="images/4_3_3_gcodeprintr_16.png" width="960"/><br>

  **Die Vorschau befindet sich nun im "Druckmodus", der dir den Druckvorgang grafisch darstellt.**

  <img src="images/4_3_3_gcodeprintr_17.png" width="960"/><br>

  **Nachdem der Druck abgeschlossen ist, erscheint ein Pop-up-Fenster.**

  <img src="images/4_3_3_gcodeprintr_18.png" width="360"/><br>

#### *Das ist alles! Ich wünsche dir viel Spaß beim Drucken deiner Designs!*

---

#### Nach dem Druckprozess

* Nehme nach Abschluss des Drucks die magnetische Druckbettoberfläche mit dem blauen Klebeband ab und biege sie. Es macht sich so einfacher, den Druck von der Plattform zu enfernen.
* Wenn du Stützmaterial verwendet hast, löse es mit einer Miniatur-Taschenzange von deinem Objekt. Des braucht etwas Erfahrung, mit der Zeit wird dies besser funktionieren.

  <img src="images/ChainNosePocketPliers.jpg" width="256"/><br>


---

#### Gedruckte Beispiele

| <img src="images/Necklace2.jpg" width="400"/><br> | <img src="images/ShoeTag2.jpg" width="400"/><br> |
|----------------------|-----------------------|
| <img src="images/Robot2.jpg" width="400"/><br>  |  <img src="images/CuteRobot2.jpg" width="400"/><br> |

| <img src="images/Fingerring2.jpg" width="840"/><br>   |
|-------------------------------------------------------|
| <img src="images/Fingerpuppet2.jpg" width="840"/><br> |


---
## ENDE

### Ich hoffe, dir hat dieser Kurs gefallen. Viel Spaß ein "Maker" zu sein :-)

***Vielen Dank, dass du dir die Zeit genommen hast, und wenn du Feedback und/oder Vorschläge hast, werde ich mich darüber sehr freuen!***

#### contact@jensmeisner.net
