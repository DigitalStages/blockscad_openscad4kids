# Slic3r

![img1](images/Slic3r.png)


Slic3r is the tool you need to convert a 3D model into printing instructions for your 3D printer. It cuts the model into horizontal slices (layers), generates toolpaths to fill them and calculates the amount of material to be extruded.

Slic3r is:

    Open: it is totally open source and it’s independent from any commercial company or printer manufacturer. We want to keep 3D printing open and free.
    Compatible: it supports all the known G-code dialects (Marlin, Repetier, Mach3, LinuxCNC, Machinekit, Smoothie, Makerware, Sailfish).
    Advanced: many configuration options allow for fine-tuning and full control. While novice users often need just few options, Slic3r is mostly used by advanced users.
    Community-driven: new features or issues are discussed in the GitHub repository. Join our collaborative effort and help improve it!
    Robust: the codebase includes more than 1,000 unit and regression tests, collected in 6 years of development.
    Modular: the core of Slic3r is libslic3r, a C++ library that provides a granular API and reusable components.
    Embeddable: a complete and powerful command line interface allows to use Slic3r from the shell or to integrate it in server-side applications.

## Directories and its content

* Slic3r_EasyThreedX2_Config: Contains importable configuration especially for the EasyThreedX2 Printer in order to start straight away with slicing

[Link to Slic3r](https://slic3r.org)
